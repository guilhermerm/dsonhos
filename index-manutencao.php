<?php
//    header ("Location: ./dsonhos/inicio/inicio.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>.: D'Sonhos :.</title>
        <style>
            *{
                border: 0;
                 margin: 0;
                 padding: 0;                
            }
            body{
                 background-color: black;
                 background-image:url('imagens/layout/bg_image.jpg');
                 background-position: top center;
                 background-repeat: repeat;
                 font-family: Arial, Helvetica, sans-serif;
                /* font-family:"DsonhosFont", Arial, sans-serif;*/
                 color:#732f3c;        
                 line-height: 20px;  
                 height: 100%;       
                 padding: 50px 0 0 0;
                 /*border: 1px solid black;*/
            }
            
        </style>
    </head>
    <body >        
        <table align="center" valign="middle" width="100%" height="100%">
            <tr valign="middle">
                <td  align="center" valign="middle">
                    <img src='imagens/layout/logo-dsonhos.png'/>
                    <br><br>
                            <span style="font-size: 20px"> No momento estamos em manutenção. <br>Em breve voltamos!</span>
                        <br><br><b>Telefones de Contato:</b><br><br>
                        Selma Toledo / Thiago Toledo <br>
                        (27) 3084-9511 / (27) 9921-8795 / (27) 3323-9760 <br>
                        Local: Vitória - ES
                        <br><br>
                        <b>Contato por E-mail:</b>
                        dsonhos@dsonhos.com<br><br><b>www.dsonhos.com</b>
                </td>                
            </tr>
        </table>
    </body>
</html>
