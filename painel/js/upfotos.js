//Função usada pelo swfHandlers.js
var $jQ2 = jQuery.noConflict();

jQuery(document).ready(function() {
    //LINKS DO MENU
    $jQ2('#linkalbuns').click(function(){ window.location = 'portfolio.php?p=albuns'; });
    $jQ2('#linkaddalbum').click(function(){ window.location = 'portfolio.php?p=addalbum'; });
    $jQ2('#linkupfotos').click(function(){ window.location = 'portfolio.php?p=upfotos'; });
    $jQ2('#linkaddvideo').click(function(){ window.location = 'portfolio.php?p=addvideo'; });
    $jQ2('#linkvideos').click(function(){ window.location = 'portfolio.php?p=videos'; });
    
    //Iniciar LightBox
//    initLightbox();     
    
});


function loadFotoToBox(){    
    
    $jQ2("#boxupfotos").css("border","1px solid blue");
    
    var grouptmp = $jQ2("#grouptmp").val();        
    var idalbum = $jQ2("#idalbum").val();        
    
    $jQ2.post('upfotosload.php',{op:'load',grouptmp:grouptmp,idalbum:idalbum},function(r){
        if(r!=='false'){
            $jQ2("#boxupfotos").html(r);
            $jQ2("#boxupfotos").css("border","1px solid green");                            
            
            //quantidade de fotos enviadas e carregadas via ajax
            var count = $jQ2("#count").val();
            $jQ2("#spancount").text(count);
            $jQ2("#spancount2").text(count);
            $jQ2("#spancount2").css({color:"green","font-weight":"bold"});
            
            //Quantidade total de fotos enviadas
            var total = $jQ2("#spantotal").text();
            total = parseInt(total) + 1;
            $jQ2("#spantotal").text(total);
            $jQ2("#spantotal").css({color:"black","font-weight":"bold"});
            
            //Quantidade total possivelmente com erro;
            var totalerror = total - count;
            $jQ2("#spantotalerror").text(totalerror);
            $jQ2("#spantotalerror").css({color:"red","font-weight":"bold"});
            
        }else{
            $jQ2("#boxupfotos").html("<small> Não foi possível carregar as fotos</small>"+r);            
            $jQ2("#boxupfotos").css("border","1px solid red");
        }
    });
    
}