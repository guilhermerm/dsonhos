var $jQ = jQuery.noConflict();

jQuery(document).ready(function($) {      
    
    //Iniciar Lista Sortable
    $("#sortable0").sortable({
        update: function(){
            salvarOrdemProds();
        },
        //axis: "y",
        cursor: "move",
        disabled: true
    });
    //Iniciar Lista Sortable
    $("#sortable1").sortable({
        update: function(){
            salvarOrdemCats();
        },
        //axis: "y",
        cursor: "move",
        disabled: true
    });

    //Mostrar Miniaturas dos Produtos
    $('.itemprod').hover(
        function(){ 
            $(this).find(".thumbimg").toggle();
        },
        function(){
            $(this).find(".thumbimg").toggle();
//            var alb = $(this).attr('idalbum');
//            $('.ordthumb[idalbum='+alb+']').toggle();     
        }                
    );   
    //Botão Mostrar ordernar Produtos
    $('#btnitemmove').click(function(){
        $(".itemprodmove").toggle();
        $(".itemcontrole").toggle();
        tex = $("#btnitemprodmove").find("#btntext");
        texto = tex.text();
        if(texto === "Organizar produtos"){
            tex.text("Finalizar");
            $("#sortable0").sortable("option","disabled",false);
        }else{
            $("#sortable0").sortable("option","disabled",true);
            tex.text("Organizar produtos");
        }
    });
        
        
    //LINKS DO MENU
    $('#linkprods').click(function(){ window.location = 'produtos.php?p=prods'; });
    $('#linkaddprod').click(function(){ window.location = 'produtos.php?p=addprod'; });
    $('#linkordemprod').click(function(){ window.location = 'produtos.php?p=ordemprod'; });
    $('#linkcats').click(function(){ window.location = 'produtos.php?p=cats'; });
    $('#linkaddcat').click(function(){ window.location = 'produtos.php?p=addcat'; });
   
    
    //BOTAO Submit ADD PRODUTO
    $('#submitaddprod').click(function(){ 
        if($("#scat").val() === "0"){        
            alert("Selecione um Categoria");
            $("#scat").focus();
            return false;
        }         
        if($("#texto1").val() == ""){        
            alert("Informe o nome do Produto");
            $("#texto1").focus();
            return false;
        }
        
        document.formaddprod.action = "produtos-bd.php";
        document.formaddprod.submit();
    });
    $('#canceladdprod').click(function(){ window.location = 'produtos.php?p=prods'; });
    //BOTAO Submit EDIT PRODUTOS
    $('#submiteditprod').click(function(){ 
        
        if($("#scat").val() === "0"){        
            alert("Selecione um Categoria");
            $("#scat").focus();
            return false;
        }         
        if($("#texto1").val() == ""){        
            alert("Informe o nome do Produto");
            $("#texto1").focus();
            return false;
        }            
        document.formeditprod.action = "produtos-bd.php";
        document.formeditprod.submit();
    });
    $('#canceleditprod').click(function(){ window.location = 'produtos.php?p=prods'; });    
    
    //BOTAO Submit ADD CATEGORIA
    $('#submitaddcat').click(function(){ 
        if($("#texto1").val() === ""){        
            alert("Informe o nome da Categoria");
            $("#texto1").focus();
            return false;
        }
        document.formaddcat.action = "produtos-bd.php";
        document.formaddcat.submit();
    });
    $('#canceladdcat').click(function(){ window.location = 'produtos.php?p=cats'; });
    //BOTAO Submit EDIT CATEGORIA
    $('#submiteditcat').click(function(){        
        if($("#texto1").val() == ""){        
            alert("Informe o nome da Categoria");
            $("#texto1").focus();
            return false;
        }            
        document.formeditcat.action = "produtos-bd.php";
        document.formeditcat.submit();
    });
    $('#canceleditcat').click(function(){ window.location = 'produtos.php?p=cats'; });
       
    //Botão Mostrar ordernar Categorias
    $('#btncatmove').click(function(){
        $(".itemcatmove").toggle();
        $(".itemcontrole").toggle();
        tex = $("#btncatmove").find("#btntext");
        texto = tex.text();
        if(texto === "Organizar categorias"){
            tex.text("Finalizar");
            $("#sortable1").sortable("option","disabled",false);
        }else{
            $("#sortable1").sortable("option","disabled",true);
            tex.text("Organizar categorias");
        }
    });
    
});

//Exclui Produto pelo ID
function excluirProduto(codigo){
    
    var li = $jQ(".lisort[idprod="+codigo+"]");
    var di = $jQ(".itemprod[idprod="+codigo+"]");
    di.css({background:"#FAAAAA"});
    di.css("pointerEvents","none");
    if(confirm("Deseja excluir permanentemente este produto?")){
                        
        $jQ.post('produtos-bd.php',{op:'exc-prod',idprod:codigo},function(r){
//            alert (r);
            if(r=='true'){
                //Excluido
                li.remove();                
            }else{
                //Erro ao excluir
                di.css("pointerEvents","auto");
                di.css({background:"white"});                  
            }
        });        
    }else{
        di.css("pointerEvents","auto");
        di.css({background:"white"});   
    }   
}
//Exclui CATEGORIA vazia pelo ID
function excluirCategoria(codigo){
    
    var li = $jQ(".catlisort[idcat="+codigo+"]");
    var di = $jQ(".itemcat[idcat="+codigo+"]");
    di.css({border:"1px solid red",background:"#FAAAAA"});
    di.css("pointerEvents","none");
    if(confirm("Deseja excluir permanentemente esta Categoria?")){
                        
        $jQ.post('produtos-bd.php',{op:'exc-cat',idcat:codigo},function(r){
//            alert (r);
            if(r=='true'){
                //Excluido
                li.remove();                
            }else{
                //Erro ao excluir
                di.css("pointerEvents","auto");
                di.css({border:"1px solid #ccc",background:"white"});                  
            }
        });        
    }else{
        di.css("pointerEvents","auto");
        di.css({border:"1px solid #ccc",background:"white"});   
    }   
}
//Exclui CATEGORIA cheia pelo ID
function excluirCategoriaCheia(codigo){
    
    var li = $jQ(".lisort[idcat="+codigo+"]");
    var di = $jQ(".itemcat[idcat="+codigo+"]");
    di.css({border:"1px solid blue",background:"#AAAAFA"});
    di.css("pointerEvents","none");
    alert("Não é possível excluir uma categoria com produtos cadastrados");
        di.css("pointerEvents","auto");
        di.css({border:"1px solid #ccc",background:"white"});   
//    if(confirm("Deseja excluir permanentemente esta Categoria?")){
//                        
//        $jQ.post('produtos-bd.php',{op:'exc-cat',idcat:codigo},function(r){
////            alert (r);
//            if(r=='true'){
//                //Excluido
//                li.remove();                
//            }else{
//                //Erro ao excluir
//                di.css("pointerEvents","auto");
//                di.css({border:"1px solid #ccc",background:"white"});                  
//            }
//        });        
//    }else{
//        di.css("pointerEvents","auto");
//        di.css({border:"1px solid #ccc",background:"white"});   
//    }   
}

function mostrarExcluir(){
    $jQ('.btnexcluir').toggle();
    link = $jQ('#togglexcluir');
}

function prodlermais(id){
    var aux = $jQ(".alermais[idprod="+id+"]");
    val = aux.attr("value");
    if(val === "0"){
        $jQ(".adescricao[idprod="+id+"]").css({
            height:"auto",
            "white-space": "normal"
        });
        aux.attr("value","1");
        aux.text("Menos");
    }else{
        $jQ(".adescricao[idprod="+id+"]").css({
            height:"14px",
            "white-space": "nowrap"
        });        
        aux.attr("value","0");
        aux.text("Ler mais");
    }
}

function catlermais(id){
    var aux = $jQ(".alermais[idcat="+id+"]");
    val = aux.attr("value");
    if(val === "0"){
        $jQ(".adescricao[idcat="+id+"]").css({
            height:"auto",
            "white-space": "normal"
        });
        aux.attr("value","1");
        aux.text("Menos");
    }else{
        $jQ(".adescricao[idcat="+id+"]").css({
            height:"14px",
            "white-space": "nowrap"
        });        
        aux.attr("value","0");
        aux.text("Ler mais");
    }
}

/* - - - - - - - - - - - - - - - - - - - - - - - - -EDIT PRODUTOS - - - - */
function trocarImagem(){
    $jQ('#boxfoto').toggle();
    $jQ('#boxfilefoto').toggle();
    $jQ('#trocarimg').val('1');    
}
function cancelarTrocarImagem(){
    $jQ('#boxfoto').toggle();
    $jQ('#boxfilefoto').toggle();
    $jQ('#trocarimg').val('0');    
}

//Função usada pelo swfHandlers.js
function loadFotoToBox(){
    
    var idprod = $jQ("#idprod").val();
    var grouptmp = $jQ("#grouptmp").val();      
    
    $jQ.post('produtos-bd.php',{op:'load-fts',idprod:idprod, grouptmp:grouptmp},function(r){
        if(r!=='false'){
            $jQ("#boxprodfotos").html(r);
//            lightbox();
        }else{
            $jQ("#boxprodfotos").html("<center><small><span class='glyphicon glyphicon-warning-sign'></span> Sem fotos ou não foi possível carregar.<br><br></small></center>");
        }
    });
    
}

function excluirFto(id){
//    var li = $jQ(".lisort[idprod="+id+"]");
    var di = $jQ(".prodfotos[idfoto="+id+"]");
    di.css({border:"1px solid red",background:"#FAAAAA"});
//    di.css("pointerEvents","none");
    if(confirm("Deseja excluir esta foto?")){
                        
        $jQ.post('produtos-bd.php',{op:'exc-fto',idfoto:id},function(r){
//            alert (r);
            if(r=='true'){
                //Excluido
                di.remove();
            }else{
                //Erro ao excluir
//                di.css("pointerEvents","auto");
                di.css({border:"1px solid #ccc",background:"white"});
            }
        });        
    }else{
//        di.css("pointerEvents","auto");
        di.css({border:"1px solid #ccc",background:"white"});   
    }  
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - ORDENAR PROD/CAT- - - - */

function salvarOrdemProds(){    
    var sort = $jQ("#sortable0");
    var ordem = '';    
    sort.children('li').each(function(){
        ordem += $jQ(this).attr('idprod')+',';
    });
    $jQ.post('produtos-bd.php',{op:'ord-prod',ordem:ordem},function(r){
        if(r==='true'){
        }else{            
        }
    });
}

function salvarOrdemCats(){    
    var sort = $jQ("#sortable1");
    var ordem = '';    
    sort.children('li').each(function(){
        ordem += $jQ(this).attr('idcat')+',';
    });
    $jQ.post('produtos-bd.php',{op:'ord-cat',ordem:ordem},function(r){
        if(r==='true'){
        }else{            
        }
    });
}
