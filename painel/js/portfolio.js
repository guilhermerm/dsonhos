//var $jD = 
var $jQ = jQuery.noConflict();

jQuery(document).ready(function($) {
//$(document).ready(function() {
    
    //Iniciar LightBox
//    lightbox();     
    
    //Iniciar Lista Sortable
    var qtd = $("#contsort").attr("value");   
    for(var i=0;i<=qtd;i++){
        $("#sortable"+i).sortable({
            update: function(){
                var idsort = $(this).attr("id");
                var idpai = $(this).attr("idpai");
                salvarOrdemAlbuns(idsort,idpai);
            },
            //axis: "y",
            cursor: "move"
        });
    }
    $("#sortablev").sortable({
        update: function(){
            salvarOrdemVideos();
        },
//        axis: "y",
        cursor: "move",
        disabled: true
    });
    //Mostrar Miniaturas dos albuns no Ordenar Albuns
    $('.albumsub,.albumpai').hover(
        function(){ 
            var alb = $(this).attr('idalbum');
            $('.ordthumb[idalbum='+alb+']').toggle();
            $('.linkgoalb[idalbum='+alb+']').toggle();
        },
        function(){
            var alb = $(this).attr('idalbum');
            $('.ordthumb[idalbum='+alb+']').toggle();   
            $('.linkgoalb[idalbum='+alb+']').toggle();   
        }                
    );   
    //Botão Mostrar Sub-albuns em orderm de fotos
    $('#mostrarSubAlbuns').click(function(){
        $(".ulsubalbuns").toggle();
        texto = $("#mostrarSubAlbuns").text();
        if(texto === "Mostrar Sub-álbuns"){
            $("#mostrarSubAlbuns").text("Ocultar Sub-álbuns");
        }else{
            $("#mostrarSubAlbuns").text("Mostrar Sub-álbuns");
        }
    });
        
        
    //LINKS DO MENU
    $('#linkalbuns').click(function(){ window.location = 'portfolio.php?p=albuns'; });
    $('#linkordemalbum').click(function(){ window.location = 'portfolio.php?p=ordemalbum'; });
    $('#linkaddalbum').click(function(){ window.location = 'portfolio.php?p=addalbum'; });
    $('#linkupfotos').click(function(){ window.location = 'upfoto.php'; });
    $('#linkaddvideo').click(function(){ window.location = 'portfolio.php?p=addvideo'; });
    $('#linkvideos').click(function(){ window.location = 'portfolio.php?p=videos'; });
    
    //BOTAO Submit ADD ALBUM
    $('#submitaddalbum').click(function(){ 
        if($("#texto1").val() == ""){        
            alert("Informe o Título do novo álbum");
            $("#texto1").focus();
            return false;
        }
//        if($("#texto2").val() == ""){
//            alert("Informe uma breve descrição");
//            $("#texto2").focus();
//            return false;
//        }            
        document.formaddalbum.action = "portfolio-bd.php";
        document.formaddalbum.submit();
    });
    $('#canceladdalbum').click(function(){ window.location = 'portfolio.php?p=albuns'; });
    
    //BOTAO Submit EDIT ALBUM
    $('#submiteditalbum').click(function(){ 
        if($("#texto1").val() == ""){        
            alert("Informe o Título do novo álbum");
            $("#texto1").focus();
            return false;
        }
//        if($("#texto2").val() == ""){
//            alert("Informe uma breve descrição");
//            $("#texto2").focus();
//            return false;
//        }            
        document.formeditalbum.action = "portfolio-bd.php";
        document.formeditalbum.submit();
    });    
    $('#canceleditalbum').click(function(){ window.location = 'portfolio.php?p=albuns'; });      
    
    //BOTAO Submit ADD VIDEO
    $('#submitaddvid').click(function(){ 
        if($("#texto1").val() == ""){        
            alert("Informe o Título do novo Vídeo");
            $("#texto1").focus();
            return false;
        }
        if($("#texto2").val() == ""){
            alert("Informe o Link do vídeo no YouTube");
            $("#texto2").focus();
            return false;
        }else if (!isYoutubeUrl($("#texto2").val())){
            alert("Informe um Link válido");
            $("#texto2").focus();
            return false;            
        }
            
        document.formaddvid.action = "portfolio-bd.php";
        document.formaddvid.submit();
    });
    $('#canceladdvid').click(function(){ window.location = 'portfolio.php?p=albuns'; });
    
    //BOTAO Submit EDIT VIDEO
    $('#submiteditvid').click(function(){ 
        if($("#texto1").val() == ""){        
            alert("Informe o Título do novo Vídeo");
            $("#texto1").focus();
            return false;
        }
        if($("#texto2").val() == ""){
            alert("Informe o Link do vídeo no YouTube");
            $("#texto2").focus();
            return false;
        }else if (!isYoutubeUrl($("#texto2").val())){
            alert("Informe um Link válido");
            $("#texto2").focus();
            return false;            
        }          
        document.formeditvid.action = "portfolio-bd.php";
        document.formeditvid.submit();
    });    
    $('#canceleditvid').click(function(){ window.location = 'portfolio.php?p=albuns'; });      
    
    //Botão Mostrar Sub-albuns em orderm de fotos
    $("#btnitemvidmove").click(function(){
//        alert("oi");
//        $(this).css("border","1px solid green");
        $(".videomove").toggle();
        $(".videocontrole").toggle();
        tex = $("#btnitemvidmove").find("#btntext");
        texto = tex.text();
        if(texto === "Organizar videos"){
            tex.text("Finalizar");
            $("#sortablev").sortable("option","disabled",false);
        }else{
            $("#sortablev").sortable("option","disabled",true);
            tex.text("Organizar videos");
        }
    });      
    
});

function isYoutubeUrl(url){
	return /((http|https):\/\/)?(www\.)?(youtube\.com)(\/)?([a-zA-Z0-9\-\.]+)\/?/.test(url);
}

function verificaVideo(){
	var text = $jQ("#texto2").val();
        if(text!=="" && isYoutubeUrl(text)){
           var aux = text.split("v=");
           var aux1 = aux[1].split("&");
            $jQ("#iframevideo").show().html('<iframe width="340" height="255" src="//www.youtube.com/embed/'+aux1[0]+'" frameborder="0" allowfullscreen></iframe>');
        }else{
            alert("Link inválido");            
        }
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - ALBUM DE FOTOS- - - - -*/
//Ocultar ALBUM
function ocultarAlbum(codigo){    
    $jQ(this).css("pointerEvents","none");
    $jQ("#btnocultar").hide();
    $jQ("#waiting").show();
    
    $jQ.post('portfolio-bd.php',{op:'ocu-alb',idalbum:codigo,alboculto:'1'},function(r){
//        alert(r);
        if(r=='true'){
            $jQ("#btnmostrar").css("pointerEvents","auto");
            $jQ("#btnmostrar").show();
            $jQ("#waiting").hide();
            $jQ("#btnocultar").css("pointerEvents","none");            
            $jQ("#btnocultar").hide();
        }else{
            //Erro ao ocultar
            $jQ(this).css("pointerEvents","auto");
            $jQ("#btnocultar").show();
            $jQ("#waiting").hide();
        }
    });
         
}
//Mostrar ALBUM
function mostrarAlbum(codigo){    
    $jQ(this).css("pointerEvents","none");     
    $jQ("#btnmostrar").hide();
    $jQ("#waiting").show();    
    $jQ.post('portfolio-bd.php',{op:'ocu-alb',idalbum:codigo,alboculto:'0'},function(r){
//        alert(r);
        if(r=='true'){
            //Changed
            $jQ("#btnmostrar").css("pointerEvents","none");
            $jQ("#btnmostrar").hide();
            $jQ("#waiting").hide();
            $jQ("#btnocultar").css("pointerEvents","auto");
            $jQ("#btnocultar").show();
        }else{
            //Erro ao mostrar
            $jQ(this).css("pointerEvents","auto");
            $jQ("#waiting").hide();
            $jQ("#btnmostrar").show();
        }
    });
         
}

//Exclui Foto pelo ID
function excluirFoto(codigo){
    
    $jQ(".tfoto[idfoto="+codigo+"]").css("border","1px solid red");
    $jQ(".tfoto[idfoto="+codigo+"]").css("pointerEvents","none");
    if(confirm("Deseja excluir permanentemente esta foto?")){
                        
        $jQ.post('portfolio-bd.php',{op:'exc-fto',idfoto:codigo},function(r){
//            alert (r);
            if(r=='true'){
                //Excluido
                $jQ(".tfoto[idfoto="+codigo+"]").hide();
                valor = $jQ("#ftoqtd").text() - 1;
                $jQ("#ftoqtd").text(valor);
            }else{
                //Erro ao excluir
                $jQ(".tfoto[idfoto="+codigo+"]").css("pointerEvents","auto");
                $jQ(".tfoto[idfoto="+codigo+"]").css("border","1px solid transparent");                  
            }
        });
        
    }else{
        $jQ(".tfoto[idfoto="+codigo+"]").css("border","1px solid transparent");  
        $jQ(".tfoto[idfoto="+codigo+"]").css("pointerEvents","auto");
    }   
}

//Exclui Video pelo ID
function excluirVideo(codigo){
    
    var di = $jQ(".itemvideo[idvid="+codigo+"]").css({border:"1px solid red",background:"#FFAAAA"});
    var li = $jQ(".livideo[idvid="+codigo+"]").css("pointerEvents","none");
    if(confirm("Deseja excluir permanentemente este video?")){
                        
        $jQ.post('portfolio-bd.php',{op:'exc-vid',idvideo:codigo},function(r){
            if(r=='true'){
                //Excluido
                li.remove();
            }else{
                //Erro ao excluir
                di.css({border:"1px solid #BBB",background:"#FFF"});
                li.css("pointerEvents","auto");
            }
        });        
    }else{
        di.css({border:"1px solid #BBB",background:"#FFF"});
        li.css("pointerEvents","auto");
    }        
    
}

//Excluiir Todas as fotos do álbum.
function excluirAllFoto (idalbum){
    $jQ("#albumfotos").css("border","1px solid red");
    $jQ("#albumfotos").css("background-color","#FFA0A0");
    $jQ("#albumfotos").css("pointerEvents","none");
    
    if(confirm("Deseja realmente excluir TODAS as fotos deste álbum?")){
        $jQ.post('portfolio-bd.php',{op:'exc-allfto',idalbum:idalbum},function(r){
            if(r=='true'){               
                location.href="portfolio.php?p=album&album="+idalbum+"&msg=1";
            }else{
                alert('Não foi possivel excluir todas as fotos.');
                $jQ("#albumfotos").css("border","1px solid transparent");
                $jQ("#albumfotos").css("background-color","#f2f2f2");
                $jQ("#albumfotos").css("pointerEvents","auto");               
            }
        });
    }else{
        $jQ("#albumfotos").css("border","1px solid transparent");
        $jQ("#albumfotos").css("background-color","#f2f2f2");
        $jQ("#albumfotos").css("pointerEvents","auto");         
    }
}

//Excluir Album e Todas as fotos do álbum.
function excluirAlbum (idalbum){
    $jQ("#albumfotos").css("border","1px solid red");
    $jQ("#albumfotos").css("background-color","#FFA0A0");
    $jQ("#albumfotos").css("pointerEvents","none");
    $jQ("#subalbumfotos").css("background-color","#FFA0A0");
    $jQ("#subalbumfotos").css("pointerEvents","none");    
    
    if(confirm("Todas as fotos serão perdidas. Deseja realmente excluir este Álbum?")){
        $jQ.post('portfolio-bd.php',{op:'exc-alb',idalbum:idalbum},function(r){
            if(r=='true'){               
                location.href="portfolio.php?p=albuns&msg=del";
            }else{
                alert('Não foi possivel excluir este Álbum.');
                $jQ("#albumfotos").css("border","1px solid transparent");
                $jQ("#albumfotos").css("background-color","#f2f2f2");
                $jQ("#albumfotos").css("pointerEvents","auto");
                $jQ("#subalbumfotos").css("background-color","#FFF");
                $jQ("#subalbumfotos").css("pointerEvents","auto");                
            }
        });
    }else{
        $jQ("#albumfotos").css("border","1px solid transparent");
        $jQ("#albumfotos").css("background-color","#f2f2f2");
        $jQ("#albumfotos").css("pointerEvents","auto");
        $jQ("#subalbumfotos").css("background-color","#FFF");
        $jQ("#subalbumfotos").css("pointerEvents","auto");        
    }
}

function mostrarExcluir(){
    $jQ('.btnexcluir').toggle();
    link = $jQ('#togglexcluir');
}

/* - - - - - - - - - - - - - - - - - - - - - - - - -EDIT ALBUM DE FOTOS- - - - */
function trocarImagem(){
    $jQ('#boxfoto').toggle();
    $jQ('#boxfilefoto').toggle();
    $jQ('#trocarimg').val('1');
    
}
function cancelarTrocarImagem(){
    $jQ('#boxfoto').toggle();
    $jQ('#boxfilefoto').toggle();
    $jQ('#trocarimg').val('0');
    
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - ORDENAR ALBUNS- - - - */

//Exclui Foto pelo ID
function salvarOrdemAlbuns(idsort,idpai){
    
    var sort = $jQ("#"+idsort);
//    alert(sort.attr('id'));
    var ordem = '';
    
    sort.children('li').each(function(){
        ordem += $jQ(this).attr('idalbum')+',';
    });
//    alert(retorno);

    $jQ.post('portfolio-bd.php',{op:'ord-alb',ordem:ordem,idpai:idpai},function(r){
        if(r=='true'){
            //SALVO
//            $jQ(".tfoto[idfoto="+codigo+"]").hide();
        }else{
            //Erro ao Salvar
            
        }
    });
}
function salvarOrdemVideos(){    
    var sort = $jQ("#sortablev");
    var ordem = '';    
    sort.children('.livideo').each(function(){
        ordem += $jQ(this).attr('idvid')+',';
    });
    $jQ.post('portfolio-bd.php',{op:'ord-vid',ordem:ordem},function(r){
        if(r=='true'){
            //SALVO
        }else{
            //Erro ao Salvar            
        }
    });
}
