var $jQ = jQuery.noConflict();

jQuery(document).ready(function($) {
    
    //Iniciar LightBox
    initLightbox();     
    
    //Iniciar Lista Sortable
    $("#sortable0").sortable({
        update: function(){
            salvarOrdemDeps();
        },
        //axis: "y",
        cursor: "move",
        disabled: true
    });

    //Mostrar Miniaturas dos Depoimentos
    $('.itemdep').hover(
        function(){ 
            $(this).find(".thumbimg").toggle();
        },
        function(){
            $(this).find(".thumbimg").toggle();
//            var alb = $(this).attr('idalbum');
//            $('.ordthumb[idalbum='+alb+']').toggle();     
        }                
    );   
    //Botão Mostrar Sub-albuns em orderm de fotos
    $('#btnitemdepmove').click(function(){
        $(".itemdepmove").toggle();
        $(".itemcontrole").toggle();
        tex = $("#btnitemdepmove").find("#btntext");
        texto = tex.text();
        if(texto === "Organizar depoimentos"){
            tex.text("Finalizar");
            $("#sortable0").sortable("option","disabled",false);
        }else{
            $("#sortable0").sortable("option","disabled",true);
            tex.text("Organizar depoimentos");
        }
    });
        
        
    //LINKS DO MENU
    $('#linkdeps').click(function(){ window.location = 'depoimentos.php?p=deps'; });
    $('#linkordemdep').click(function(){ window.location = 'depoimentos.php?p=ordemdep'; });
    $('#linkadddep').click(function(){ window.location = 'depoimentos.php?p=adddep'; });
   
    
    //BOTAO Submit ADD DEPOIMENTO
    $('#submitadddep').click(function(){ 
        if($("#texto1").val() == ""){        
            alert("Informe o Autor do depoimento");
            $("#texto1").focus();
            return false;
        }

        document.formadddep.action = "depoimentos-bd.php";
        document.formadddep.submit();
    });
    $('#canceladddep').click(function(){ window.location = 'depoimentos.php?p=deps'; });
    
    //BOTAO Submit EDIT DEPOIMENTO
    $('#submiteditdep').click(function(){ 
        if($("#texto1").val() == ""){        
            alert("Informe o Autor do depoimento");
            $("#texto1").focus();
            return false;
        }            
        document.formeditdep.action = "depoimentos-bd.php";
        document.formeditdep.submit();
    });
    
    $('#canceleditdep').click(function(){ window.location = 'depoimentos.php?p=deps'; });
    
});

//Exclui excluirDepoimento pelo ID
function excluirDepoimento(codigo){
    
    var li = $jQ(".lisort[iddep="+codigo+"]");
    var di = $jQ(".itemdep[iddep="+codigo+"]");
    di.css({border:"1px solid red",background:"#FAAAAA"});
    di.css("pointerEvents","none");
    if(confirm("Deseja excluir permanentemente este depoimento?")){
                        
        $jQ.post('depoimentos-bd.php',{op:'exc-dep',iddep:codigo},function(r){
//            alert (r);
            if(r=='true'){
                //Excluido
                li.remove();                
            }else{
                //Erro ao excluir
                di.css("pointerEvents","auto");
                di.css({border:"1px solid #ccc",background:"white"});                  
            }
        });
        
    }else{
        di.css("pointerEvents","auto");
        di.css({border:"1px solid #ccc",background:"white"});   
    }   
}

function mostrarExcluir(){
    $jQ('.btnexcluir').toggle();
    link = $jQ('#togglexcluir');
}

function deplermais(id){
    var aux = $jQ(".alermais[iddep="+id+"]");
    val = aux.attr("value");
    if(val === "0"){
        $jQ(".adescricao[iddep="+id+"]").css({
            height:"auto",
            "white-space": "normal"
        });
        aux.attr("value","1");
        aux.text("Menos");
    }else{
        $jQ(".adescricao[iddep="+id+"]").css({
            height:"14px",
            "white-space": "nowrap"
        });        
        aux.attr("value","0");
        aux.text("Ler mais");
    }
}

/* - - - - - - - - - - - - - - - - - - - - - - - - -EDIT DEPOIMENTOS - - - - */
function trocarImagem(){
    $jQ('#boxfoto').toggle();
    $jQ('#boxfilefoto').toggle();
    $jQ('#trocarimg').val('1');
    
}
function cancelarTrocarImagem(){
    $jQ('#boxfoto').toggle();
    $jQ('#boxfilefoto').toggle();
    $jQ('#trocarimg').val('0');
    
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - ORDENAR DEPOIMENTOS- - - - */

//Exclui Foto pelo ID
function salvarOrdemDeps(){
    
    var sort = $jQ("#sortable0");
//    alert(sort.attr('id'));
    var ordem = '';
    
    sort.children('li').each(function(){
        ordem += $jQ(this).attr('iddep')+',';
    });
//    alert(ordem);

    $jQ.post('depoimentos-bd.php',{op:'ord-dep',ordem:ordem},function(r){
        if(r=='true'){
            //SALVO
//            $jQ(".tfoto[idfoto="+codigo+"]").hide();
        }else{
            //Erro ao Salvar
            
        }
    });

}
