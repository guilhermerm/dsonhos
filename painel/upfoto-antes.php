<?php
session_start();
include_once './lib/security.php';
include_once './lib/conection.php';

//if(!isset($_SESSION["index"])){
//    header("Location: index.php?m=1");
//}
$idalbum = 0;
if(isset($_GET["a"])){
    $idalbum = $_GET["a"];
}

if ($idalbum != 0){
    $sql = "SELECT a1.id, a1.idpai, a1.texto1, IFNULL((SELECT a2.texto1 FROM albuns as a2 WHERE a2.id = a1.idpai),0) as texto2 FROM albuns as a1  WHERE a1.id = $idalbum";
    $res = mysql_query($sql, $con);
    
    if(mysql_num_rows($res) > 0){
        $row = mysql_fetch_array($res);
        $id     = $row['id'];
        $texto1 = utf8_decode($row['texto1']);
        $texto2 = utf8_decode($row['texto2']);    
    }else{
        $idalbum = 0;        
    }
}

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
        <title>Login - Painel D'Sonhos</title>
        
        <link rel="StyleSheet" type="text/css" href="css/main.css"/>                 
        <link rel="StyleSheet" type="text/css" href="css/upfoto.css"/>                 
        <script language="javascript" type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
        
        <!-- Bootstrap -->
        <link rel="StyleSheet" type="text/css" href="css/bootstrap-theme.min.css"/>
        <link rel="StyleSheet" type="text/css" href="css/bootstrap.min.css"/>
        <script language="javascript" type="text/javascript" src="js/bootstrap.min.js"></script>
        
        <!-- SWFupload -->
        <link href="css/swfdefault.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/swfupload.js"></script>
        <script type="text/javascript" src="js/swfupload.queue.js"></script>
        <script type="text/javascript" src="js/swffileprogress.js"></script>
        <script type="text/javascript" src="js/swfhandlers.js"></script>        
        <script type="text/javascript">
		var swfu;

		window.onload = function() {
			var settings = {
				flash_url : "js/swfupload.swf",
				upload_url: "lib/swfupload.php",
				post_params: {"PHPSESSID" : "<?php echo session_id(); ?>", "idalbum":"<?php echo $idalbum; ?>" , "grouptmp":"<?php echo date("ymdHis"); ?>"},
				file_size_limit : "10 MB",
				file_types : "*.jpg",
				file_types_description : "All Files",
				file_upload_limit : 50,
				file_queue_limit : 0,
				custom_settings : {
					progressTarget : "fsUploadProgress",
					cancelButtonId : "btnCancel"
				},
				debug: false,

				// Button settings
				button_image_url: "img/button-upfoto.png",
				button_width: "125",
				button_height: "29",
				button_placeholder_id: "spanButtonPlaceHolder",
				button_text: '<span class="theFont">Escolher fotos</span>',
				button_text_style: ".theFont { font-size: 14; }",
				button_text_left_padding: 12,
				button_text_top_padding: 4,
				
				// The event handler functions are defined in handlers.js
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,
				queue_complete_handler : queueComplete	// Queue plugin event
			};

			swfu = new SWFUpload(settings);
	     };
	</script>        
        
        
        <link rel="SHORTCUT ICON" href="../imagens/favicon.png" type="image/x-icon" />
    </head>
    <body>
        <?php
            include_once 'php/content/barratopo.php';
        ?>                
<!--        <div id="barrainicio">
            <div class="corpo">
            
            </div>
        </div>-->
        <div id="conteudo">
            <div class="corpo">
        <?php
            if ($idalbum != 0){
        ?>   
                <?php
                    if ($texto2 == '0'){
                       echo "<div id='conteudo'><h3>$texto1</h3>";
                    }else{
                       echo "<div id='conteudo'><h3>$texto2 <small>> <b>$texto1</b></small></h3>";                
                    }
                ?>   
                
                <div class="panel panel-default">                    
                    <div class="panel-heading"><b>Enviar fotos:</b></div>
                    <div class="panel-body">
                        <div id="upesquerda">                            
                            <form id="form1" action="upfoto.php?a=<?php echo $idalbum?>" method="post" enctype="multipart/form-data">                               
                                <div>
                                    <span id="spanButtonPlaceHolder"></span>
                                    <input id="btnCancel" type="button" value="Cancelar envio" onclick="swfu.cancelQueue();" disabled="disabled" style="margin-left: 2px; font-size: 8pt; height: 29px;" />
                                </div>
                                <div class="fieldset flash" id="fsUploadProgress">
                                    <span class="legend">Fila de envios:</span>
                                </div>
                                <div id="divStatus"></div>

                            </form>
                        </div>
                        <div id="updireita">
                            <div id="upinformacoes" class='panel panel-default'>
                                <div class='panel-body' id='upinfotit'>
                                    Informações:                                    
                                </div>
                                <div class='panel-footer'>
                                    <b>Passo 1</b> - Clique em "Escolher fotos" para selecionar as fotos a serem enviadas.
                                    <div class="subinfo">
                                        - Máximo de <b>50 fotos</b> por vez.
                                        <br> - Máximo <b>10MB</b> por foto. (Quanto maior a foto mais lento será o envio.)
                                        <br> - As fotos serão automaticamente redimensionadas para 800x600 para não pesarem o servidor.
                                    </div>
                                    <b>Passo 2</b> - <b>Aguarde</b> as fotos serem enviadas
                                    <div class="subinfo">
                                        - Será inserido automaticamente a <b>marca d'agua "D'Sonhos www.dsonhos.com"</b>
                                        <br> - As fotos <b>irão para o início do álbum</b> na ordem que forem enviadas (A primeira foto enviada será a primeira do álbum).
                                        <br> - <b>Não feche esta janela</b> antes de enviar todas as fotos. Para parar clique em "Cancelar envio".
                                        <br> - Fotos enviadas já aparecem no álbum.
                                    </div>
                                    <b>Passo 3</b> - Processo Finalizado.
                                    <div class="subinfo">
                                        - Depois de enviadas todas as fotos clique em "Pronto" para voltar para o álbum.
                                    </div>
                                </div>                                
                            </div>
                            <div class="buttonlinha ">
                                <a href="portfolio.php?p=album&album=<?php echo $idalbum?>" title="Aguarde que todas fotos sejam enviadas." class="btn btn-sm btn-success " style='width: 100px'><span class="glyphicon glyphicon-ok"></span> Pronto</a>
                                <span class="small"> Aguarde até que todas as fotos sejam enviadas.</span>
                            </div>
                        </div>

                    </div>
                </div>
        <?php
            }else{
        ?>   
             <div class="alert alert-danger"><b>Ops! </b> Esta página foi aberta incorretamente. Clique em Início ou em voltar no seu navegador.</div>     
        <?php
            }
        ?>   

            </div>
        </div>
        <?php
            include_once 'php/content/rodape.php';
        ?>   
    </body>
</html>
<script>
    $(document).ready(function() {
        //LINKS DO MENU
        $('#linkalbuns').click(function(){ window.location = 'portfolio.php?p=albuns'; });
        $('#linkaddalbum').click(function(){ window.location = 'portfolio.php?p=addalbum'; });
        $('#linkupfotos').click(function(){ window.location = 'portfolio.php?p=upfotos'; });
        $('#linkaddvideo').click(function(){ window.location = 'portfolio.php?p=addvideo'; });
        $('#linkvideos').click(function(){ window.location = 'portfolio.php?p=videos'; });
    });
</script>