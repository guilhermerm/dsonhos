<?php
error_reporting(6143);
//error_reporting(0);
session_start();
include_once './lib/security.php';
include_once './lib/conection.php';


$pastafotos = "../dsonhos/produtos/fotos/";

$page = 'prods';
if(isset($_GET["p"])){
    $page = $_GET["p"];       
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
        <title>Login - Painel D'Sonhos</title>
        
        <link rel="StyleSheet" type="text/css" href="css/main.css"/>
        <link rel="StyleSheet" type="text/css" href="css/produtos.css"/>
        <link rel="StyleSheet" type="text/css" href="css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
        <script language="javascript" type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/produtos.js"></script>
        <!--<script language="javascript" type="text/javascript" src="js/interface.js"></script>-->

         <!--LiteBox1.0--> 
        <link rel="stylesheet" type="text/css" href="css/lightbox.css" media="screen" />
        <script type="text/javascript" src="js/lightbox-2.6.min.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>  
<?php
    if($page=="addprod"||$page=="editprod"){
        $grouptmp = date("ymdHis");
        
        $idprod = '0';
        if($page=="editprod" && isset($_GET['prod'])){
            $idprod = $_GET['prod'];        
        }
?>
        <link href="css/swfdefault.css" rel="stylesheet" type="text/css" />    
        <script type="text/javascript" src="js/swfupload.js"></script>
        <script type="text/javascript" src="js/swfupload.queue.js"></script>
        <script type="text/javascript" src="js/swffileprogress.js"></script>
        <script type="text/javascript" src="js/swfhandlers.js"></script>        
        <script type="text/javascript">
		var swfu;
                window.onload = function() {
			var settings = {
				flash_url : "js/swfupload.swf",
				upload_url: "produtos-bd.php",
				post_params: {"PHPSESSID" : "<?php echo session_id(); ?>","op":"up-fto" , "idprod":"<?php echo $idprod; ?>" , "grouptmp":"<?php echo $grouptmp ?>"},
				file_size_limit : "10 MB",
				file_types : "*.jpg",
				file_types_description : "All Files",
				file_upload_limit : 10,
				file_queue_limit : 0,
				custom_settings : {
                                    progressTarget : "fsUploadProgress",
                                    cancelButtonId : "btnCancel",
				},
				debug: false,

				// Button settings
				button_image_url: "img/button-upfoto.png",
				button_width: "125",
				button_height: "29",
				button_placeholder_id: "spanButtonPlaceHolder",
				button_text: '<span class="theFont">Escolher fotos</span>',
				button_text_style: ".theFont { font-size: 14; }",
				button_text_left_padding: 12,
				button_text_top_padding: 4,
				
				// The event handler functions are defined in handlers.js
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,
				queue_complete_handler : queueComplete	// Queue plugin event
			};

			swfu = new SWFUpload(settings);
	     };
	</script>        
<?php        
    }//if($page)
?>          
        
        <!-- Bootstrap -->
        <link rel="StyleSheet" type="text/css" href="css/bootstrap-theme.min.css"/>
        <link rel="StyleSheet" type="text/css" href="css/bootstrap.min.css"/>
        <script language="javascript" type="text/javascript" src="js/bootstrap.min.js"></script>
        
        <!-- LiteBox1.0 -->
        <link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
        <script type="text/javascript" src="js/prototype.lite.js"></script>
        <script type="text/javascript" src="js/moo.fx.js"></script>
        <script type="text/javascript" src="js/litebox-1.0.js"></script>                             
        
        <link rel="SHORTCUT ICON" href="../imagens/favicon.png" type="image/x-icon" />
    </head>
    <body>
        <?php
            include_once 'php/content/barratopo.php';
        ?>                
        <div id="conteudo">
            <div class="corpo">
                <h3>Produtos</h3>
                <div id="menuesq">
                    <h4></h4>
                    <div class="btn-group-vertical">
                        <button type="button" class="btn btn-default btn-sm" id="linkprods"><span class="glyphicon glyphicon-align-justify"></span> Todos produtos</button>                           
                        <!--<button type="button" class="btn btn-default btn-sm" id="linkordemprod"><span class="glyphicon glyphicon-list"></span> Organizar produtos</button>-->
                        <button type="button" class="btn btn-default btn-sm" id="linkaddprod"><span class="glyphicon glyphicon-plus-sign"></span> Adic. novo produto</button>
                    </div>
                    <h4>Categorias</h4>
                    <div class="btn-group-vertical">
                        <button type="button" class="btn btn-default btn-sm" id="linkcats"><span class="glyphicon glyphicon-align-justify"></span> Todas categorias</button>
                        <button type="button" class="btn btn-default btn-sm" id="linkaddcat"><span class="glyphicon glyphicon-plus-sign"></span> Adic. nova categoria</button>
                    </div>
                </div>
                <div id="conteudodir">
                <?php
                    switch ($page){ 
                        case 'prods':
                ?>
                              
                    <div id="prodtop">
                        <h4 class="prodtit">Todos os Produtos</h4>
<!--                        <div id="prodcontrole">
                             <div id="btnitemprodmove" class="btn btn-default btn-sm" title="Mostrar controle para organizar os produtos">
                                 <span class='glyphicon glyphicon-move'></span> <span id='btntext'>Organizar produtos</span>
                             </div>                            
                        </div>-->
                    </div>
                    <?php 
                    if (isset($_GET['msg'])){ switch ($_GET['msg']) {
                      case 'added': ?> <div class="alert alert-success"><b>Sucesso!</b> Novo produto adicionado com sucesso!</div> <?php break;
                      case 'edited': ?> <div class="alert alert-info"><b>Salvo!</b> As novas informações foram salvas com sucesso!.</div><?php break;
                      case 'notprod': ?> <div class="alert alert-danger"><b>Não existente.</b> O produto parece não existir.</div><?php break;
                      case 'del': ?> <div class="alert alert-success"><b>Removido.</b> O produto foi excluído com sucesso.</div><?php break;
                      default : break;  } } 
                    ?> 
                    <div id="prods">
                    <?php
                            $sql = "SELECT  id, texto1, texto2
                                      FROM  prodcategorias
                                     WHERE  deletado = 0 
                                  ORDER BY  ordem ASC";
                            $res = mysql_query($sql, $con);
                            $c = 0; $catvazio = true;
                            while($row = mysql_fetch_array($res)){
                                $catvazio = false;
                                $cid     = $row['id'];
                                $ctexto1 = utf8_decode($row['texto1']);
                                $ctexto2 = utf8_decode($row['texto2']);
                            
                    ?>                                                 
                        <div class="catsprod">
                            <div class="titcatsprod">                                
                                <?php echo $ctexto1, ($ctexto2!=""?"<small> > $ctexto2</small>":"") ?>
                            </div>
                            <div class="prodscabecalho">
                                <div class="prodscabecalhonome">Produto</div>
                                <div class="prodscabecalhoverpreco">Mostrar preço</div>
                                <div class="prodscabecalhopreco">Preço</div>
                                <div class="prodscabecalhoqtdfotos">Fotos</div>
                                <div class="prodscabecalhocontrole"></div>
                                <div class="prodscabecalhomove"></div>
                            </div>
                            <div class="clear"></div>
                            <ul id="sortable<?php echo $c ?>" idcat="<?php echo $cid ?>">                            
                        <?php
                                $sql = "SELECT  p.id, p.texto1, p.texto2, p.texto3, verpreco,
                                                (SELECT foto FROM prodfotos pf WHERE pf.idprod = p.id AND pf.deletado = 0 LIMIT 0,1) as foto,
                                                (SELECT COUNT(*) FROM prodfotos pf WHERE pf.idprod = p.id AND pf.deletado = 0) as qtd
                                          FROM  produtos as p
                                         WHERE  p.deletado = 0 
                                           AND  p.categoria = $cid
                                      ORDER BY  p.texto1 ASC";
                                $res2 = mysql_query($sql, $con);
                                $prodvazio = true;
                                while($row2 = mysql_fetch_array($res2)){
                                    $prodvazio = false;
                                    $id       = $row2['id'];
                                    $texto1   = utf8_decode($row2['texto1']);
                                    $texto2   = utf8_decode($row2['texto2']);
                                    $texto3   = utf8_decode($row2['texto3']);                                
                                    $verpreco = ($row2['verpreco']=="0" ? "Sim" : "Não");
                                    $qtdfto   = (($row2['qtd']=="0"||$row2['qtd']=="") ? "Sem fotos" : $row2['qtd']);
                                    $thumb    = ($row2['foto']!="" ? $pastafotos."t_".$row2['foto'] : "img/t_nopfoto.png");                                                               
                        ?>
                                <li idprod='<?php echo $id?>' class='lisort'>
                                    <div idprod='<?php echo $id?>' class="itemprod">                          
<!--                                         <div class="thumbpic">
                                            <img src="<?php echo $thumb?>">
                                         </div>                                                        -->
                                         <div class="atitulo" title="<?php echo $texto1?>"><?php echo $texto1 ?></div>
                                         <div class="verpreco"><?php echo $verpreco ?></div>
                                         <div class="preco" title="<?php echo $texto3?>"><?php echo $texto3 ?>&nbsp;</div>
                                         <div class="qtdfotos" ><?php echo $qtdfto ?></div>
                                         <div class="itemcontrole"  style="display: block">
                                             <!--<div class="btn-group">-->
                                                 <a class="btn btn-info btn-xs" href="produtos.php?p=editprod&prod=<?php echo $id ?>" title="Editar" style="width:70px; margin: 0 2px 0 0"><span class="glyphicon glyphicon-edit"></span>Editar</a>
                                                 <a class="btn btn-danger btn-xs" onclick="return excluirProduto(<?php echo $id ?>)" title="Excluir" style="width:30px;">&nbsp;<span class="glyphicon glyphicon-trash"></span>&nbsp;</a>
                                                 <!--<div class="btn btn-default btn-xs" style="padding: 4px 3px 6px 6px;" title="Mover"><span class="glyphicon glyphicon-move"></span></div>-->
                                             <!--</div>-->                                                                      
                                         </div>                                                                      
                                         <div class="itemprodmove" style="display: none">
                                             <span class="glyphicon glyphicon-move"></span>
                                         </div>
                                    </div>
                                </li>
                        <?php  
                            }//while
                        ?>
                            </ul>
                        </div>
                        <?php  
                            if($prodvazio){
                        ?>
                                <div>
                                    <div class="alert alert-info"><b>Sem produtos </b>Ainda não foram cadastrados produtos para esta categoria.</div>
                                </div>
                        <?php  
                            }//if(albunvazio)
                        ?>
                    <?php  
                            $c++;
                        }//while
                    ?>
                    <?php  
                        if($catvazio){
                    ?>
                            <div>
                                <div class="alert alert-info"><b>Sem Categorias</b> Ainda não foram cadastrados Categorias e Produtos.</div>
                            </div>
                    <?php  
                        }//if(albunvazio)
                    ?>
                    </div>                                                
                    
                                
                <?php  break; case 'addprod': ?>
                    <h4>Adicionar um novo produto</h4>
                    <div class="panel panel-default">                    
                        <div class="panel-heading">Preencha o formulário abaixo:</div>
                        <div class="panel-body">
                            <?php if ($_GET['msg']=='error') { ?>
                            <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                           
                            <?php } ?> 
                            <?php                             
                                $sql2 = "SELECT  id, texto1
                                          FROM  prodcategorias 
                                         WHERE  deletado = 0
                                      ORDER BY  ordem ASC";
                                $res_cats = mysql_query($sql2, $con);
                                if($res_cats && mysql_num_rows($res_cats)>0){ 
                            ?> 
                                    

                            <form class="form-addprod" name="formaddprod" method="post" enctype="multipart/form-data">
                                <div class="addesquerda">
                                    <input type="hidden" id="op" name="op" value="add-prod"/>
                                    <input type="hidden" id="idprod" name="idprod" value=""/>
                                    <input type="hidden" id="grouptmp" name="grouptmp" value="<?php echo $grouptmp ?>"/>
                                    <div class="form-group">
                                        <label> Categoria:</label>
                                        <div id="selectcat">
                                            <select id="scat" name="scat" style="width: 250px">
                                                <option value="0" selected>Selecione uma categoria</option>
                                                <option disabled>- - - - - - - - -</option>
                                    <?php                             
                                            while($row = mysql_fetch_array($res_cats)){
                                                $id      = $row['id']; 
                                                $texto1  = utf8_decode($row['texto1']);
                                    ?> 
                                                <option value="<?php echo $id?>"><?php echo $texto1?></option>
                                    <?php                             
                                            }
                                    ?> 
                                            </select>
                                            
                                        </div>
                                        <!--<span class="help-block">Nome dos autores do produtos.</span>-->
                                    </div>                                    
                                    <div class="form-group">
                                        <label> Nome:</label>
                                        <input type="text" id="texto1" name="texto1" class="form-control" placeholder="Nome" style="width: 250px">
                                        <!--<span class="help-block">Nome dos autores do produtos.</span>-->
                                    </div>                                    
                                    <div class="form-group">
                                        <label> Descrição:</label>
                                        <textarea id="texto2" name="texto2" class="form-control" placeholder="Descrição" style="height:100px;"></textarea>
    <!--                                    <span class="help-block">(Opcional)</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label > Preço:</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <label style="margin: 0; padding: 0; font-weight: normal;" onclick="document.getElementById('texto3').focus()">
                                                    <input type="checkbox" style="padding:0px;" id="verpreco" name="verpreco"> Mostrar preço
                                                </label>
                                            </span>
                                            <input type="text" id="texto3" name="texto3" class="form-control" placeholder="Preço">
                                        </div>
                                        <span class="help-block">Ex: "R$3,50" ou "De: R$6,00 Por: R$4,50".</span>
                                    </div>
                                </div>
                            </form>   
                                <div class="adddireita">
                                    <div class="form-group">
                                     
                                        <label>Fotos:</label>
                                        <div id="boxprodfotos">
                                            <small>Sem fotos...<br>&nbsp;</small>
                                        </div>
                                        <label>Enviar fotos:</label>
                                        <form id="form1" action="produtos-bd.php?grouptmp=<?php echo $grouptmp?>" method="post" enctype="multipart/form-data">                               
                                            <div>
                                                <span id="spanButtonPlaceHolder"></span>
                                                <input id="btnCancel" type="button" value="Cancelar envio" onclick="swfu.cancelQueue();" disabled="disabled" style="margin-left: 2px; font-size: 8pt; height: 29px;" />
                                            </div>
                                            <div class="fieldset flash" id="fsUploadProgress">
                                                <span class="legend">Fila de envios:</span>
                                            </div>
                                            <div id="divStatus"></div>
                                            <small><span class="glyphicon glyphicon-warning-sign"></span>Aguarde até que todas fotos sejam enviadas para clicar em Adicionar.</small>
                                        </form>                                        
                                        <!--<input type="file" id="filefoto" name="filefoto" class="form-control">-->
                                        <!--<span class="help-block">Dê preferência para uma foto do bolo.</span>-->
                                    </div>
                                </div>
                                <div class="addcontrol">
                                    <div class="form-group">
                                        <div class="buttonlinha ">
                                            <input type="button" id="submitaddprod" name="submitaddprod" value="Adicionar" class="btn btn-primary " >    
                                            <a href="produtos.php" class="btn btn-default">Cancelar</a>                                                                       
                                        </div>                                    
                                    </div>
                                </div>
                            <?php                             
                                }else{
                            ?>                             
                            <div class="alert alert-info"><b>Categorias:</b> Primeiro deve-se ter uma categoria antes de adicionar um produto. <a href="produtos.php?p=addcat" style="float:right">Adicionar uma categoria</a>.</div>                                    
                            <?php                             
                                }
                            ?>                             
                        </div>
                    </div>   
 
               <?php  break; case 'editprod' : ?>
                   <?php     
                        $erro = false;
                        if(isset($_GET['prod'])){
                            $idprod = $_GET['prod'];
                            $sql = "SELECT  id, texto1, texto2, texto3, verpreco, categoria
                                      FROM  produtos 
                                     WHERE  deletado = 0 
                                       AND  id = '$idprod'
                                  ORDER BY  ordem ASC";
                            $res = mysql_query($sql, $con);
                            $sql2 = "SELECT  id, texto1
                                      FROM  prodcategorias 
                                     WHERE  deletado = 0
                                  ORDER BY  ordem ASC";
                            $res_cats = mysql_query($sql2, $con);
                            if($res && mysql_num_rows($res)>0 && $res_cats && mysql_num_rows($res_cats)>0){
                                while($row = mysql_fetch_array($res)){
                                    $id        = $row['id']; 
                                    $texto1    = utf8_decode($row['texto1']);
                                    $texto2    = utf8_decode($row['texto2']);
                                    $texto3    = utf8_decode($row['texto3']);
                                    $verpreco  = $row['verpreco']; 
                                    $categoria = $row['categoria']; 
                                }
                    ?>                                  
                    <h4>Editar Produto</h4>
                    <div class="panel panel-default">                    
                        <div class="panel-heading">Preencha o formulário abaixo para editar :</div>
                        <div class="panel-body">
                            <?php if ($_GET['msg']=='error') { ?>
                                <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                           
                            <?php } ?> 
                            <form class="formeditprod" name="formeditprod" method="post" enctype="multipart/form-data">
                                <div class="addesquerda">                                
                                    <input type="hidden" id="op" name="op" value="edt-prod"/>
                                    <input type="hidden" id="idprod" name="idprod" value="<?php echo $idprod?>"/>
                                    <div class="form-group">
                                        <label> Categoria:</label>
                                        <div id="selectcat">
                                            <select id="scat" name="scat" style="width: 250px">
                                                <option value="0">Selecione uma categoria</option>
                                                <option disabled>- - - - - - - - -</option>
                                    <?php                             
                                            while($row2 = mysql_fetch_array($res_cats)){
                                                $cid      = $row2['id']; 
                                                $ctexto1  = utf8_decode($row2['texto1']);
                                    ?> 
                                                <option value="<?php echo $cid?>"<?php echo ($categoria==$cid?"selected":"") ?>><?php echo $ctexto1?></option>
                                    <?php                             
                                            }
                                    ?> 
                                            </select>
                                            
                                        </div>
                                        <!--<span class="help-block">Nome dos autores do produtos.</span>-->
                                    </div>                                    
                                    <div class="form-group">
                                        <label> Produto:</label>
                                        <input type="text" id="texto1" name="texto1" class="form-control" placeholder="Produto" value="<?php echo $texto1?>" style="width:250px">
                                        <!--<span class="help-block">Nome dos autores do depoimento.</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label> Descrição:</label>
                                        <textarea id="texto2" name="texto2" class="form-control" placeholder="Descrição" style="height:100px;"><?php echo $texto2?></textarea>
    <!--                                    <span class="help-block">(Opcional)</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label > Preço:</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <label style="margin: 0; padding: 0; font-weight: normal;" onclick="document.getElementById('texto3').focus()">
                                                    <input type="checkbox" style="padding:0px;" id="verpreco" name="verpreco" <?php echo ($verpreco=="0"?"checked":"") ?>> Mostrar preço
                                                </label>
                                            </span>
                                            <input type="text" id="texto3" name="texto3" class="form-control" placeholder="Preço" value="<?php echo $texto3?>">
                                        </div>
                                        <span class="help-block">Ex: "R$3,50" ou "De: R$6,00 Por: R$4,50".</span>
                                    </div>
                                </div>
                            </form>
                                <div class="adddireita">                            
                                    <div class="form-group">                                     
                                        <label>Fotos:</label>
                                        <div id="boxprodfotos">
                                            <center><small><img src="images/loading.gif"/><br>&nbsp;</small></center>
                                        </div>
                                        <script>loadFotoToBox()</script>
                                        <label>Enviar fotos:</label>
                                        <form id="form1" action="produtos-bd.php" method="post" enctype="multipart/form-data">                               
                                            <div id='upftoinfo'>
                                                <small><span class="glyphicon glyphicon-th-large"></span> As fotos ficarão com tamanho máximo de 300x300.</small><br>
                                                <small><span class="glyphicon glyphicon-upload"></span> Poste poucas fotos por produto.</small><br>
                                                <small><span class="glyphicon glyphicon-ok"></span> Fotos bem sucedidas serão carregadas automaticamente.</small><br>
                                                <small><span class="glyphicon glyphicon-warning-sign"></span> Aguarde até que todas fotos sejam enviadas antes de Salvar.</small>
                                            </div>
                                            <div>
                                                <span id="spanButtonPlaceHolder"></span>
                                                <input id="btnCancel" type="button" value="Cancelar envio" onclick="swfu.cancelQueue();" disabled="disabled" style="margin-left: 2px; font-size: 8pt; height: 29px;" />
                                            </div>
                                            <div class="fieldset flash" id="fsUploadProgress">
                                                <span class="legend">Fila de envios:</span>
                                            </div>
                                            <div id="divStatus"></div>
                                        </form>
                                    </div>                                    
                                </div>
                                <div class="addcontrol">                                
                                    <div class="form-group">
                                        <div class="buttonlinha ">
                                            <input type="button" id="submiteditprod" name="submiteditprod" value="Salvar" class="btn btn-primary " >                                
                                            <a href="produtos.php?p=prods" class="btn btn-default">Cancelar</a>                                   
                                        </div>                                    
                                    </div>
                                </div>
                                
                        </div>
                    </div>
                    <?php }else{
                            $erro = true;
                          }
                        }else{
                            $erro = true;                            
                        }
                      if ($erro) {?>
                        <h4>Editar Produtos</h4>
                        <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                      
                    <?php } ?>
                                                                              
                <?php  break; case 'addcat': ?>
                    <h4>Adicionar um nova categoria</h4>
                    <div class="panel panel-default">                    
                        <div class="panel-heading">Preencha o formulário abaixo:</div>
                        <div class="panel-body">
                            <?php if ($_GET['msg']=='error') { ?>
                            <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                           
                            <?php } ?> 
                            <form class="form-addcat" name="formaddcat" method="post" enctype="multipart/form-data">
                                <div class="addesquerda"  style="width: auto;">
                                    <input type="hidden" name="op" value="add-cat"/>                                   
                                    <div class="form-group">
                                        <label> Nome da categoria:</label>
                                        <input type="text" id="texto1" name="texto1" class="form-control" placeholder="Categoria" style="width: 250px">
                                    </div>                                    
                                    <div class="form-group">
                                        <label> Breve sub-título:</label>
                                        <input type="text" id="texto2" name="texto2" class="form-control" placeholder="Sub-tílulo">
                                        <!--<textarea id="texto2" name="texto2" class="form-control" placeholder="Descrição da categoria" style="height:100px;"></textarea>-->
    <!--                                    <span class="help-block">(Opcional)</span>-->
                                    </div>                                    
                                </div>                                
                                <div class="addcontrol">
                                    <div class="form-group">
                                        <div class="buttonlinha ">
                                            <input type="button" id="submitaddcat" name="submitaddcat" value="Adicionar" class="btn btn-primary " >    
                                            <a href="produtos.php?p=cats" class="btn btn-default">Cancelar</a>                                                                       
                                        </div>                                    
                                    </div>
                                </div>
                            </form>                                
                        </div>
                    </div>   
                <?php                       
                    break; case 'cats':
                ?>                              
                    <div id="cattop">
                        <h4 class="cattit">Categorias</h4>
                        <div id="catcontrole">
                             <div id="btncatmove" class="btn btn-default btn-sm" title="Mostrar controle para organizar as categorias">
                                 <span class='glyphicon glyphicon-move'></span> <span id='btntext'>Organizar categorias</span>
                             </div>                            
                        </div>
                    </div>
                    <?php 
                    if (isset($_GET['msg'])){ switch ($_GET['msg']) {
                      case 'added': ?> <div class="alert alert-success"><b>Sucesso!</b> Nova categoria adicionada com sucesso!</div> <?php break;
                      case 'edited': ?> <div class="alert alert-info"><b>Salvo!</b> As novas informações foram salvas com sucesso!.</div><?php break;
                      case 'notcat': ?> <div class="alert alert-danger"><b>Não existente.</b> Esta categoria parece não existir.</div><?php break;
                      case 'del': ?> <div class="alert alert-success"><b>Removido.</b> A categoria foi excluída com sucesso.</div><?php break;
                      default : break;  } } 
                    ?> 
                    <div id="cats">
                        <ul id="sortable1">
                            
                    <?php
                            $sql = "SELECT  pc.id, pc.texto1, pc.texto2, (SELECT count(*) FROM produtos as p WHERE pc.id = p.categoria AND p.deletado = 0)as qtd
                                      FROM  prodcategorias as pc
                                     WHERE  pc.deletado = 0 
                                  ORDER BY  pc.ordem ASC";
                            $res = mysql_query($sql, $con);
                            $c = 0; $catvazio = true;
                            while($row = mysql_fetch_array($res)){
                                $catvazio = false;
                                $id     = $row['id'];
                                $cqtd     = $row['qtd'];
                                $texto1 = utf8_decode($row['texto1']);
                                $texto2 = utf8_decode($row['texto2']);                                                                
                    ?>
                            <li idcat='<?php echo $id?>' class='catlisort'>
                                <div idcat='<?php echo $id?>' class="itemcat">                                                               
                                     <div class="itemcatmove" style="display: none;">
                                         <span class="glyphicon glyphicon-move"></span>
                                     </div>
                                     <div class="atitulo" title="<?php echo $texto1?>"><?php echo $texto1 , "<small> ($cqtd produto",($cqtd>=1?"s":""),")</small>"?></div>
                                     <div class="itemcontrole">
                                     <?php if($cqtd==0){ ?>
                                             <a class="btn btn-danger btn-xs" onclick="return <?php echo ($cqtd=='0'?"excluirCategoria($id)":"excluirCategoriaCheia($id)") ?>" title="Excluir" style="margin:0 3px 0 0;"><span class="glyphicon glyphicon-trash"></span>Excluir</a>
                                     <?php } ?>
                                         <a class="btn btn-info btn-xs" href="produtos.php?p=editcat&cat=<?php echo $id ?>" title="Editar"><span class="glyphicon glyphicon-edit"></span>Editar</a>
                                     </div>                                                                      
                                     <?php if($texto2!=''){ ?>
                                         <div class="adescricao" idcat='<?php echo $id ?>'>
                                             <?php echo $texto2?>
                                         </div>
                                         <div class="linklermais"  style="margin: 5px 0 0 7px;">                                          
                                             [<a href="javascript:void(0)" class="alermais" idcat='<?php echo $id?>' value="0" onclick="return catlermais(<?php echo $id?>)">Ler mais</a>]
                                         </div>
                                     <?php } ?>
                                </div>
                            </li>
                <?php  
                    }//while
                ?>
                        </ul>
                    </div>                                                
                    
                <?php  
                    if($catvazio){
                ?>
                        <div>
                            <div class="alert alert-info"><b>Nenhuma Categoria: </b>Ainda não foram cadastrados categorias.</div>
                        </div>
                <?php  
                    }//if(catvazio)
                ?>                                             
                
               <?php  break; case 'editcat' : ?>
                   <?php     
                        $erro = false;
                        if(isset($_GET['cat'])){
                            $idcat = $_GET['cat'];
                            $sql = "SELECT  id, texto1, texto2
                                      FROM  prodcategorias 
                                     WHERE  deletado = 0 
                                       AND  id = '$idcat'
                                  ORDER BY  ordem ASC";
                            $res = mysql_query($sql, $con);
                            if($res && mysql_num_rows($res)>0){
                                while($row = mysql_fetch_array($res)){
                                    $id      = $row['id']; 
                                    $texto1  = utf8_decode($row['texto1']);
                                    $texto2  = utf8_decode($row['texto2']);
                                }
                    ?>                                  
                    <h4>Editar Categoria</h4>
                    <div class="panel panel-default">                    
                        <div class="panel-heading">Altere o formulário abaixo para editar :</div>
                        <div class="panel-body">
                            <?php if ($_GET['msg']=='error') { ?>
                                <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                           
                            <?php } ?> 
                            <form class="formeditcat" name="formeditcat" method="post" enctype="multipart/form-data">
                                <div class="addesquerda" style="width: auto;">                                
                                    <input type="hidden" name="op" value="edt-cat"/>
                                    <input type="hidden" name="id" value="<?php echo $id?>"/>
                                    <div class="form-group">
                                        <label> Categoria:</label>
                                        <input type="text" id="texto1" name="texto1" class="form-control" placeholder="Categoria" value="<?php echo $texto1?>" style="width:250px">
                                        <!--<span class="help-block">Nome dos autores do depoimento.</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label> Breve sub-título:</label>
                                        <input type="text" id="texto2" name="texto2" class="form-control" placeholder="Sub-tílulo" value="<?php echo $texto2?>">
                                        <!--<textarea id="texto2" name="texto2" class="form-control" placeholder="Descrição" style="height:100px;"><?php echo $texto2?></textarea>-->
    <!--                                    <span class="help-block">(Opcional)</span>-->
                                    </div>
                                </div>                                
                                <div class="addcontrol">                                
                                    <div class="form-group">
                                        <div class="buttonlinha ">
                                            <input type="button" id="submiteditcat" name="submiteditcat" value="Salvar" class="btn btn-primary " >                                
                                            <a href="produtos.php?p=cats" class="btn btn-default">Cancelar</a>                                   
                                        </div>                                    
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                    <?php }else{
                            $erro = true;
                          }
                        }else{
                            $erro = true;                            
                        }
                      if ($erro) {?>
                        <h4>Editar Categoria</h4>
                        <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                      
                    <?php } ?>
                                       
                                          
                      
                      
                <?php  
                    break;              
                    }//switch($page)  
                ?>
                   
                </div><!--id="conteudodir"-->                
            </div>
        </div>   
        <?php
            include_once 'php/content/rodape.php';
        ?>   
    </body>
</html>