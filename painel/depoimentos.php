<?php
session_start();
include_once './lib/security.php';
include_once './lib/conection.php';

error_reporting(0);

$pastafotos = "../dsonhos/depoimentos/fotos/";

//if(!isset($_SESSION["index"])){
//    header("Location: index.php?m=1");
//}
$page = 'deps';
if(isset($_GET["p"])){
    $page = $_GET["p"];       
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
        <title>Login - Painel D'Sonhos</title>
        
        <link rel="StyleSheet" type="text/css" href="css/main.css"/>
        <link rel="StyleSheet" type="text/css" href="css/depoimentos.css"/>
        <link rel="StyleSheet" type="text/css" href="css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
        <script language="javascript" type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/depoimentos.js"></script>
        <script language="javascript" type="text/javascript" src="js/interface.js"></script>
        
        <!-- Bootstrap -->
        <link rel="StyleSheet" type="text/css" href="css/bootstrap-theme.min.css"/>
        <link rel="StyleSheet" type="text/css" href="css/bootstrap.min.css"/>
        <script language="javascript" type="text/javascript" src="js/bootstrap.min.js"></script>
        
        <!-- LiteBox1.0 -->
        <link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
        <script type="text/javascript" src="js/prototype.lite.js"></script>
        <script type="text/javascript" src="js/moo.fx.js"></script>
        <script type="text/javascript" src="js/litebox-1.0.js"></script>
        
        <link rel="SHORTCUT ICON" href="../imagens/favicon.png" type="image/x-icon" />
    </head>
    <body>
        <?php
            include_once 'php/content/barratopo.php';
        ?>                
<!--        <div id="barrainicio">
            <div class="corpo">
            
            </div>
        </div>-->
        <div id="conteudo">
            <div class="corpo">
                <h3>Depoimentos</h3>
                <div id="menuesq">
                    <h4></h4>
                    <div class="btn-group-vertical">
                        <button type="button" class="btn btn-default btn-sm" id="linkdeps"><span class="glyphicon glyphicon-th"></span> Todos depoimentos</button>                           
                        <!--<button type="button" class="btn btn-default btn-sm" id="linkordemdep"><span class="glyphicon glyphicon-list"></span> Organizar depoimentos</button>-->
                        <button type="button" class="btn btn-default btn-sm" id="linkadddep"><span class="glyphicon glyphicon-plus-sign"></span> Adic. novo depoimento</button>
                    </div>
                </div>
                <div id="conteudodir">
                <?php
                    switch ($page){ 
                        case 'deps':
                ?>
                              
                    <div id="deptop">
                        <h4 class="deptit">Todos Depoimentos</h4>
                        <div id="depcontrole">
                             <div id="btnitemdepmove" class="btn btn-default btn-sm" title="Mostrar controle para organizar os depoimentos">
                                 <span class='glyphicon glyphicon-move'></span> <span id='btntext'>Organizar depoimentos</span>
                             </div>                            
                        </div>
                    </div>
                    <?php if (isset($_GET['msg'])){ switch ($_GET['msg']) {
                           case 'added': ?> <div class="alert alert-success"><b>Sucesso!</b> Novo depoimento adicionado com sucesso!</div> <?php break; ?>
                    <?php  case 'edited': ?> <div class="alert alert-info"><b>Salvo!</b> As novas informações foram salvas com sucesso!.</div><?php break; ?> 
                    <?php  case 'notdep': ?> <div class="alert alert-danger"><b>Não existente.</b> O depoimento parece não existir mais.</div><?php break; ?> 
                    <?php  case 'del': ?> <div class="alert alert-success"><b>Removido.</b> O depoimento foi excluído com sucesso.</div><?php break; ?> 
                    <?php  default : break;  } } ?> 
                    <div id="deps">
                        <ul id="sortable0">
                            
                    <?php
                            $sql = "SELECT  id, texto1, texto2, texto3, foto
                                      FROM  depoimentos
                                     WHERE  deletado = 0 
                                  ORDER BY  ordem ASC";
                            $res = mysql_query($sql, $con);
                            $c = 0; $depvazio = true;
                            while($row = mysql_fetch_array($res)){
                                $depvazio = false;
                                $id     = $row['id'];
                                $texto1 = utf8_decode($row['texto1']);
                                $texto2 = utf8_decode($row['texto2']);
                                $texto3 = utf8_decode($row['texto3']);                                
                                $thumb  =  ($row['foto']!="0" ? $pastafotos."t_".$row['foto'] : "img/t_nofoto.png");
                                                                
                    ?>
                            <li iddep='<?php echo $id?>' class='lisort'>
                                <div iddep='<?php echo $id?>' class="itemdep">                          
                                     <div class="thumbimg" style="display: none">
                                        <img src="<?php echo $thumb?>">
                                     </div>                                                        
                                     <div class="atitulo" title="<?php echo $texto1?>"><?php echo $texto1 ?></div>
                                     <div class="adescricao" iddep='<?php echo $id ?>'>
                                         <?php echo $texto2?>
                                     </div>         
                                     <div class="linklermais" >                                             
                                         [<a href="javascript:void(0)" class="alermais" iddep='<?php echo $id?>' value="0" onclick="return deplermais(<?php echo $id?>)">Ler mais</a>]
                                     </div>
                                     <div class="itemcontrole">                                 
                                         <a class="btn btn-info btn-xs" href="depoimentos.php?p=editdep&dep=<?php echo $id ?>" title="Editar"><span class="glyphicon glyphicon-edit"></span>Editar</a>
                                         <div class="btn btn-danger btn-xs" onclick="return excluirDepoimento(<?php echo $id ?>)" title="Exluir"><span class="glyphicon glyphicon-trash"></span>Excluir</div>
                                         <!--<div class="btn btn-default btn-xs" style="padding: 4px 3px 6px 6px;" title="Mover"><span class="glyphicon glyphicon-move"></span></div>-->
                                     </div>                                                                      
                                     <div class="itemdepmove" style="display: none">
                                         <span class="glyphicon glyphicon-move"></span>
                                     </div>                                                        

                                </div>
                            </li>
                <?php  
                    }//while
                ?>
                        </ul>
                    </div>                                                
                    
                <?php  
                    if($depvazio){
                ?>
                        <div>
                            <div class="alert alert-info"><b>Nenhum Depoimento: </b>Ainda não foram cadastrados depoimentos.</div>
                        </div>
                <?php  
                    }//if(albunvazio)
                ?>
                    </div>
                                
                <?php  break; case 'adddep': ?>
                    <h4>Adicionar um novo depoimento</h4>
                    <div class="panel panel-default">                    
                        <div class="panel-heading">Preencha o formulário abaixo:</div>
                        <div class="panel-body">
                            <?php if ($_GET['msg']=='error') { ?>
                            <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                           
                            <?php } ?> 
                            <form class="form-adddep" name="formadddep" method="post" enctype="multipart/form-data">
                                <div class="addesquerda">
                                    <input type="hidden" name="op" value="add-dep"/>
                                    <div class="form-group">
                                        <label> Nome dos autores:</label>
                                        <input type="text" id="texto1" name="texto1" class="form-control" placeholder="Autores" style="width: 250px">
                                        <!--<span class="help-block">Nome dos autores do depoimento.</span>-->
                                    </div>                                    
                                    <div class="form-group">
                                        <label> Depoimento:</label>
                                        <textarea id="texto2" name="texto2" class="form-control" placeholder="Depoimento" style="height:100px;"></textarea>
    <!--                                    <span class="help-block">(Opcional)</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label> Curto depoimento:</label>
                                        <input type="text" id="texto3" name="texto3" class="form-control" placeholder="Depoimento Curto">
                                        <span class="help-block">(Uma frase curta marcante do depoimento)</span>
                                    </div>
                                </div>
                                <div class="adddireita">
                                    <div class="form-group">
                                        <label>Foto do depoimento:</label>
                                        <input type="file" id="filefoto" name="filefoto" class="form-control">
                                    <span class="help-block">Dê preferência para uma foto do bolo.</span>
                                    </div>
                                    <div class="form-group">
                                        <img src="img/t_nofoto.png"><br>                                        
                                    </div>
                                </div>
                                <div class="addcontrol">
                                    <div class="form-group">
                                        <div class="buttonlinha ">
                                            <input type="button" id="submitadddep" name="submitadddep" value="Adicionar" class="btn btn-primary " >    
                                            <a href="depoimentos.php" class="btn btn-default">Cancelar</a>                                                                       
                                        </div>                                    
                                    </div>
                                </div>
                            </form>                                
                        </div>
                    </div>   
 
               <?php  break; case 'editdep' : ?>
                   <?php     
                        $erro = false;
                        if(isset($_GET['dep'])){
                            $iddep = $_GET['dep'];
                            $sql = "SELECT  id, texto1, texto2, texto3, foto
                                      FROM  depoimentos 
                                     WHERE  deletado = 0 
                                       AND  id = '$iddep'
                                  ORDER BY  ordem ASC";
                            $res = mysql_query($sql, $con);
                            if($res && mysql_num_rows($res)>0){
                                while($row = mysql_fetch_array($res)){
                                    $id      = $row['id']; 
                                    $texto1  = utf8_decode($row['texto1']);
                                    $texto2  = utf8_decode($row['texto2']);
                                    $texto3  = utf8_decode($row['texto3']);
                                    $imgfoto = ( '0' != $row['foto'] ? $pastafotos.$row['foto'] : 'img/t_nofoto.png');
                                    $timgfoto= ( '0' != $row['foto'] ? $pastafotos."t_".$row['foto'] : 'img/t_nofoto.png');                                                                        
                                }
                    ?>                                  
                    <h4>Editar Depoimento</h4>
                    <div class="panel panel-default">                    
                        <div class="panel-heading">Preencha o formulário abaixo para editar :</div>
                        <div class="panel-body">
                            <?php if ($_GET['msg']=='error') { ?>
                                <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                           
                            <?php } ?> 
                            <form class="form-adddep" name="formeditdep" method="post" enctype="multipart/form-data">
                                <div class="addesquerda">                                
                                    <input type="hidden" name="op" value="edt-dep"/>
                                    <input type="hidden" name="id" value="<?php echo $id?>"/>
                                    <div class="form-group">
                                        <label> Nome dos Autores:</label>
                                        <input type="text" id="texto1" name="texto1" class="form-control" placeholder="Autores" value="<?php echo $texto1?>" style="width:250px">
                                        <!--<span class="help-block">Nome dos autores do depoimento.</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label> Depoimento:</label>
                                        <textarea id="texto2" name="texto2" class="form-control" placeholder="Depoimento" style="height:100px;"><?php echo $texto2?></textarea>
    <!--                                    <span class="help-block">(Opcional)</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label> Depoimento Curto:</label>
                                        <input type="text" id="texto1" name="texto3" class="form-control" placeholder="Autores" value="<?php echo $texto3?>" style="width:250px">
                                        <span class="help-block">(Uma frase curta marcante do depoimento)</span>
                                    </div>
                                </div>
                                <div class="adddireita">
                                    <input type="hidden" name="trocarimg" id="trocarimg" value="0"/>
                                    <label>Foto do depoimento:</label>                                   
                                    <div class="form-group" id="boxfilefoto" style="display: none;">
                                        <input type="file" id="filefoto" name="filefoto" class="form-control">
                                        <a href="javascript:void(0)" onclick="return cancelarTrocarImagem()">Cancelar</a>
                                    </div>
                                    <div class="form-group" id="boxfoto"  style="display: block;">
                                        <a href="<?php echo $imgfoto ?>" rel="lightbox"><img src="<?php echo $timgfoto ?>" id="i"/></a><br>
                                        <a href="javascript:void(0)" onclick="return trocarImagem()">Trocar Imagem</a>
                                    </div>                  
                                </div>
                                <div class="addcontrol">                                
                                    <div class="form-group">
                                        <div class="buttonlinha ">
                                            <input type="button" id="submiteditdep" name="submiteditdep" value="Salvar" class="btn btn-primary " >                                
                                            <a href="depoimentos.php?p=deps&dep=<?php echo $id?>" class="btn btn-default">Cancelar</a>                                   
                                        </div>                                    
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                    <?php }else{
                            $erro = true;
                          }
                        }else{
                            $erro = true;                            
                        }
                      if ($erro) {?>
                        <h4>Editar Depoimento</h4>
                        <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                      
                    <?php } ?>
                                       
                    
                <?php break; case 'dep': ?>
                    <?php     
                        $erro = false;
                        if(isset($_GET['dep'])){
                            $iddep = $_GET['dep'];
                            $sql = "SELECT  id, texto1, texto2, texto3, foto
                                      FROM  depoimentos 
                                     WHERE  deletado = 0 
                                       AND  id = '$iddep'
                                  ORDER BY  ordem ASC";
                            $res = mysql_query($sql, $con);
                            if($res && mysql_num_rows($res)>0){
                                while($row = mysql_fetch_array($res)){
                                    $id      = $row['id']; 
                                    $texto1  = utf8_decode($row['texto1']);
                                    $texto2  = utf8_decode($row['texto2']);
                                    $texto3  = utf8_decode($row['texto3']);
                                    $imgfoto = ( '0' != $row['foto'] ? $pastafotos.$row['foto'] : 'img/t_nofoto.png');
                                    $timgfoto= ( '0' != $row['foto'] ? $pastafotos."t_".$row['foto'] : 'img/t_nofoto.png');
                                }
                    ?>                      
                        <h4>Depoimento <small>> <?php echo $texto1 ?></small></h4>
                        <div id="controle1">                         
                            <div class="btn-group btn-group-sm"><a class="btn btn-default" href="portfolio.php?p=albuns" title="Voltar"><span class="glyphicon glyphicon-arrow-left"></span> Álbuns</a></div>
                            <div class="acontrole">
                                <div class="btn-group btn-group-sm">
                                    <a class="btn btn-info" href="portfolio.php?p=albuns" title="Editar">
                                        <span class="glyphicon glyphicon-edit"></span> Editar
                                    </a>
                                </div>
                                <div class="btn-group btn-group-sm">
                                    <a class="btn btn-danger" href="javascript:void(0)" onclick="return excluirDepoimento(<?php echo $id ?>)" title="Apagar depoimento">
                                        <span class="glyphicon glyphicon-trash"></span> Excluir
                                    </a>
                                </div>
                            </div>  
                        </div>  
                        <div class="clearfix"></div>

                        <?php if (isset($_GET['msg'])){ switch ($_GET['msg']) {
                               case '1': ?> <div class="alert alert-success" style="margin: 0"><b>Finalizado!</b> Todas as fotos foram excluídas com sucesso.!</div> <?php break; ?>                        
                        <?php  case 'added': ?> <div class="alert alert-success"><b>Sucesso!</b> Novo álbum adicionado com sucesso!</div> <?php break; ?>
                        <?php  case 'edited': ?> <div class="alert alert-info"><b>Salvo!</b> As novas informações foram salvas com sucesso!.</div><?php break; ?>                         
                        <?php     }//switch
                              }?>
                                           
                                             
                    <?php 
                        }else{
                    ?>
                        <h4>Depoimento <small>(Desconhecido)</small></h4>
                        <div class="alert alert-danger"><b>Ops!</b> Algo não está certo. Tente novamente mais tarde.</div>                        
                    <?php 
                        }
                    }else{
                    ?>
                        <h4>Depoimento <small>(Desconhecido)</small></h4>
                        <div class="alert alert-danger"><b>Ops!</b> Algo não está certo. Tente novamente mais tarde.</div>                        
                    <?php                         
                    }
                    ?>
                    
                                            
                <?php break; case 'fotosordem': ?>
                    <?php     
                        $erro = false;
                        if(isset($_GET['a'])){
                            $idalbum = $_GET['a'];
                            $sql = "SELECT  texto1, texto2
                                      FROM  albuns as a 
                                     WHERE  a.deletado = 0 
                                       AND  a.id = '$idalbum'
                                  ORDER BY  a.ordem ASC";
                            $res = mysql_query($sql, $con);
                            if($res){
                                while($row = mysql_fetch_array($res)){                                                
                                    $texto1 = utf8_decode($row['texto1']);
                                    $texto2 = utf8_decode($row['texto2']);
                                }
                    ?>                      
                        <h4><?php echo $texto1 ?> <small><?php echo $texto2 ?></small></h4>
                        <div class="acontrole btn-group btn-group-sm">
                            <a class="btn btn-default" href="portfolio.php?p=album&album=<?php echo $idalbum ?>" title="Voltar"><span class="glyphicon glyphicon-arrow-left"></span> Voltar sem salvar</a>
                            <a class="btn btn-primary" href="portfolio.php?p=albuns" title="Voltar"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar esta ordem</a>
                        </div>
                        <div class="clearfix"></div>
                        <div id="albumfotos">
                        <?php
                            $sql = "SELECT  id, name
                                      FROM  fotos as f 
                                     WHERE  f.idalbum = '$idalbum'
                                       AND  f.deletado = 0
                                  ORDER BY  f.ordem ASC";
//                            die($sql);
                            $res2 = mysql_query($sql, $con);
                            if($res2){
                                if(mysql_num_rows($res2)==0){
                                ?>                      
                                <div class="alert alert-info"><b>Sem fotos</b> Este álbum ainda não possui fotos.</div>  
                                <?php
                                }
//                                echo '<div class="recebeDrag">';
                                while($row2 = mysql_fetch_array($res2)){     
                                    $id = $row2['id'];
                                    $name = $row2['name'];
                        ?>                   
<!--                                <div class="itemDrag">                                    
                                    <div class="area">                                    -->
                                        <a href="<?php echo $pastafotos,$idalbum,"/",$name ?>" rel="lightbox[example]" title="<?php echo $texto1 ?>" class="tfoto thumbnail">
                                            <!--<div class="afoto" style="background: url('<?php echo $pastafotos,$idalbum,"/",'t_',$name ?>') center center no-repeat;"></div>-->
                                        </a>                    
<!--                                    </div>
                                </div>-->
                            <!--</div>-->
                        <?php 
                                }//while
//                                echo '</div>';
                            }else{
                               //Select error
                                $erro = true;
                            }
                          }else{
                           //Select sem resultado
                            $erro = true;
                          }
                        }else{
                           //Get sem album
                            $erro = true;                            
                        }
                      if ($erro) {?>
                        <h4>Álbum <small>(Desconhecido)</small></h4>
                        <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                      
                    <?php } ?>
                    </div>    
                    <?php                     
                        if (!$erro) {
                    ?>
                            <a class="btn btn-primary" href="portfolio.php?p=albuns" title="Voltar"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar esta ordem</a>
                        </div>                                                        
                    <?php 
                    }
                    ?>                                       
                    
                <?php  
                    break;              
                    }//switch($page)  
                ?>

<!--                    <div class="panel panel-default">                    
                        <div class="panel-body">
                            
                        </div>
                    </div>            -->
                    
                </div><!--id="conteudodir"-->                

            </div>
        </div>
        <?php
            include_once 'php/content/rodape.php';
        ?>   
    </body>
</html>