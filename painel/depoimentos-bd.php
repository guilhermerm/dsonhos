<?php
session_start();
include_once './lib/security.php';
include_once 'lib/conection.php';
include_once './lib/resize-class.php';

$pastafotos = "../dsonhos/depoimentos/fotos/";

switch ($_POST['op']){
    
    //ADICIONAR NOVO DEPOIMENTO
    case 'add-dep':
        
        if(isset($_POST['iddep'])){
            $idpai  =  $_POST['iddep'];
        }else{
            $idpai  = 0;
        }
        
        //GET valores dos inputs
        $texto1 = mysql_escape_string(utf8_encode(trim($_POST['texto1'])));
        $texto2 = mysql_escape_string(utf8_encode(trim($_POST['texto2'])));
        $texto3 = mysql_escape_string(utf8_encode(trim($_POST['texto3'])));
        
        $foto = '0';
        $filefoto = isset($_FILES["filefoto"]) ? $_FILES["filefoto"] : FALSE; 
        
        // Verifica se o mime-type do arquivo é de imagem
        if (!eregi("^image\/(pjpeg|jpeg)$", $filefoto["type"])) {
            echo "Arquivo em formato inválido! A imagem deve ser JPG. Envie outro arquivo";
        } else {
            $img_name = "dep_".md5(uniqid("img", true)).".jpg";             
            $imagem_dir = $pastafotos.$img_name;
            
            move_uploaded_file($filefoto["tmp_name"], $imagem_dir);
            
            /*Redimensionar imager e criar thumb imagem*/
            $resizeObj = new resize($imagem_dir);
            //THUMB
            $resizeObj -> resizeImage(120, 100, 'crop');
            $resizeObj -> saveImage($imagem_dir,90);
            
            if($resizeObj->getWidth() > $maxwidth || $resizeObj->getHeight() > $maxheight){
                $resizeObj -> resizeImage(800, 600, 'auto');
            }
            $resizeObj -> saveImage($pastafotos.$img_name,85);            
            
            //Adicionar marca na imagem
            $img1 = imagecreatefromjpeg($imagem_dir);
            $img2 = imagecreatefrompng('img/marca_dsonhos.png');
              // Mantem definições de transparencia
              imageAlphaBlending($img2, true);
              imageSaveAlpha($img2, true);            
            //mescla as imanges
            imagecopy($img1, $img2, (imagesx($img1)/2-(imagesx($img2)/2)), (imagesy($img1)-imagesy($img2)), 0, 0, imagesx($img2), imagesy($img2));            
            imagejpeg($img1,$imagem_dir,90);            
            
            //Adicionar reflexo no thumb
            $img1 = imagecreatefromjpeg($imagem_dir);
            $img2 = imagecreatefrompng('img/dsonhos-reflexo-thumb.png');
              // Mantem definições de transparencia
              imageAlphaBlending($img2, true);
              imageSaveAlpha($img2, true);            
            //mescla as imanges
            imagecopy($img1, $img2, 0, 0, 0, 0, imagesx($img2), imagesy($img2));            
            imagejpeg($img1,$imagem_dir,90);
            
            $foto = $img_name;
            
        }
        
        //Monta SQL para inserir no Banco               
        $ordem = "(SELECT IF(MAX(d.ordem),MAX(d.ordem)+1,0) as ordem FROM depoimentos as d)";
                      
        $sql = "INSERT INTO depoimentos(ordem, texto1, texto2, texto3, foto, deletado) 
                     VALUES  ($ordem, '$texto1', '$texto2', '$texto3', '$foto', 0)";
        
        $res = mysql_query($sql, $con);
        
        if($res){ 
            header("Location:./depoimentos.php?p=deps&msg=added");
            exit;                                 
        }else{
            //Erro
            header("Location:./depoimentos.php?p=adddep&msg=error");
            exit;
        }

    break;
    
    /***************************************************************************/
    
    //EDITAR ALBUM
    case 'edt-dep':

        $id   = $_POST['id'];
        $texto1 = mysql_escape_string(utf8_encode(trim($_POST['texto1'])));
        $texto2 = mysql_escape_string(utf8_encode(trim($_POST['texto2'])));
        $texto3 = mysql_escape_string(utf8_encode(trim($_POST['texto3'])));        
        
        $trocarimg = $_POST['trocarimg'];
        $setimg = '';
        
        if( ! $trocarimg == '0'){
            $foto = '0';
            $filefoto = isset($_FILES["filefoto"]) ? $_FILES["filefoto"] : FALSE;             

            // Verifica se o mime-type do arquivo é de imagem
            if (!eregi("^image\/(pjpeg|jpeg)$", $filefoto["type"])) {
                echo "Arquivo em formato inválido! A imagem deve ser JPG. Envie outro arquivo";
            } else {
                $img_name = "dep_".md5(uniqid("img", true)).".jpg";             
                $imagem_dir = $pastafotos.$img_name;

                move_uploaded_file($filefoto["tmp_name"], $imagem_dir);

                /*Redimensionar imager e criar thumb imagem*/
                $resizeObj = new resize($imagem_dir);
                //THUMB
                $resizeObj -> resizeImage(120, 100, 'crop');
                $resizeObj -> saveImage($pastafotos."t_".$img_name,90);
                
                if($resizeObj->getWidth() > $maxwidth || $resizeObj->getHeight() > $maxheight){
                    $resizeObj -> resizeImage(800, 600, 'auto');
                }
                $resizeObj -> saveImage($pastafotos.$img_name,85);            

                //Adicionar marca na imagem
                $img1 = imagecreatefromjpeg($imagem_dir);
                $img2 = imagecreatefrompng('img/marca_dsonhos.png');
                  // Mantem definições de transparencia
                  imageAlphaBlending($img2, true);
                  imageSaveAlpha($img2, true);            
                //mescla as imanges
                imagecopy($img1, $img2, (imagesx($img1)/2-(imagesx($img2)/2)), (imagesy($img1)-imagesy($img2)), 0, 0, imagesx($img2), imagesy($img2));            
                imagejpeg($img1,$imagem_dir,90);                 
                
                //Adicionar reflexo no thumb
                $img1 = imagecreatefromjpeg($imagem_dir);
                $img2 = imagecreatefrompng('img/dsonhos-reflexo-thumb.png');
                  // Mantem definições de transparencia
                  imageAlphaBlending($img2, true);
                  imageSaveAlpha($img2, true);            
                //mescla as imanges
                imagecopy($img1, $img2, 0, 0, 0, 0, imagesx($img2), imagesy($img2));            
                imagejpeg($img1,$imagem_dir,90);

                $foto = $img_name;      
                
                $setimg = " , foto = '$foto' ";
            }  
            
        }
                   
        $sql = "UPDATE  depoimentos
                   SET  texto1 = '$texto1'
                        , texto2 = '$texto2'
                        , texto3 = '$texto3'
                        $setimg
                 WHERE  id = $id
                ";
//        die($sql);
        $res = mysql_query($sql, $con);      
        
        if($res){             
            header("Location:./depoimentos.php?p=deps&msg=edited");
        }else{
            //Erro
            header("Location:./depoimentos.php?p=editdep&dep=$iddep&msg=error");           
            exit;
        }

    break;
    
    /***************************************************************************/
    
    //EXCLUIR UM DEPOIMENTO
    case 'exc-dep' :        

        $iddep = $_POST['iddep'];

        $sql = "UPDATE depoimentos SET deletado = 1 WHERE id = $iddep";
        
        $res = mysql_query($sql, $con);
        if($res){
            echo 'true';
            exit;
        }else{            
            echo 'false';
            exit;
        }
        
        echo 'Não pode ser Excluido';
        exit;
    break;
        
    /***************************************************************************/
    //SALVAR ORDEM DEPOIMENTOS
    case 'ord-dep' :              
        $ordem = $_POST['ordem'];        
        $lista = explode(",",$ordem);
        
        $tam = count($lista);
        for($i=0;$i<$tam-1;$i++){
            $sql = " UPDATE depoimentos SET ordem =".($i+1)." WHERE id = $lista[$i] ";        
            $res = mysql_query($sql, $con);
        }
        
        if($res){
            echo 'true';
            exit;
        }else{            
            echo 'false';
            exit;
        }
        
        echo 'Não pode ser excluído';
        exit;
    break;
    
    default :
            //Operação Ilegal
            header("Location:./depoimentos.php");
            exit;        
    break;
    
}    
?>
