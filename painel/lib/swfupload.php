<?php
    include_once './lib/security.php';
    include("wideimage/WideImage.php");
    include("./conection.php");

   
    $pastafotos = "../../dsonhos/portfolio/fotos/";  
    $maxwidth   = 800;
    $maxheight  = 600;
   
	 // Work-around for setting up a session because Flash Player doesn't send the cookies
	 if (isset($_POST["PHPSESSID"])) {
             session_id($_POST["PHPSESSID"]);
	 }
	session_start();

        $idalbum = $_POST["idalbum"];
        $grouptmp   = $_POST["grouptmp"];
        
        //Criar pasta do album se não existir.
        if (!file_exists($pastafotos.$idalbum)){
            mkdir($pastafotos.$idalbum, 0777, true);
        }
            $img_name = md5(uniqid("img", true)).".jpg";
            $img_endereco     = $pastafotos.$idalbum."/".$img_name;
            $img_endereco_tmp = $pastafotos.$idalbum."/ori_".$img_name;
            $img_endthumb     = $pastafotos.$idalbum."/".'t_'.$img_name;	
			
	// The Demos don't save file
        if (!isset($_FILES["Filedata"]) || !is_uploaded_file($_FILES["Filedata"]["tmp_name"]) || $_FILES["Filedata"]["error"] != 0) { 
             echo "Ocorreu um erro ao enviar as imagens. Tente novamente mais tarde ou entre em contato com o administrador.<br><br>";  
//            print_r($_FILES);			
        }else{                       
            /*Aqui é onde movemos o nosso upload para a pasta files acessível pelo servidor */
             move_uploaded_file($_FILES["Filedata"]["tmp_name"], $img_endereco_tmp); 
            
            /*Redimensionar imager e criar thumb imagem*/
            // $resizeObj = new resize($img_endereco_tmp);
            $image = WideImage::load($img_endereco_tmp);

            //IMAGEM
            if($image->getWidth()>$maxwidth ||$image->getHeight()>$maxheight){
                $image = $image->resize($maxwidth, $maxheight);
            }
            $image->saveToFile($img_endereco);
			
            //THUMB
            $image = $image->resize(120, 100, 'outside');
            $image = $image->crop("center","middle",120,100);
            $image->saveToFile($img_endthumb);

                        
            //Adicionar logo marca na imagem              
            $img1 = imagecreatefromjpeg($img_endereco);
            if(imagesy($img1)>=410){
                $img2 = imagecreatefrompng('uploads/marca_dsonhos_center.png');
                  // Mantem definições de transparencia
                  imageAlphaBlending($img2, true);
                  imageSaveAlpha($img2, true);            
                //mescla as imanges
                imagecopy($img1, $img2, (imagesx($img1)/2-(imagesx($img2)/2)), (imagesy($img1)-imagesy($img2)), 0, 0, imagesx($img2), imagesy($img2));                            
            }else{
                $img2 = imagecreatefrompng('uploads/marca_dsonhos_right.png');
                  // Mantem definições de transparencia
                  imageAlphaBlending($img2, true);
                  imageSaveAlpha($img2, true);            
                //mescla as imanges
                imagecopy($img1, $img2, (imagesx($img1)-(imagesx($img2))), (imagesy($img1)-imagesy($img2)), 0, 0, imagesx($img2), imagesy($img2));                                            
            }
            imagejpeg($img1,$img_endereco,90);
            
                                               
            //Adicionar reflexo no thumb
            $img1 = imagecreatefromjpeg($img_endthumb);
            $img2 = imagecreatefrompng('uploads/dsonhos-reflexo-thumb.png');
              // Mantem definições de transparencia
              imageAlphaBlending($img2, true);
              imageSaveAlpha($img2, true);            
            //mescla as imanges
            imagecopy($img1, $img2, 0, 0, 0, 0, imagesx($img2), imagesy($img2));            
            imagejpeg($img1,$img_endthumb,90);                          
            
            /*Salvar endereço no banco*/        
            //Shift na ordem dos banco.
            $sql = "UPDATE fotos SET ordem = ordem + 1 WHERE idalbum = $idalbum AND grouptmp <> $grouptmp";
            $res = mysql_query($sql, $con);
            
            //Inserir imagem
            $ordem = "(SELECT COUNT(*)+1 as ordem FROM fotos as f WHERE f.idalbum = $idalbum AND f.grouptmp = $grouptmp)";            
//             $ordem = "(SELECT IF(MAX(f.ordem),MAX(f.ordem)+1,0) as ordem FROM fotos as f WHERE f.idalbum = $idalbum)";            
             $sql = "INSERT INTO fotos (idalbum, name, ordem, grouptmp) VALUES ($idalbum,'$img_name',$ordem,$grouptmp)";
             $res = mysql_query($sql, $con);      
        }			
	exit(0);
?>