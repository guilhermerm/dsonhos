<?php
session_start();
include_once './lib/security.php';
include_once 'lib/conection.php';
include_once './lib/resize-class.php';
include_once './lib/funcoes.php';


$pastafotos = "../dsonhos/portfolio/fotos/";
$pastatmp = "tmp";

switch ($_POST['op']){
    
    //ADICIONAR NOVO ALBUM
    case 'add-alb':
        
        if(isset($_POST['idalbumpai'])){
            $idpai  =  $_POST['idalbumpai'];
        }else{
            $idpai  = 0;
        }
        
        //GET valores dos inputs
        $texto1 = mysql_escape_string(utf8_encode(trim($_POST['texto1'])));
        $texto2 = mysql_escape_string(utf8_encode(trim($_POST['texto2'])));     
        $link = nametolink($_POST['texto1']);
        
        //Criar pasta termporária
        if (!file_exists($pastafotos.$pastatmp."/")){
            mkdir($pastafotos.$pastatmp.'/', 0777, true);
        }                
        
        $foto = '0';
        $filefoto = isset($_FILES["filefoto"]) ? $_FILES["filefoto"] : FALSE; 
        
        // Verifica se o mime-type do arquivo é de imagem
        if (!eregi("^image\/(pjpeg|jpeg)$", $filefoto["type"])) {
            echo "Arquivo em formato inválido! A imagem deve ser JPG. Envie outro arquivo";
        } else {
            $img_name = "alb_".md5(uniqid("img", true)).".jpg";             
            $imagem_dir = $pastafotos.$pastatmp."/".$img_name;
            
            move_uploaded_file($filefoto["tmp_name"], $imagem_dir);
            
            /*Redimensionar imager e criar thumb imagem*/
            $resizeObj = new resize($imagem_dir);
            //THUMB
            $resizeObj -> resizeImage(120, 100, 'crop');
            $resizeObj -> saveImage($imagem_dir,90);
            //Adicionar reflexo no thumb
            $img1 = imagecreatefromjpeg($imagem_dir);
            $img2 = imagecreatefrompng('img/dsonhos-reflexo-thumb.png');
              // Mantem definições de transparencia
              imageAlphaBlending($img2, true);
              imageSaveAlpha($img2, true);            
            //mescla as imanges
            imagecopy($img1, $img2, 0, 0, 0, 0, imagesx($img2), imagesy($img2));            
            imagejpeg($img1,$imagem_dir,90);
            
            $foto = $img_name;
            
        }
        
        //Monta SQL para inserir no Banco               
        $ordem = "(SELECT IF(MAX(a.ordem),MAX(a.ordem)+1,0) as ordem FROM albuns as a)";        
                      
        $sql = "INSERT INTO albuns(idpai, ordem, texto1, texto2, link, foto, oculto, deletado) 
                     VALUES  ($idpai, $ordem, '$texto1', '$texto2', '$link','$foto', 0, 0)";
        
        $res = mysql_query($sql, $con);
        $idalbum = mysql_insert_id();

        
        if($res){ 
            
            //Criar pasta do album se não existir.
            if (!file_exists($pastafotos.$idalbum)){
                mkdir($pastafotos.$idalbum, 0777, true);
            }
            if($foto!='0'){
                copy($pastafotos.$pastatmp."/".$foto, $pastafotos.$idalbum."/".$foto);
            }
            //OK
            if($idpai==0){
                header("Location:./portfolio.php?p=albuns&msg=added");
            }else{
                header("Location:./portfolio.php?p=album&album=".$idpai."&msg=added");                
            }
            exit;                                 
        }else{
            //Erro
            header("Location:./portfolio.php?p=addalbum&msg=error");
            exit;
        }

    break;
    
    //ADICIONAR NOVO VIDEO
    case 'add-vid':
        //GET valores dos inputs

        $texto1 = mysql_escape_string(utf8_encode(trim($_POST['texto1'])));
        $texto2 = mysql_escape_string(utf8_encode(trim($_POST['texto2'])));
        $texto3 = mysql_escape_string(utf8_encode(trim($_POST['texto3'])));    
        $link = nametolink($_POST['texto1']);
        
        $aux = explode("v=",$texto2);
        $aux = explode("&",$aux[1]);
        $watchv =  $aux[0];
        
        //Monta SQL para inserir no Banco               
//        $ordem = "(SELECT IF(MAX(a.ordem),MAX(a.ordem)+1,0) as ordem FROM albuns as a)";        
        //Shift na ordem
        $sql = "UPDATE videosyt SET ordem=ordem+1";
        $res = mysql_query($sql, $con);
                      
        
        $sql = "INSERT INTO videosyt (ordem, texto1, texto2, texto3, link, watchv, deletado) 
                     VALUES  (1, '$texto1', '$texto2', '$texto3', $link ,'$watchv', 0)";
        $res = mysql_query($sql, $con);
        
        if($res){ 
            //OK
            header("Location:./portfolio.php?p=videos&msg=added");            
            exit;                                 
        }else{
            //Erro
            header("Location:./portfolio.php?p=addvideo&msg=error");
            exit;
        }

    break;
    
    /***************************************************************************/
    
    //EDITAR ALBUM
    case 'edt-alb':
        $idalbum = $_POST['idalbum'];
        $idpai   = $_POST['idpai'];
        $texto1 = mysql_escape_string(utf8_encode(trim($_POST['texto1'])));
        $texto2 = mysql_escape_string(utf8_encode(trim($_POST['texto2'])));
        $link = nametolink($_POST['texto1']);
        
        //Criar pasta do album se não existir.
        if (!file_exists($pastafotos.$idalbum)){
            mkdir($pastafotos.$idalbum, 0777, true);
        }        
        
        $trocarimg = $_POST['trocarimg'];
        $setimg = '';
        
        if( ! $trocarimg == '0'){
            $foto = '0';
            $filefoto = isset($_FILES["filefoto"]) ? $_FILES["filefoto"] : FALSE;             

            // Verifica se o mime-type do arquivo é de imagem
            if (!eregi("^image\/(pjpeg|jpeg)$", $filefoto["type"])) {
                echo "Arquivo em formato inválido! A imagem deve ser JPG. Envie outro arquivo";
            } else {
                $img_name = "alb_".md5(uniqid("img", true)).".jpg";             
                $imagem_dir = $pastafotos.$idalbum."/".$img_name;

                move_uploaded_file($filefoto["tmp_name"], $imagem_dir);

                /*Redimensionar imager e criar thumb imagem*/
                $resizeObj = new resize($imagem_dir);
                //THUMB
                $resizeObj -> resizeImage(120, 100, 'crop');
                $resizeObj -> saveImage($imagem_dir,90);
                //Adicionar reflexo no thumb
                $img1 = imagecreatefromjpeg($imagem_dir);
                $img2 = imagecreatefrompng('img/dsonhos-reflexo-thumb.png');
                  // Mantem definições de transparencia
                  imageAlphaBlending($img2, true);
                  imageSaveAlpha($img2, true);            
                //mescla as imanges
                imagecopy($img1, $img2, 0, 0, 0, 0, imagesx($img2), imagesy($img2));            
                imagejpeg($img1,$imagem_dir,90);

                $foto = $img_name;      
                
                $setimg = " , foto = '$foto' ";
            }  
            
        }
        
        $sql = "UPDATE  albuns
                   SET  texto1 = '$texto1'
                        , texto2 = '$texto2'
                        , link = '$link'
                        $setimg
                 WHERE  id = $idalbum
                ";
//        die($sql);
        $res = mysql_query($sql, $con);      
        
        if($res){ 
            if($idpai=='0'){
                header("Location:./portfolio.php?p=albuns&msg=edited");
            }else{
                header("Location:./portfolio.php?p=album&album=".$idpai."&msg=edited");                
            }                              
        }else{
            //Erro
            header("Location:./portfolio.php?p=editalbum&album=$idalbum&msg=error");
            exit;
        }
    break;
    
    //EDITAR VIDEO
    case 'edt-vid':
        $idvideo = $_POST['idvideo'];
        $texto1 = mysql_escape_string(utf8_encode(trim($_POST['texto1'])));
        $texto2 = mysql_escape_string(utf8_encode(trim($_POST['texto2'])));
        $texto3 = mysql_escape_string(utf8_encode(trim($_POST['texto3'])));    
        $link = nametolink($_POST['texto1']);
                   
        $sql = "UPDATE  videosyt
                   SET  texto1 = '$texto1'
                        , link = '$link'
                        , texto2 = '$texto2'
                        , texto3 = '$texto3'
                 WHERE  id = $idvideo
                ";
        
        $res = mysql_query($sql, $con);      
        
        if($res){ 
            header("Location:./portfolio.php?p=videos&msg=edited");                         
        }else{
            //Erro
            header("Location:./portfolio.php?p=editvideo&vid=$idvideo&msg=error");
            exit;
        }

    break;
    
    //Ocultar ou Mostrar album
    case 'ocu-alb' :        

        $idalbum = $_POST['idalbum'];
        $alboculto  = $_POST['alboculto'];

        $sql = "UPDATE albuns SET oculto = $alboculto WHERE id = $idalbum ";
        
        $res = mysql_query($sql, $con);
        if($res){
            echo 'true';
            exit;
        }else{            
            echo 'false';
            exit;
        }
        
        echo 'Não pode ser Excluido';
        exit;
    break;
    
    /***************************************************************************/
    
    //EXCLUIR UMA FOTO
    case 'exc-fto' :        

        $idfoto = $_POST['idfoto'];

        $sql = "UPDATE fotos SET deletado = 1 WHERE id = $idfoto ";
        
        $res = mysql_query($sql, $con);
        if($res){
            echo 'true';
            exit;
        }else{            
            echo 'false';
            exit;
        }
        
        echo 'Não pode ser Excluido';
        exit;
    break;
    
    //EXCLUIR TODAS AS FOTO DO ALBUM
    case 'exc-allfto' :        

        $idalbum = $_POST['idalbum'];

        $sql = "UPDATE fotos SET deletado = 1 WHERE idalbum = $idalbum ";
        
        $res = mysql_query($sql, $con);
        if($res){
            echo 'true';
            exit;
        }else{            
            echo 'false';
            exit;
        }
        
        echo 'Não pode ser Excluido';
        exit;
    break;

    //EXCLUIR TODAS AS FOTO DO ALBUM e o ALBUM
    case 'exc-alb' :        

        $idalbum = $_POST['idalbum'];

        $sql = "UPDATE fotos SET deletado = 1 WHERE idalbum = $idalbum ";        
        $res = mysql_query($sql, $con);
        if($res){            
            $sql = "UPDATE albuns SET deletado = 1 WHERE id = $idalbum ";        
            $res = mysql_query($sql, $con);
            if($res){            
                echo 'true';
                exit;
            }else{            
                echo 'false2';
                exit;
            }
            exit;
        }else{            
            echo 'false1';
            exit;
        }
        
        echo 'Nenhuma operação foi realizada.';
        exit;
    break;    

    //EXCLUIR UM VIDEO
    case 'exc-vid' :        

        $idvideo = $_POST['idvideo'];

        $sql = "UPDATE videosyt SET deletado = 1 WHERE id = $idvideo ";        
        $res = mysql_query($sql, $con);
        if($res){                     
            echo 'true';
            exit;
        }else{            
            echo 'false';
            exit;
        }
        
        echo 'Nenhuma operação foi realizada.';
        exit;
    break;    
       
    /***************************************************************************/
    //SALVAR ORDEM ALBUNS
    case 'ord-alb' :        

        $idpai = $_POST['idpai'];        
        $ordem = $_POST['ordem'];
        
        $lista = explode(",",$ordem);
        
        $tam = count($lista);
        for($i=0;$i<$tam-1;$i++){
            $sql = " UPDATE albuns SET ordem =".($i+1)." WHERE id = $lista[$i] ";        
            $res = mysql_query($sql, $con);
        }
        
        if($res){
            echo 'true';
            exit;
        }else{            
            echo $sql;
            exit;
        }
        
        echo 'Não pode ser excluído';
        exit;
    break;
       
    /***************************************************************************/
    //SALVAR ORDEM VIDEOS
    case 'ord-vid' :        
        
        $ordem = $_POST['ordem'];        
        $lista = explode(",",$ordem);
        
        $tam = count($lista);
        for($i=0;$i<$tam-1;$i++){
            $sql = " UPDATE videosyt SET ordem =".($i+1)." WHERE id = $lista[$i] ";        
            $res = mysql_query($sql, $con);
        }        
        if($res){
            echo 'true';
            exit;
        }else{            
            echo $sql;
            exit;
        }        
        echo 'Não pode ser excluído';
        exit;
    break;
    
    
    default :
            //Operação Ilegal
            header("Location:./depoimentos.php");
            exit;        
    break;
    
}    
?>
