<?php
session_start();
include_once './lib/security.php';
include_once './lib/conection.php';

error_reporting(0);

$pastafotos = "../dsonhos/portfolio/fotos/";

//if(!isset($_SESSION["index"])){
//    header("Location: index.php?m=1");
//}
$page = 'albuns';
if(isset($_GET["p"])){
    $page = $_GET["p"];       
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
        <title>Login - Painel D'Sonhos</title>
        
        <link rel="StyleSheet" type="text/css" href="css/main.css"/>
        <link rel="StyleSheet" type="text/css" href="css/portfolio.css"/>
        <link rel="StyleSheet" type="text/css" href="css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
        <script language="javascript" type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/portfolio.js"></script>
        
        <!-- Bootstrap -->
        <link rel="StyleSheet" type="text/css" href="css/bootstrap-theme.min.css"/>
        <link rel="StyleSheet" type="text/css" href="css/bootstrap.min.css"/>
        <script language="javascript" type="text/javascript" src="js/bootstrap.min.js"></script>
        
        <!-- LightBox 2.0 -->
        <link rel="stylesheet" type="text/css" href="css/lightbox.css" media="screen" />
        <script type="text/javascript" src="js/lightbox-2.6.min.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        
        <link rel="SHORTCUT ICON" href="../imagens/favicon.png" type="image/x-icon" />
    </head>
    <body>
        <?php
            include_once 'php/content/barratopo.php';
        ?>                
<!--        <div id="barrainicio">
            <div class="corpo">
            
            </div>
        </div>-->
        <div id="conteudo">
            <div class="corpo">
                <h3>Portfólio</h3>
                <div id="menuesq">
                    <h4>Álbuns</h4>
                    <div class="btn-group-vertical">
                        <button type="button" class="btn btn-default btn-sm" id="linkalbuns"><span class="glyphicon glyphicon-th"></span> Todos álbuns</button>                           
                        <button type="button" class="btn btn-default btn-sm" id="linkordemalbum"><span class="glyphicon glyphicon-list"></span> Organizar álbuns</button>
                        <button type="button" class="btn btn-default btn-sm" id="linkaddalbum"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar novo álbum</button>
                    </div>
<!--                    <h4>Fotos</h4>
                    <div class="btn-group-vertical">
                        <button type="button" class="btn btn-default btn-sm" id="linkupfotos"><span class="glyphicon glyphicon-camera"></span> Enviar fotos</button>
                    </div>-->
                    <h4>Vídeos</h4>
                    <div class="btn-group-vertical">
                        <button type="button" class="btn btn-default btn-sm" id="linkvideos"><span class="glyphicon glyphicon-expand"></span> Todos vídeos</button>
                        <button type="button" class="btn btn-default btn-sm" id="linkaddvideo"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar vídeo</button>
                    </div>
                </div>
                <div id="conteudodir">
                              
                <?php
                    switch ($page){ 
                        case 'albuns':
                ?>
                    <h4>Todos álbuns</h4>
                    <?php if (isset($_GET['msg'])){ switch ($_GET['msg']) {
                           case 'added': ?> <div class="alert alert-success"><b>Sucesso!</b> Novo álbum adicionado com sucesso!</div> <?php break; ?>
                    <?php  case 'edited': ?> <div class="alert alert-info"><b>Salvo!</b> As novas informações foram salvas com sucesso!.</div><?php break; ?> 
                    <?php  case 'notalbum': ?> <div class="alert alert-danger"><b>Não existente.</b> O álbum parece não existir mais.</div><?php break; ?> 
                    <?php  case 'del': ?> <div class="alert alert-success"><b>Removido.</b> O álbum foi excluído com sucesso.</div><?php break; ?> 
                    <?php  default : break;  } } ?> 
                    <div id="albuns">                                              
                    <?php
                            $sql = "SELECT  id, texto1, texto2, foto
                                      FROM  albuns as a 
                                     WHERE  a.deletado = 0 
                                       AND  a.idpai = 0 
                                  ORDER BY  a.ordem ASC";
                            $res = mysql_query($sql, $con);
                            $c = 0; $albvazio = true;
                            while($row = mysql_fetch_array($res)){            
                                $albvazio = false;
                                $id     = $row['id'];
                                $texto1 = utf8_decode($row['texto1']);
                                $texto2 = utf8_decode($row['texto2']);
                                if($row['foto']!="0"){
                                    $thumb  = $pastafotos.$id."/".$row['foto'];
                                }else{
                                    $thumb  = "img/t_nofoto.png";
                                }
                    ?>
                    <div class="itemalbum">                           
                        <div class="thumbnail" >
                            <div class="thumbimg" title="<?php echo $texto1?>">
                                <img src="<?php echo $thumb?>">
                             </div>                            
                             <div class="caption">
                               <div class="atitulo" title="<?php echo $texto1?>"><?php echo $texto1?></div>
                               <div class="adescricao" title="<?php echo $texto2?>"><?php echo $texto2?></div>         
                                 <div class="btn-group btn-group-justified">      
                                      <a class="btn btn-group btn-primary btn-sm" style="width: 77%" href="portfolio.php?p=album&album=<?php echo $id ?>" role="button"><span class="glyphicon glyphicon-folder-open"></span> Abrir</a>
                                      <a class="btn btn-group btn-info btn-sm" style="width: 23%" href="portfolio.php?p=editalbum&album=<?php echo $id ?>" role="button" title="Editar"><span class="glyphicon glyphicon-edit"></span></a>
                                 </div>      
                               
                                   <!--<a href="portfolio.php?p=album&album=<?php echo $id ?>" class="btn btn-primary btn-sm btn-block" role="button"><span class="glyphicon glyphicon-folder-open"></span> Abrir</a>-->
<!--                                   <a href="#" class="btn btn-success btn-sm" role="button"><span class="glyphicon glyphicon-edit"></span> Editar</a>
                                   <a href="#" class="btn btn-default btn-sm" role="button"><span class="glyphicon glyphicon-trash"></span> Remover</a>-->                                  
                             </div>                            
                        </div>
                    </div>                                                
                <?php  
                    }//while
                    
                    if($albvazio){
                ?>
                        <div>
                            <div class="alert alert-info"><b>Nenhum Álbum: </b>Ainda não foram cadastrados álbuns.</div>
                        </div>
                <?php  
                    }//if(albunvazio)
                ?>
                    </div>

                <?php  break; case 'ordemalbum':
                    //Albuns pai
                    $sql  = "SELECT a.id, a.idpai, a.texto1, a.texto2, a.foto, a.ordem, (SELECT count(*) FROM fotos as f WHERE f.idalbum = a.id AND f.deletado = 0)  as qtd, (SELECT count(*) FROM albuns as a2 WHERE a2.idpai = a.id AND a2.deletado = 0)  as qtd2 FROM albuns as a WHERE a.deletado=0 and a.idpai=0 ORDER BY ordem";
                    $res_albpai = mysql_query($sql);                    
                    //Sub-albuns
                    $sql2 = "SELECT a.id, a.idpai, a.texto1, a.texto2, a.foto, a.ordem, (SELECT count(*) FROM fotos as f WHERE f.idalbum = a.id AND f.deletado = 0) as qtd FROM albuns as a WHERE a.deletado=0 and a.idpai<>0 ORDER BY ordem";
                    $res_albsub = mysql_query($sql2);
                    
                    $qtdalbunspai = mysql_num_rows($res_albpai);
                    $qtdsubalbuns = mysql_num_rows($res_albsub);
                    $qtdalbuns = $qtdalbunspai + $qtdsubalbuns;
                ?>
                                <div id="controle1">
                                    <div style="float:right; margin: 8px 0 0 0;">
                                        <div class="btn btn-default btn-sm" id="mostrarSubAlbuns">Mostrar Sub-álbuns</div>
                                    </div>
                                    <h4 style="margin-bottom:5px;">Organizar ordem dos Álbuns</h4>
                                    <small><b>Total de Álbuns: </b><?php echo $qtdalbunspai,($qtdalbunspai>1?' álbuns':' álbum')?> </small>
                                </div>
                                    <div class="ordemalbuns">
                                        <div id="listalbuns">                    
                                            <ul id="sortable0" idpai='0' class="listalbuns">
                <?php
                    //Mostrar Álbuns
                    $cont_sort = 0;
                    while($row = mysql_fetch_array($res_albpai)){
                        $id      = $row['id']; 
                        $ordem   = $row['ordem']; 
                        $qtd     = $row['qtd']; 
                        $qtd2    = $row['qtd2']; 
                        $texto1  = utf8_decode($row['texto1']);
                        $texto2  = utf8_decode($row['texto2']);
                        $imgfoto = ( '0' != $row['foto'] ? $pastafotos.$id."/".$row['foto'] : 'img/t_nofoto.png');
                ?>
                                                <li idalbum="<?php echo $id?>"  class="albumpai">
                                                    <div class="ordthumb listfoto" idalbum="<?php echo $id?>" style="display: none;"><img src="<?php echo $imgfoto ?>"/></div>
                                                    <div class="linkgoalb" idalbum="<?php echo $id?>"><a href="portfolio.php?p=album&album=<?php echo $id; ?>">Ir <span class="glyphicon glyphicon-share-alt"></span></a></div>
                                                    <!--<div class="numordem label label-primary"><?php echo $ordem; ?></div>-->
                                                    <?php 
                                                        if($qtd==0){
                                                            $fqtd  = 'Sem fotos';                                                            
                                                        }else{
                                                            $fqtd  = $qtd." foto".($qtd>1?'s':'');                                                            
                                                        }
                                                        if($qtd2==0){
                                                            $sbqtd  = 'e sem sub-álbuns';                                                            
                                                        }else{
                                                            $sbqtd  = $qtd2." sub-álbu".($qtd2>1?'ns':'m');                                                            
                                                        }
                                                        echo "<span title='",$fqtd," e ",$sbqtd,"'>",$texto1,"</span>"; 
                                                    ?>
                <?php
                        //Mostrar Álbuns
                        mysql_data_seek($res_albsub,0 );
                        $setul = false;
                        while($srow = mysql_fetch_array($res_albsub)){
                            $sidpai   = $srow['idpai']; 
                            if($sidpai != $id){
                                continue;                                
                            }
                            if ($setul==false){
                                $cont_sort++;
                                echo '<ul id="sortable',$cont_sort,'" idpai="',$sidpai,'" class="ulsubalbuns" style="display:none">';
                                $setul=true;
                            }
                            $sid      = $srow['id']; 
                            $sordem   = $srow['ordem']; 
                            $sqtd     = $srow['qtd']; 
                            $stexto1  = utf8_decode($srow['texto1']);
                            $stexto2  = utf8_decode($srow['texto2']);
                            $simgfoto = ( '0' != $srow['foto'] ? $pastafotos.$sid."/".$srow['foto'] : 'img/t_nofoto.png');
                ?>
                                                <li idalbum="<?php echo $sid?>" class="albumsub">
                                                    <div class="ordthumb slistfoto" idalbum="<?php echo $sid?>" style="display: none;"><img src="<?php echo $simgfoto ?>"/></div>
                                                    <div class="linkgoalb" idalbum="<?php echo $sid?>"><a href="portfolio.php?p=album&album=<?php echo $sid; ?>">Ir <span class="glyphicon glyphicon-share-alt"></span></a></div>
                                                    <!--<div class="numordem label label-info"><?php echo $ordem,$sordem; ?></div>-->
                                                    <?php echo $stexto1," <small> ($sqtd)</small>"?>
                                                </li>
                <?php
                        }
                        if($setul==true){
                            echo '</ul>';
                        }
                ?>
                                                </li>
                <?php
                    }
                ?>
                                            </ul>
                                            <div style="display:none" id="contsort" value="<?php echo $cont_sort ?>"></div> 
                                        </div> 
                                    </div>
                                    
                    
                <?php  break; case 'addalbum':                          
                        $albumpai = 0;
                        $subalbum = false;
                        $erro = false;
                        if(isset($_GET['ap'])){
                            //CRIAR SUB-ALBUM
                            $subalbum = true;                            
                            $albumpai = $_GET['ap'];
                            
                            $sql = "SELECT  id, idpai, texto1, texto2
                                      FROM  albuns as a 
                                     WHERE  a.deletado = 0 
                                       AND  a.id = '$albumpai'
                                  ORDER BY  a.ordem ASC";
                            $res = mysql_query($sql, $con);
                            if($res){                                
                                if(mysql_num_rows($res)==0){
                                    $erro = true;
                ?>
                                    <h4>Adicionar um novo álbum</h4>
                                    <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>
                <?php
                                }else{
                                    while($row = mysql_fetch_array($res)){                                        
                                        $texto1 = utf8_decode($row['texto1']);
                                        $texto2 = utf8_decode($row['texto2']);
                                    } 
                ?>
                                    <h4><?php echo $texto1?> <small>> Adicionar um Sub-Álbum</small></h4>
                <?php
                                }
                            }else{
                                $erro = true;
                ?>                                    
                                <h4>Adicionar um novo álbum</h4>
                                <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>
                <?php
                            }//if($res)                                                    
                        }else{
                ?>
                            <h4>Adicionar um novo álbum</h4>
                <?php
                        }//IF SUBALBUM
                        if(!$erro){
                ?>                    

                    <div class="panel panel-default">                    
                        <div class="panel-heading">Preencha o formulário abaixo:</div>
                        <div class="panel-body">
                            <?php if ($_GET['msg']=='error') { ?>
                            <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                           
                            <?php } ?> 
                            <form class="form-addalbum" name="formaddalbum" method="post" enctype="multipart/form-data">
                                <div class="addalbesquerda">
                                    <input type="hidden" name="op" value="add-alb"/>
                                    <?php
                                        if($subalbum){
                                            echo '<input type="hidden" name="idalbumpai" value="'.$albumpai.'"/>';
                                        }
                                    ?>
                                    <div class="form-group">
                                        <label> Título do Álbum:</label>
                                        <input type="text" id="texto1" name="texto1" class="form-control" placeholder="Título do álbum" style="width: 250px">
                                        <span class="help-block">Ex: Casamentos, Aniversários, Doces finos...</span>
                                    </div>                                    
                                    <div class="form-group">
                                        <label> Sub-título:</label>
                                        <input type="text" id="texto2" name="texto2" class="form-control" placeholder="Sub-título">
    <!--                                    <span class="help-block">(Opcional)</span>-->
                                    </div>
                                </div>
                                <div class="addalbdireita">
                                    <div class="form-group">
                                        <label>Foto do álbum <small>(miniatura)</small>:</label>
                                        <input type="file" id="filefoto" name="filefoto" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <img src="img/t_nofoto.png"><br>                                        
                                    </div>
                                </div>
                                <div class="addalbcontrol">
                                    <div class="form-group">
                                        <div class="buttonlinha ">
                                            <input type="button" id="submitaddalbum" name="submitaddalbum" value="Adicionar" class="btn btn-primary " >    
                                            <a href="portfolio.php?<?php echo ($subalbum ? "p=album&album=".$albumpai : "p=albuns") ?>" class="btn btn-default">Cancelar</a>                                                                       
                                        </div>                                    
                                    </div>
                                </div>
                            </form>
                                
                        </div>
                    </div>   
                <?php
                    }
                ?>
               <?php  break; case 'editalbum' : ?>
                   <?php     
                        $erro = false;
                        if(isset($_GET['album'])){
                            $idalbum = $_GET['album'];
                            $sql = "SELECT  texto1, texto2, foto, idpai
                                      FROM  albuns as a 
                                     WHERE  a.deletado = 0 
                                       AND  a.id = '$idalbum'
                                  ORDER BY  a.ordem ASC";
                            $res = mysql_query($sql, $con);
                            if($res){
                                while($row = mysql_fetch_array($res)){
//                                    $id      = $row['id']; 
                                    $idpai   = $row['idpai']; 
                                    $texto1  = utf8_decode($row['texto1']);
                                    $texto2  = utf8_decode($row['texto2']);
                                    $imgfoto = ( '0' != $row['foto'] ? $pastafotos.$idalbum."/".$row['foto'] : 'img/t_nofoto.png');
                                }
                    ?>                                  
                    <h4>Editar álbum</h4>
                    <div class="panel panel-default">                    
                        <div class="panel-heading">Preencha o formulário abaixo para editar :</div>
                        <div class="panel-body">
                            <?php if ($_GET['msg']=='error') { ?>
                                <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                           
                            <?php } ?> 
                            <form class="form-addalbum" name="formeditalbum" method="post" enctype="multipart/form-data">
                                <div class="addalbesquerda">                                
                                    <input type="hidden" name="op" value="edt-alb"/>
                                    <input type="hidden" name="idpai" value="<?php echo $idpai?>"/>
                                    <input type="hidden" name="idalbum" value="<?php echo $idalbum?>"/>
                                    <div class="form-group">
                                        <label> Título do Álbum:</label>
                                        <input type="text" id="texto1" name="texto1" class="form-control" placeholder="Título do álbum" value="<?php echo $texto1?>" style="width:250px">
                                        <span class="help-block">Ex: Casamentos, Aniversários, Doces finos...</span>
                                    </div>                                    
                                    <div class="form-group">
                                        <label> Sub-título:</label>
                                        <input type="text" id="texto2" name="texto2" class="form-control" placeholder="Sub-título" value="<?php echo $texto2?>">
    <!--                                    <span class="help-block">(Opcional)</span>-->
                                    </div>
                                </div>
                                <div class="addalbdireita">
                                    <input type="hidden" name="trocarimg" id="trocarimg" value="0"/>
                                    <label>Foto do álbum <small>(miniatura)</small>:</label>                                   
                                    <div class="form-group" id="boxfilefoto" style="display: none;">
                                        <input type="file" id="filefoto" name="filefoto" class="form-control">
                                        <a href="javascript:void(0)" onclick="return cancelarTrocarImagem()">Cancelar</a>
                                    </div>
                                    <div class="form-group" id="boxfoto"  style="display: block;">
                                        <img src="<?php echo $imgfoto ?>" id="i"/><br>
                                        <a href="javascript:void(0)" onclick="return trocarImagem()">Trocar Imagem</a>
                                    </div>                  
                                </div>
                                <div class="addalbcontrol">                                
                                    <div class="form-group">
                                        <div class="buttonlinha ">
                                            <input type="button" id="submiteditalbum" name="submiteditalbum" value="Salvar" class="btn btn-primary " >                                
                                            <a href="portfolio.php?p=album&album=<?php echo $idalbum?>" class="btn btn-default">Cancelar</a>                                   
                                        </div>                                    
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                    </div>   
                    <?php }else{
                            $erro = true;
                          }
                        }else{
                            $erro = true;                            
                        }
                      if ($erro) {?>
                        <h4>Editar álbum</h4>
                        <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                      
                    <?php } ?>
                                       
                    
                <?php break; case 'album': ?>
                    <?php     
                        $erro = false;
                        if(isset($_GET['album'])){
                            $idalbum = $_GET['album'];
                            
                            if($idalbum!=''){
                                $sql = "SELECT  texto1, texto2, idpai, oculto,
                                                (SELECT count(*) FROM fotos as f WHERE f.idalbum = $idalbum AND f.deletado =0) as qtd
                                          FROM  albuns as a 
                                         WHERE  a.deletado = 0 
                                           AND  a.id = '$idalbum'
                                      ORDER BY  a.ordem ASC";
                                $res = mysql_query($sql, $con);
                                if($res && mysql_num_rows($res)>0){
                                    while($row = mysql_fetch_array($res)){
                                        $texto1 = utf8_decode($row['texto1']);
                                        $texto2 = utf8_decode($row['texto2']);
                                        $idpai  = $row['idpai'];
                                        $oculto = $row['oculto'];
                                        $qtd    = $row['qtd'];
                                    }
                    ?>                      
                        <h4><?php echo $texto1 ?> <small><?php echo $texto2 ?></small></h4>
                        <div id="controle1">
                                <?php if($idpai=='0'){ ?>                            
                                <div class="btn-group btn-group-sm"><a class="btn btn-default" href="portfolio.php?p=albuns" title="Voltar"><span class="glyphicon glyphicon-arrow-left"></span> Álbuns</a></div>
                                <?php }else{ ?>
                                <div class="btn-group btn-group-sm"><a class="btn btn-default" href="portfolio.php?p=album<?php echo "&album=",$idpai;?>" title="Voltar"><span class="glyphicon glyphicon-arrow-left"></span> Voltar</a></div>
                                <?php } ?>
                            <div class="acontrole">
                                <!--<div class="btn-group btn-group-sm"><a class="btn btn-success" href="upfoto.php?a=<?php echo $idalbum ?>"><span class="glyphicon glyphicon-camera"></span> Enviar fotos</a></div>-->
                                <!--<div class="btn-group btn-group-sm"><a class="btn btn-warning" href="portfolio.php?p=fotosordem&a=<?php echo $idalbum ?>"><span class="glyphicon glyphicon-move"></span> Organizar fotos</a></div>-->
                                <?php if($idpai=='0'){ ?>                            
                                <div class="btn-group btn-group-sm"><a class="btn btn-primary" href="portfolio.php?p=addalbum&ap=<?php echo $idalbum ?>"><span class="glyphicon glyphicon-plus-sign"></span> Criar Sub-álbum</a></div>
                                <?php } ?>
                                <div style="position: relative; vertical-align:middle; display: inline-block; margin: 0; padding: 0;">
                                    <div class="btn-group btn-group-sm" id="btnocultar" style="display:<?php echo ($oculto=='1'?'none':'block')?>; width:84px;" onclick="return ocultarAlbum(<?php echo $idalbum ?>)">
                                        <a class="btn btn-info" title="Click para tornar Privado">
                                           <span class="glyphicon glyphicon-eye-open"></span>&nbsp; Público
                                        </a>
                                    </div>
                                    <div class="btn-group btn-group-sm" id="waiting" style="display:none; width:84px;" onclick="return ocultarAlbum(<?php echo $idalbum ?>)">
                                        <a class="btn btn-default" title="Salvando" style="width:84px;">
                                            <img src="images/loading.gif" height="19"/>
                                        </a>
                                    </div>
                                    <div class="btn-group btn-group-sm" id="btnmostrar" style="display:<?php echo ($oculto=='0'?'none':'block')?>; width:84px;" onclick="return mostrarAlbum(<?php echo $idalbum ?>)">
                                        <a class="btn btn-warning" title="Click para tornar Público">
                                           <span class="glyphicon glyphicon-eye-close"></span>&nbsp; Privado
                                        </a>
                                    </div>
                                </div>
                                <div class="btn-group btn-group-sm">
                                    <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
                                        <span class="glyphicon glyphicon-edit"></span> Editar&nbsp;&nbsp;<span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="portfolio.php?p=editalbum&album=<?php echo $idalbum ?>"><span class="glyphicon glyphicon-picture"></span> Títulos e Miniatura</a></li>
                                        <!--<li><a href="portfolio.php?p=ordemfotos&album=<?php echo $idalbum ?>"><span class="glyphicon glyphicon-move"></span>Ordem das fotos</a></li>-->                                    
                                    </ul>
                                </div>                                
                                <div class="btn-group btn-group-sm">
                                    <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#" >
                                        <span class="glyphicon glyphicon-trash"></span> Excluir Álbum&nbsp;&nbsp;<span class="caret"></span>
                                    </a>    
                                    <ul class="dropdown-menu" role="menu">
                                      <li><a href="javascript:void(0)" onclick="return excluirAlbum(<?php echo $idalbum ?>)"><span class="glyphicon glyphicon-folder-open"></span> Este álbum</a></li>
                                    </ul>
                                </div>
                            </div>  
                        </div>  
                        <div class="clearfix"></div>
                        
                        <?php if (isset($_GET['msg'])){ switch ($_GET['msg']) {
                               case '1': ?> <div class="alert alert-success" style="margin: 0"><b>Finalizado!</b> Todas as fotos foram excluídas com sucesso.!</div> <?php break; ?>                        
                        <?php  case 'added': ?> <div class="alert alert-success"><b>Sucesso!</b> Novo álbum adicionado com sucesso!</div> <?php break; ?>
                        <?php  case 'edited': ?> <div class="alert alert-info"><b>Salvo!</b> As novas informações foram salvas com sucesso!.</div><?php break; ?>                         
                        <?php     }//switch
                              }?>
                                           
                        <div id="subalbumfotos">
                        <?php
                                $sql = "SELECT  id, texto1, texto2, foto
                                          FROM  albuns as a 
                                         WHERE  a.deletado = 0 
                                           AND  a.idpai = $idalbum 
                                      ORDER BY  a.ordem ASC";
                                $res = mysql_query($sql, $con);
                                $c = 0; $albvazio = true;
                                while($row = mysql_fetch_array($res)){            
                                    $albvazio = false;
                                    $id     = $row['id'];
                                    $texto1 = utf8_decode($row['texto1']);
                                    $texto2 = utf8_decode($row['texto2']);
                                    if($row['foto']!="0"){
                                        $thumb  = $pastafotos.$id."/".$row['foto'];
                                    }else{
                                        $thumb  = "img/t_nofoto.png";
                                    }
                        ?>
                                <div class="itemalbum-sub">                           
                                    <div class="thumbnail">
                                        <div class="thumbimg-sub" title="<?php echo $texto1?>">
                                            <img src="<?php echo $thumb?>">
                                         </div>                            
                                         <div class="caption">
                                           <div class="atitulo-sub" title="<?php echo $texto1?>"><?php echo $texto1?></div>
                                           <!--<div class="adescricao-sub" title="<?php echo $texto2?>"><?php echo $texto2?></div>-->
                                             <div class="btn-group btn-group-justified" >
                                                  <a class="btn btn-primary btn-sm btn-group btn-group-justified" style="width: 75%" href="portfolio.php?p=album&album=<?php echo $id ?>" role="button"><span class="glyphicon glyphicon-folder-open"></span> Abrir</a>     
                                                  <a class="btn btn-info btn-sm btn-group btn-group-justified" style="width: 25%" href="portfolio.php?p=editalbum&album=<?php echo $id ?>" role="button" title="Editar"><span class="glyphicon glyphicon-edit"></span></a>
                                             </div>      
                                         </div>      
                                    </div>
                                </div>
                        <?php
                                }
                        ?>                            
                            <div style="clear: both"></div>
                            </div>
                        <?php
                            $sql = "SELECT  id, name
                                      FROM  fotos as f 
                                     WHERE  f.idalbum = '$idalbum'
                                       AND  f.deletado = 0
                                  ORDER BY  f.ordem ASC";
                            $res2 = mysql_query($sql, $con);
                            
                        ?>                      
                            <div id="albumfotos">
                            <div id="controle2">
                                <div class="btn-group btn-group-sm"><a class="btn btn-success" href="upfoto.php?a=<?php echo $idalbum ?>"><span class="glyphicon glyphicon-camera"></span> Enviar fotos</a></div>
                                <div class="btn-group btn-group-sm">
                                    <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#" >
                                        <span class="glyphicon glyphicon-trash"></span> Excluir Fotos&nbsp;&nbsp;<span class="caret"></span>
                                    </a>    
                                    <ul class="dropdown-menu" role="menu">
                                      <li><a href="javascript:void(0)" onclick="return mostrarExcluir()" id="togglexcluir"><span class="glyphicon glyphicon-picture"></span> Algumas Fotos</a></li>
                                      <li><a href="javascript:void(0)" onclick="return excluirAllFoto(<?php echo $idalbum ?>)" id="togglexcluir"><span class="glyphicon glyphicon-th-large"></span> Todas as Fotos</a></li>
                                    </ul>
                                </div>
                            </div>
                                <h4 style="margin:4px 0 15px 5px;">Fotos <small>(<span id="ftoqtd"><?php echo $qtd; ?></span>)</small></h4>                                                        
                            
                        <?php
                            if($res2){
                                if(mysql_num_rows($res2)==0){
                                ?>                      
                                <div class="alert alert-info"><b>Sem fotos</b> Este álbum não possui fotos.</div>  
                                <?php
                                }
                                while($row2 = mysql_fetch_array($res2)){     
                                    $id = $row2['id'];
                                    $name = $row2['name'];
                        ?>                      
                                <div class="tfoto"  idfoto="<?php echo $id ?>">
                                    <a class="thumbnail" href="<?php echo $pastafotos,$idalbum,"/",$name ?>" rel="lightbox[]" >
                                        <div class="afoto" style="background: url('<?php echo $pastafotos,$idalbum,"/",'t_',$name ?>') center center no-repeat;"></div>
                                    </a>                    
                                    <div class="btnexcluir"><a onclick="return excluirFoto(<?php echo $id ?>)" class="aexcluir" id="<?php echo $id ?>"><span class="glyphicon glyphicon-trash"></span>Excluir</a></div>
                                </div>
                        <?php 
                                    }//while
                                }else{
                                   //Select error
                                    $erro = true;
                                }
                              }else{
                               //Select sem resultado
                                $erro = true;
                              }
                            }else{
                                $erro = true;
                            }
                        }else{
                           //Get sem album
                            $erro = true;                            
                        }
                      if ($erro) {?>
                        <h4>Álbum <small>(Desconhecido)</small></h4>
                        <div class="alert alert-danger"><b>Ops!</b> Algo não está certo. Tente novamente mais tarde.</div>                      
                    <?php } ?>
                        <div style="clear: both"></div>
                    </div>                    
                    
                                            
                <?php break; case 'fotosordem': ?>
                    <?php     
                        $erro = false;
                        if(isset($_GET['a'])){
                            $idalbum = $_GET['a'];
                            $sql = "SELECT  texto1, texto2
                                      FROM  albuns as a 
                                     WHERE  a.deletado = 0 
                                       AND  a.id = '$idalbum'
                                  ORDER BY  a.ordem ASC";
                            $res = mysql_query($sql, $con);
                            if($res){
                                while($row = mysql_fetch_array($res)){                                                
                                    $texto1 = utf8_decode($row['texto1']);
                                    $texto2 = utf8_decode($row['texto2']);
                                }
                    ?>                      
                        <h4><?php echo $texto1 ?> <small><?php echo $texto2 ?></small></h4>
                        <div class="acontrole btn-group btn-group-sm">
                            <a class="btn btn-default" href="portfolio.php?p=album&album=<?php echo $idalbum ?>" title="Voltar"><span class="glyphicon glyphicon-arrow-left"></span> Voltar sem salvar</a>
                            <a class="btn btn-primary" href="portfolio.php?p=albuns" title="Voltar"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar esta ordem</a>
                        </div>
                        <div class="clearfix"></div>
                        <div id="albumfotos">
                        <?php
                            $sql = "SELECT  id, name
                                      FROM  fotos as f 
                                     WHERE  f.idalbum = '$idalbum'
                                       AND  f.deletado = 0
                                  ORDER BY  f.ordem ASC";
//                            die($sql);
                            $res2 = mysql_query($sql, $con);
                            if($res2){
                                if(mysql_num_rows($res2)==0){
                                ?>                      
                                <div class="alert alert-info"><b>Sem fotos</b> Este álbum ainda não possui fotos.</div>  
                                <?php
                                }
//                                echo '<div class="recebeDrag">';
                                while($row2 = mysql_fetch_array($res2)){     
                                    $id = $row2['id'];
                                    $name = $row2['name'];
                        ?>                   
<!--                                <div class="itemDrag">                                    
                                    <div class="area">                                    -->
                                        <a href="<?php echo $pastafotos,$idalbum,"/",$name ?>" rel="lightbox[example]" title="<?php echo $texto1 ?>" class="tfoto thumbnail">
                                            <!--<div class="afoto" style="background: url('<?php echo $pastafotos,$idalbum,"/",'t_',$name ?>') center center no-repeat;"></div>-->
                                        </a>                    
<!--                                    </div>
                                </div>-->
                            <!--</div>-->
                        <?php 
                                }//while
//                                echo '</div>';
                            }else{
                               //Select error
                                $erro = true;
                            }
                          }else{
                           //Select sem resultado
                            $erro = true;
                          }
                        }else{
                           //Get sem album
                            $erro = true;                            
                        }
                      if ($erro) {?>
                        <h4>Álbum <small>(Desconhecido)</small></h4>
                        <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                      
                    <?php } ?>
                    </div>    
                    <?php                     
                        if (!$erro) {
                    ?>
                            <a class="btn btn-primary" href="portfolio.php?p=albuns" title="Voltar"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar esta ordem</a>
                        </div>                                                        
                    <?php 
                    }
                    ?>        
                
                <?php break; case 'videos': ?>
                    
                    <div id="vidtop">
                        <h4 class="vidtit">Todos vídeos</h4>
                        <div id="vidcontrole">
                            <div id="btnitemvidmove" class="btn btn-default btn-sm" title="Mostrar controle para organizar videos">
                                <span class='glyphicon glyphicon-move'></span> <span id='btntext'>Organizar videos</span>
                            </div>                            
                        </div>
                    </div>
                        <?php
                            $sql = "SELECT  id, texto1, texto2, texto3, watchv
                                      FROM  videosyt
                                     WHERE  deletado = 0
                                  ORDER BY  ordem ASC";
//                            die($sql);
                            $res = mysql_query($sql, $con);
                            if($res){
                                if(mysql_num_rows($res)==0){
                                ?>                      
                                <div class="alert alert-info"><b>Sem fotos</b> Este álbum ainda não possui fotos.</div>  
                                <?php
                                }else{
                                ?>
                                <div class="boxvideos">                                 
                                    <ul id="sortablev">
                                <?php
                                    while($row = mysql_fetch_array($res)){
                                        $id = $row['id'];
                                        $texto1 = utf8_decode($row['texto1']);
                                        $texto2 = utf8_decode($row['texto2']);
                                        $texto3 = utf8_decode($row['texto3']);
                                        $watchv = utf8_decode($row['watchv']);
                                ?>                      
                                        <li idvid="<?php echo $id?>" class="livideo">
                                            <div class="itemvideo" idvid="<?php echo $id?>" >
                                                <div class="videopic">
                                                    <img src="https://i1.ytimg.com/vi/<?php echo $watchv ?>/default.jpg"/>
                                                </div>
                                                <div class="videotit">
                                                    <?php echo $texto1 ?>
                                                </div>
                                                <div class="videocontrole">
                                                    <a class="btn btn-info btn-xs" href="portfolio.php?p=editvideo&vid=<?php echo $id ?>" title="Editar"><span class="glyphicon glyphicon-edit"></span> Editar</a>
                                                    <a class="btn btn-danger btn-xs" href="javascript:excluirVideo(<?php echo $id ?>)" title="Editar">&nbsp;<span class="glyphicon glyphicon-trash"></span>&nbsp;</a>
                                                </div>
                                                <div class="videomove">
                                                    <span class="glyphicon glyphicon-move"></span>
                                                </div>
                                            </div>
                                        </li>
                                <?php
                                    }//while
                                ?>                      
                                    </ul>
                                </div>
                                <?php
                                }//if                            
                                ?>                      
                            <?php
                            } else {                           
                            ?>                      
                                <div class="alert alert-danger"><b>Ocorreu um erro </b> Tente novamente mais tarde.</div>  
                            <?php
                            }                           
                            ?>                      
                    
                <?php break; case 'addvideo': ?>
                    
                    <h4>Adicionar videos</h4>
                    <div class="panel panel-default">        
                        <div class="panel-heading">Preencha o formulário abaixo:</div>
                        <div class="panel-body">
                        <?php if (isset($_GET['msg'])){ switch ($_GET['msg']) {
                               case '1': ?> <div class="alert alert-success" style="margin: 0"><b>Finalizado!</b> Todas as fotos foram excluídas com sucesso.!</div> <?php break; ?>                        
                        <?php  case 'added': ?> <div class="alert alert-success"><b>Sucesso!</b> Novo álbum adicionado com sucesso!</div> <?php break; ?>
                        <?php  case 'edited': ?> <div class="alert alert-info"><b>Salvo!</b> As novas informações foram salvas com sucesso!.</div><?php break; ?>                         
                        <?php  case 'error': ?> <div class="alert alert-danger"><b>Ocorreu um erro</b> Tente novamente mais tarde.</div><?php break; ?>                         
                        <?php     }//switch
                              }?>
                            <form class="formaddvid" name="formaddvid" method="post" enctype="multipart/form-data">
                                <div class="addesquerda">
                                    <input type="hidden" name="op" value="add-vid"/>
                                    <div class="form-group">
                                        <label> Link YouTube:</label>
                                        <a href="javascript:verificaVideo()" class="btn btn-default" style="float: right; margin: 25px 0 0 0">Verificar</a>
                                        <input type="text" id="texto2" name="texto2" class="form-control" placeholder="http://www.youtube..." value="http://www.youtube.com/watch?v=" style="width: 265px">
                                        <span class="help-block"><small>Ex: http://www.youtube.com/watch?v=mXMM_EONips</small></span>
                                    </div>
                                    <div class="form-group">
                                        <label> Título do vídeo:</label>
                                        <input type="text" id="texto1" name="texto1" class="form-control" placeholder="Título   " style="width: 250px">
                                        <!--<span class="help-block">Nome dos autores do depoimento.</span>-->
                                    </div>                                    
                                    <div class="form-group">
                                        <label> Descrição:</label>
                                        <textarea id="texto3" name="texto3" class="form-control" placeholder="Descrição" style="height:100px;"></textarea>
                                        <!--<span class="help-block">(Opcional)</span>-->
                                    </div>
                                </div>
                                <div class="adddireita">
                                    <div id="iframevideo">
                                        
                                    </div>
                                </div>
                                <div class="addalbcontrol">
                                    <div class="form-group">
                                        <div class="buttonlinha ">
                                            <input type="button" id="submitaddvid" name="submitaddvid" value="Adicionar" class="btn btn-primary " >    
                                            <a href="portfolio.php" class="btn btn-default">Cancelar</a>                                                                       
                                        </div>                                    
                                    </div>
                                </div>
                            </form>                                
                        </div>
                    </div> 
                    
                <?php break; case 'editvideo': ?>
                    
                    <h4>Editar video</h4>
                    <div class="panel panel-default">        
                        <div class="panel-heading">Preencha o formulário abaixo:</div>
                        <div class="panel-body">
                            <?php if ($_GET['msg']=='error') { ?>
                                <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                           
                            <?php } ?> 
                            <?php 
                            if(isset($_GET['vid'])){
                                $idvideo = $_GET['vid'];        
                                $sql = "SELECT  id, texto1, texto2, texto3, watchv
                                          FROM  videosyt
                                         WHERE  deletado = 0 
                                           AND  id = $idvideo
                                      ORDER BY  ordem ASC";
   
                                $res = mysql_query($sql, $con);
//                                echo $sql;
                                if($res && mysql_num_rows($res)>0){
                                    while($row = mysql_fetch_array($res)){
                                        $id = $row['id'];
                                        $texto1 = utf8_decode($row['texto1']);
                                        $texto2 = utf8_decode($row['texto2']);
                                        $texto3 = utf8_decode($row['texto3']);
                                        $watchv = utf8_decode($row['watchv']);
                            ?>                                 
                            <form class="formeditvid" name="formeditvid" method="post" enctype="multipart/form-data">
                                <div class="addesquerda">
                                    <input type="hidden" name="op" value="edt-vid"/>
                                    <input type="hidden" name="idvideo" value="<?php echo $id ?>"/>
                                    <div class="form-group">
                                        <label> Link YouTube:</label>
                                        <a href="javascript:verificaVideo()" class="btn btn-default" style="float: right; margin: 25px 0 0 0">Verificar</a>
                                        <input type="text" id="texto2" name="texto2" class="form-control" placeholder="http://www.youtube..." value="<?php echo $texto2 ?>" style="width: 265px">
                                        <span class="help-block"><small>Ex: http://www.youtube.com/watch?v=mXMM_EONips</small></span>
                                    </div>
                                    <div class="form-group">
                                        <label> Título do vídeo:</label>
                                        <input type="text" id="texto1" name="texto1" class="form-control" placeholder="Título" value="<?php echo $texto1 ?>" style="width: 250px">
                                        <!--<span class="help-block">Nome dos autores do depoimento.</span>-->
                                    </div>                                    
                                    <div class="form-group">
                                        <label> Descrição:</label>
                                        <textarea id="texto3" name="texto3" class="form-control" placeholder="Descrição" style="height:100px;"><?php echo $texto3 ?></textarea>
                                        <!--<span class="help-block">(Opcional)</span>-->
                                    </div>
                                </div>
                                <div class="adddireita">
                                    <div id="iframevideo">
                                        
                                    </div>
                                </div>
                                <div class="addalbcontrol">
                                    <div class="form-group">
                                        <div class="buttonlinha ">
                                            <input type="button" id="submiteditvid" name="submiteditvid" value="Salvar" class="btn btn-primary " >    
                                            <a href="portfolio.php?p=videos" class="btn btn-default">Cancelar</a>                                                                       
                                        </div>                                    
                                    </div>
                                </div>
                            </form>  
                            <?php }
                                }else{
                            ?>
                                    <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div> 
                            <?php
                                }
                            }else{
                            ?>
                                    <div class="alert alert-danger"><b>Um erro ocorreu.</b> Tente novamente mais tarde.</div>                                 
                            <?php
                            }
                            ?>
                                
                        </div>
                    </div> 
                <?php break; default: ?>
                
                    <h4>Início</h4>
                    
                <?php  
                    break;              
                    }//switch($page)  
                ?>

<!--                    <div class="panel panel-default">                    
                        <div class="panel-body">
                            
                        </div>
                    </div>            -->
                    
                </div><!--id="conteudodir"-->                

            </div>
        </div>
        <?php
            include_once 'php/content/rodape.php';
        ?>   
    </body>
</html>