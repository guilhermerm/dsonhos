<?php
session_start();
include_once './lib/security.php';
include_once 'lib/conection.php';
include_once("./lib/wideimage/WideImage.php");
include_once './lib/funcoes.php';

//Imagens do Produto
$pastafotos = "../dsonhos/produtos/fotos/";
$maxwidth   = 300;
$maxheight  = 300;

switch ($_POST['op']){
    
    //ADICIONAR NOVO PRODUTO
    case 'add-prod':       
        
        //GET valores dos inputs
        $texto1 = mysql_escape_string(utf8_encode(trim($_POST['texto1'])));
        $link = nametolink($_POST['texto1']);
        $texto2 = mysql_escape_string(utf8_encode(trim($_POST['texto2'])));
        $texto3 = mysql_escape_string(utf8_encode(trim($_POST['texto3'])));
        $categoria = $_POST['scat'];
        $verpreco = (isset($_POST['verpreco']))?'0':'1';        
        
        $grouptmp = $_POST['grouptmp'];
        
        //Monta SQL para inserir no Banco               
        $ordem = "(SELECT IF(MAX(d.ordem),MAX(d.ordem)+1,0) as ordem FROM produtos as d)";                      
        $sql = "INSERT INTO produtos(ordem, texto1, texto2, texto3, link, verpreco, categoria, deletado) 
                     VALUES  ($ordem, '$texto1', '$texto2', '$texto3', '$link', $verpreco, $categoria, 0)";        
        $res = mysql_query($sql, $con);
        
        $idprod = mysql_insert_id();
        
        $sql = "UPDATE prodfotos SET idprod = $idprod WHERE grouptmp = $grouptmp";
        $res2 = mysql_query($sql, $con);
        
        if($res){ 
            header("Location:./produtos.php?p=prods&msg=added");
            exit;                                 
        }else{
            //Erro
            header("Location:./produtos.php?p=addprod&msg=error");
            exit;
        }

    break;
    
    /***************************************************************************/
    
    //EDITAR ALBUM
    case 'edt-prod':

        $id   = $_POST['idprod'];
        $texto1 = mysql_escape_string(utf8_encode(trim($_POST['texto1'])));
        $texto2 = mysql_escape_string(utf8_encode(trim($_POST['texto2'])));
        $texto3 = mysql_escape_string(utf8_encode(trim($_POST['texto3'])));
        $link = nametolink($_POST['texto1']);
        $categoria = $_POST['scat'];       
        $verpreco = (isset($_POST['verpreco']))?'0':'1';        
        
//        $grouptmp = $_POST['grouptmp'];        
        
//        //Salvar fotos possivelmente Enviadas
//        $sql = "UPDATE prodfotos SET idprod = $idprod WHERE grouptmp = $grouptmp";
//        $res2 = mysql_query($sql, $con);
        
        $sql = "UPDATE  produtos
                   SET    texto1 = '$texto1'
                        , texto2 = '$texto2'
                        , link   = '$link'
                        , texto3 = '$texto3'
                        , verpreco = $verpreco
                        , categoria = $categoria
                 WHERE  id = $id
                ";

        $res = mysql_query($sql, $con);      
        
        if($res){             
            header("Location:./produtos.php?p=prods&msg=edited");
        }else{
            //Erro
            header("Location:./produtos.php?p=editprod&prod=$id&msg=error");           
            exit;
        }

    break;
    
    /***************************************************************************/
    
    //EXCLUIR UM PRODUTOS
    case 'exc-prod' :        

        $idprod = $_POST['idprod'];

        $sql = "UPDATE produtos SET deletado = 1 WHERE id = $idprod";
        
        $res = mysql_query($sql, $con);
        if($res){
            echo 'true';
            exit;
        }else{            
            echo 'false';
            exit;
        }
        
        echo 'Não pode ser Excluido';
        exit;
    break;
        
    /***************************************************************************/
    //SALVAR ORDEM PRODUTOS
    case 'ord-prod' :              
        $ordem = $_POST['ordem'];        
        $lista = explode(",",$ordem);
        
        $tam = count($lista);
        for($i=0;$i<$tam-1;$i++){
            $sql = " UPDATE produtos SET ordem =".($i+1)." WHERE id = $lista[$i] ";        
            $res = mysql_query($sql, $con);
        }
        
        if($res){
            echo 'true';
            exit;
        }else{            
            echo 'false';
            exit;
        }
        
        echo 'Não pode ser excluído';
        exit;
    break;
        
    /***************************************************************************/
    //UPLOAD de FOTOS
    case 'up-fto' :              
        
	// Work-around for setting up a session because Flash Player doesn't send the cookies
	if (isset($_POST["PHPSESSID"])) {
		session_id($_POST["PHPSESSID"]);
	}
//	session_start();
             
        $grouptmp = (isset($_POST['grouptmp'])? $_POST['grouptmp'] : "A");        
        $idprod   = (isset($_POST['idprod'])? $_POST['idprod'] : "0");
        
        //Criar pasta do produto se não existir.
        if (!file_exists($pastafotos)){
            mkdir($pastafotos, 0777, true);
        }

	// The Demos don't save file
        if (!isset($_FILES["Filedata"]) || !is_uploaded_file($_FILES["Filedata"]["tmp_name"]) || $_FILES["Filedata"]["error"] != 0) { 
            echo "Ocorreu um erro ao enviar as imagens. Tente novamente mais tarde ou entre em contato com o administrador.";             
        }else{
            
            $img_name = "prod".md5(uniqid("img", true)).".jpg";
            $img_endereco = $pastafotos.$img_name;
            $img_endthumb = $pastafotos.'t_'.$img_name;
            $img_endereco_tmp = $pastafotos.'ori_'.$img_name;

            /*Aqui é onde movemos o nosso upload para a pasta files acessível pelo servidor */
            move_uploaded_file($_FILES["Filedata"]["tmp_name"], $img_endereco_tmp); 
            
            /*Redimensionar imagem e criar thumb imagem frontend*/
//            $resizeObj = new resize($img_endereco);
            $image = WideImage::load($img_endereco_tmp);
            
            //IMAGEM
            if($image->getWidth()>$maxwidth ||$image->getHeight()>$maxheight){
                $image = $image->resize($maxwidth, $maxheight);
            }
            $image->saveToFile($img_endereco);                        
            
//            //THUMB
//            $resizeObj -> resizeImage(96, 96, 'crop');
//            $resizeObj -> saveImage($pastafotos."t_".$img_name,90);
            //THUMB
            $image = $image->resize(96, 96, 'outside');
            $image = $image->crop("center","middle",96,96);
            $image->saveToFile($img_endthumb);

            //Adicionar logo marca na imagem              
            $img1 = imagecreatefromjpeg($img_endereco);
            $img2 = imagecreatefrompng('img/marca_dsonhos-2.png');
              // Mantem definições de transparencia
              imageAlphaBlending($img2, true);
              imageSaveAlpha($img2, true);            
            //mescla as imanges
            imagecopy($img1, $img2, (imagesx($img1)-(imagesx($img2))), (imagesy($img1)-(imagesy($img2))), 0, 0, imagesx($img2), imagesy($img2));            
            imagejpeg($img1,$img_endereco,90);
            
            //Adicionar reflexo no thumb
            $img1 = imagecreatefromjpeg($img_endthumb);
            $img2 = imagecreatefrompng('img/dsonhos-reflexo-thumb-96.png');
              // Mantem definições de transparencia
              imageAlphaBlending($img2, true);
              imageSaveAlpha($img2, true);            
            //mescla as imanges
            imagecopy($img1, $img2, 0, 0, 0, 0, imagesx($img2), imagesy($img2));            
            imagejpeg($img1,$img_endthumb,90);                          
            
            /*Salvar endereço no banco*/            
            //Inserir imagem
             $ordem = "(SELECT COUNT(*)+1 as ordem FROM prodfotos as pf WHERE pf.idprod = $idprod OR pf.grouptmp = $grouptmp)";            
//             $ordem = "(SELECT IF(MAX(f.ordem),MAX(f.ordem)+1,0) as ordem FROM fotos as f WHERE f.idalbum = $idalbum)";            
             $sql = "INSERT INTO prodfotos (idprod, foto, ordem, deletado, grouptmp) VALUES ($idprod,'$img_name',$ordem,'0',$grouptmp)";
             $res = mysql_query($sql, $con);            
        }	
	exit(0);
    break;
        
    //CARREGAR FOTOS NO ADD PRODUTOS VIA AJAX
    case 'load-fts' :
        
        $grouptmp = (isset($_POST['grouptmp'])?$_POST['grouptmp']:"");
        $idprod = (isset($_POST['idprod'])?$_POST['idprod']:"");
        
        if($idprod==""){
            $sql = "SELECT id, foto FROM prodfotos WHERE grouptmp = '$grouptmp' AND deletado = 0";
        }else{
            $sql = "SELECT id, foto FROM prodfotos WHERE idprod = '$idprod' AND deletado = 0";
        }
        $res = mysql_query($sql);
        
        if($res && mysql_num_rows($res)>0){
            while($row = mysql_fetch_array($res)){
                $id = $row['id'];
                $foto = $row['foto'];
                echo "<div class='prodfotos' idfoto='$id'>
                        <a href='$pastafotos$foto' class='prodfotosopen' rel='lightbox[]'></a>
                        <a href='javascript: excluirFto($id)' class='prodfotosdel' title='Excluir'><span class='glyphicon glyphicon-trash'></span></a>
                        <img src='$pastafotos\\t_$foto'/>                    
                      </div>\n";
            }
//                        <a class='prodfotosmove' title='Excluir'><span class='glyphicon glyphicon-move'></span></a>
        }else{
            echo 'false';
        }
        exit(0);
    break;
    
    //EXCLUIR UM PRODUTOS
    case 'exc-fto' :        

        $idfto = $_POST['idfoto'];

        $sql = "UPDATE prodfotos SET deletado = 1 WHERE id = $idfto";
        
        $res = mysql_query($sql, $con);
        if($res){
            echo 'true';
            exit;
        }else{            
            echo 'false';
            exit;
        }
        
        echo 'Não pode ser Excluido';
        exit;
    break;
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    /*                                                                           */
    /*                          C A T E G O R I A S                              */
    /*                                                                           */
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    //ADICIONAR NOVA CATEGORIA
    case 'add-cat':       
        
        //GET valores dos inputs
        $texto1 = mysql_escape_string(utf8_encode(trim($_POST['texto1'])));
        $texto2 = mysql_escape_string(utf8_encode(trim($_POST['texto2'])));    
        $link = nametolink($_POST['texto1']);
        
        //Monta SQL para inserir no Banco               
        $ordem = "(SELECT IF(MAX(pc.ordem),MAX(pc.ordem)+1,0) as ordem FROM prodcategorias as pc)";                      
        $sql = "INSERT INTO prodcategorias(ordem, texto1, texto2, link, deletado) 
                     VALUES  ($ordem, '$texto1', '$texto2', '$link',0)";        
        $res = mysql_query($sql, $con);
        
        if($res){ 
            header("Location:./produtos.php?p=cats&msg=added");
            exit;                                 
        }else{
            //Erro
            header("Location:./produtos.php?p=addcat&msg=error");
            exit;
        }

    break;
    
    /***************************************************************************/
    
    //EDITAR CATEGORIA
    case 'edt-cat':

        $id   = $_POST['id'];
        $texto1 = mysql_escape_string(utf8_encode(trim($_POST['texto1'])));
        $texto2 = mysql_escape_string(utf8_encode(trim($_POST['texto2'])));     
        $link = nametolink($_POST['texto1']);
                   
        $sql = "UPDATE  prodcategorias
                   SET  texto1 = '$texto1'
                        , texto2 = '$texto2'
                        , link   = '$link'
                 WHERE  id = $id
                ";
        $res = mysql_query($sql, $con);      
        
        if($res){             
            header("Location:./produtos.php?p=cats&msg=edited");
            exit;
        }else{
            //Erro
            header("Location:./produtos.php?p=editcat&cat=$id&msg=error");           
            exit;
        }

    break;
    
    /***************************************************************************/
    
    //EXCLUIR UMA CATEGORIA
    case 'exc-cat' :        

        $idcat = $_POST['idcat'];

        $sql = "UPDATE prodcategorias SET deletado = 1 WHERE id = $idcat";
        
        $res = mysql_query($sql, $con);
        if($res){
            echo 'true';
            exit;
        }else{            
            echo 'false';
            exit;
        }
        
        echo 'Não pode ser Excluido';
        exit;
    break;
        
    /***************************************************************************/
    //SALVAR ORDEM CATEGORIAS
    case 'ord-cat' :              
        $ordem = $_POST['ordem'];        
        $lista = explode(",",$ordem);
        
        $tam = count($lista);
        for($i=0;$i<$tam-1;$i++){
            $sql = " UPDATE prodcategorias SET ordem =".($i+1)." WHERE id = $lista[$i] ";        
            $res = mysql_query($sql, $con);
        }
        
        if($res){
            echo 'true';
            exit;
        }else{            
            echo 'false';
            exit;
        }        
        echo 'Não pode ser excluído';
        exit;
    break;    
    
    
    default :
            //Operação Ilegal
            header("Location:./produtos.php");
            exit;        
    break;
    
}    
?>
