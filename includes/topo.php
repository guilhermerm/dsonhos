<?php
    /* Formas de Layout:
     * $layout = 
     *    "menor" - Menor layout do topo e mais simples. 
     *    "maior"  - Layout com foto do slider maior.
     *    "inicio" - Layout da primeira página com slider maior 
     *               e quadro de bolos verticais.
     */

?>
            <!--<div id="bgraios"></div>-->

            <div id="logo">
                <a href="../inicio/inicio.php">
                    <img src="../../imagens/layout/logo-dsonhos.png"/>                
                </a>
            </div>
            
            <div id="menu">
                <?php include("../../includes/menu.php"); ?>
            </div>
<?php
    switch ($layout){
        case "inicio" :
?>            
            <div id="land-slider">
                <div id="quadro-bolos">
                    <div id="box">
                        <img src="../../imagens/layout/quadro-img-01.jpg"/>
                    </div>
                </div>
                <div id="slider-imagem">
                    <img src="../../imagens/layout/slide-img-01-.jpg"/>
                </div>
<!--                <div id="slider-imagem-maior">
                    <div id="slideshow">
                        <img src="../../imagens/layout/slide-img-01a.jpg" alt="" class="active" />
                        <img src="../../imagens/layout/slide-img-01b.jpg" alt="" class="active" />
                        <img src="../../imagens/layout/slide-img-01c.jpg" alt="" class="active" />
                        <img src="../../imagens/layout/slide-img-01a.jpg" alt="" class="active" />
                        <img src="../../imagens/layout/slide-img-01b.jpg" alt="" class="active" />
                        <img src="../../imagens/layout/slide-img-01c.jpg" alt="" class="active" />                    
                    </div>


                </div>-->
            </div>

<?php break; case "maior" :?>          

            <div id="land-slider">
                <div id="slider-imagem-maior">
                    <img src="../../imagens/layout/slide-img-01-boloborboletas.jpg" alt="" class="active" />
                </div>
            </div>

<?php break; case "menor": default: ?>

            <div id="land-slider">
                <div id="slider-imagem-menor">
                    <img src="../../imagens/layout/slide-img-02.jpg"/>
                </div>
            </div>            
<?php break;
    }//switch