<?php
    include_once '../../includes/manutencao.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="StyleSheet" type="text/css" href=" ../../css/estiloHome.css">
    <link rel="StyleSheet" type="text/css" href="./css/estiloAltaconfeitaria.css">    
    <link rel="SHORTCUT ICON" href="../../imagens/outras/favicon.png" type="image/x-icon" />

    <script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.js"></script>
    <script type="text/javascript" src="fancybox/jquery.easing-1.3.pack.js"></script>
    <script type="text/javascript" src="fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="fancybox/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css" media="screen"/>

    <title>: : D'Sonhos : :</title>

    <script>
            function click() {
                if (event.button==2||event.button==3) {
                    alert("Cópia Proibida! Conteúdo exclusivo D'Sonhos.");
                    oncontextmenu='return false';
                }
            }
            document.onmousedown=click;
            document.oncontextmenu = new Function("return false;");

    </script>
    <script type="text/javascript">
    /***Simple jQuery Slideshow Script Released by Jon Raasch (jonraasch.com) under FreeBSD license: free to use or modify, not responsible for anything, etc.  Please link out to me if you like it :)***/
    function slideSwitch(){var $active = $('#slideshow IMG.active');if ( $active.length == 0 ) $active = $('#slideshow IMG:last');var $next =  $active.next().length ? $active.next():$('#slideshow IMG:first');$active.addClass('last-active');$next.css({opacity: 0.0}).addClass('active').animate({opacity: 1.0}, 800, function(){$active.removeClass('active last-active');});}$(function(){setInterval( "slideSwitch()", 3000 );});
    </script>
</head>
<body onload="popup()">
    <?php include_once("../../includes/analysticsgoogle.php") ?>

    <div id="corpo">    
       
        <div id="boxtop"></div>
        <div id="boxcenter">
            
            <?php $layout = "maior"; include("../../includes/topo.php"); ?>          
            
            <div id="conteudo">
                    
                <div id="texto">
                    <p>A <b>D'SONHOS CONFEITARIA</b> é especializada no fornecimento de:</p>
                    <p>
                        <ul style="margin-left: 20px; font-size: 15px;">
                            <li>Maquetes e Bolo de corte</li>
                            <li>Bolos decorado, Personalizados ou Esculpidos,</li>
                            <li>Bem casados e Lua de Mel</li>
                        </ul>
                    </p>

                    <p>Compõe também nossa cartela de produtos: Doces finos, bombons tradicionais, mesa de saída, fonte de chocolate, cup cakes, etc.</p>

                    <p>Temos atendimento exclusivo com hora agendada para maior comodidade do cliente, pois entendemos que cada evento é único e especial.  Atendimento com hora marcada na Fábrica: Bairro Santo Antônio, Vitória - ES.</p>

                    <p>Os preços de nossos produtos são tabelados e não enviamos essas informações por e-mail, nosso cardápio de doces possui mais de 60 sabores, nossos bem casados foram eleitos por nossos clientes como o Melhor. Simplesmente incomparável!!!</p>

                    <p>Agende seu horário e se surpreenda com um atendimento na medida do seu <b>Sonho</b>... </p>
                </div>                
                
                <div id="box-links">
                    <div id="boxlink">
                    <a title="Depoimentos" href="../depoimentos/depoimentos.php" class="lin"><img id="ico" src="../../imagens/layout/ico-depoimentos.png"/></a>
                    <div class="titulo3">
                        <a title="Depoimentos" href="../depoimentos/depoimentos.php" class="lin"> Comentários dos clientes </a>
                    </div>
                    <div id="texto">
                        <p><a href="../depoimentos/depoimentos.php" class="lin"> A melhor maneira de nos conhecer é sabendo o que os nossos clientes estão dizendo.</a></p>
                    </div>
                </div>                
                <div id="boxlink">
                        <a title="Blogger" href="http://www.dsonhos.blogspot.com/" target="_blank"><img id="ico" src="../../imagens/layout/ico-blog.png"/></a>
                    <div class="titulo3">
                        <a title="Blogger" href="http://www.dsonhos.blogspot.com/" target="_blank" class="lin">Blog</a>
                    </div>
                    <div id="texto">
                        <p><a title="Blogger" href="http://www.dsonhos.blogspot.com/" target="_blank"  class="lin">Viagens, moda, decoração, matérias interessantes, curiosidades e não podemos esquecer da comida!</a></p>
                    </div>                                
                </div>
                <div id="boxlink">
                    <div class="titulo3">
                        Visite-nos também:
                    </div>
                    <div id="texto">
                        <p><a title="FaceBook D'Sonhos Eventos" href="http://www.facebook.com/dsonhoseventos" target="_blank"><img id="ico-14" height="14px" src="../../imagens/layout/ico-face.png"/>dsonhoseventos</a></p>
                        <p><a title="FaceBook Selma e Thiago Toledo" href="http://www.facebook.com/selma.t.toledo" target="_blank"><img id="ico-14" height="14px" src="../../imagens/layout/ico-face.png"/>selma.t.toledo</a></p>
                    </div>                                
                </div>
                </div>                
                
<!--                <div id="box-links">
                    <div id="boxlink">
                        <div class="titulo3">
                            Comentários dos clientes
                        </div>
                        <div id="texto">
                            A melhor maneira de nos conhecer é sabendo o que os nossos clientes estão dizendo.
                        </div>
                    </div>                
                    <div id="boxlink">
                        <div class="titulo3">
                            Blog
                        </div>
                        <div id="texto">
                            Viagens, moda, decoração, matérias interessantes, curiosidades e não podemos de esquecer da comida!
                        </div>                                
                    </div>
                    <div id="boxlink">
                        <div class="titulo3">
                            Visite-nos também:
                        </div>
                        <div id="texto">
                            Facebook<br>Youtube<br>Orkut
                        </div>                                
                    </div>
                </div>-->
            </div>
            
            
            <?php include("../../includes/msg_rodape.php"); ?>            
            
        </div>
      <div id="boxbottom"></div>
    <?php
            include("../../includes/rodape.php");
        ?>
    </div>

</body>
</html>
