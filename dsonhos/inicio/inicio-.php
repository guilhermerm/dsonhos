<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="StyleSheet" type="text/css" href=" ../../css/estiloHome.css">
    <link rel="StyleSheet" type="text/css" href="./css/estiloInicio.css">
    <link rel="stylesheet" type="text/css" href="css/slideshow.css" media="screen" />
    <link rel="SHORTCUT ICON" href="../../imagens/outras/favicon.png" type="image/x-icon" />

    <script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.js"></script>
    <script type="text/javascript" src="fancybox/jquery.easing-1.3.pack.js"></script>
    <script type="text/javascript" src="fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="fancybox/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css" media="screen"/>

    <title>: : D'Sonhos : :</title>

<!--    <script>
            function click() {
                if (event.button==2||event.button==3) {
                    alert("Cópia Proibida! Conteúdo exclusivo D'Sonhos.");
                    oncontextmenu='return false';
                }
            }
            document.onmousedown=click;
            document.oncontextmenu = new Function("return false;");

    </script>-->

    <!--<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>-->
    <script type="text/javascript">
    /***Simple jQuery Slideshow Script Released by Jon Raasch (jonraasch.com) under FreeBSD license: free to use or modify, not responsible for anything, etc.  Please link out to me if you like it :)***/
    function slideSwitch(){var $active = $('#slideshow IMG.active');if ( $active.length == 0 ) $active = $('#slideshow IMG:last');var $next =  $active.next().length ? $active.next():$('#slideshow IMG:first');$active.addClass('last-active');$next.css({opacity: 0.0}).addClass('active').animate({opacity: 1.0}, 800, function(){$active.removeClass('active last-active');});}$(function(){setInterval( "slideSwitch()", 3000 );});
    </script>

</head>
<body onload="popup()">
    <?php include_once("../../includes/analysticsgoogle.php") ?>

    <div id="corpo">    
        <div id="boxtop"></div>
        <div id="boxcenter">
            
            <?php $layout = "inicio"; include("../../includes/topo.php"); ?>
            
            <div id="conteudo">
                <div class="titulo1">
                    Seja bem-vindo, transforme seu sonho em realidade!
                </div>
                    
                <div id="texto">
                    <p>Aqui você encontrará uma pequena amostra do nosso trabalho.</p>                    
                    <p>Atendemos a todos os tipos de bolos e maquetes para casamento, aniversários infantis, 15 anos, etc. Também trabalhamos com: Doces, bombons finos, fondant, mini bolo, doces tradicionais, docinhos modelados e personalizados, cupcake, copinhos de chocolate, pirulitos de chocolate etc.</p>
                    <p>Entre em contato e agende um horário!<br>Atendimento personalizado e fotos exclusivas.</p>
                </div>                
                
            </div>
            
            <div id="box-links">
                <div id="boxlink">
                    <img id="ico" src="../../imagens/layout/ico-depoimentos.png"/>
                    <div class="titulo3">
                        <a title="Depoimentos" href="../publicidade/depoimentos.php" target="_blank" class="lin">Comentários dos clientes</a>
                    </div>
                    <div id="texto">
					<a title="Depoimentos" href="../publicidade/depoimentos.php" target="_blank" class="lin">
                        <p>A melhor maneira de nos conhecer é sabendo o que os nossos clientes estão dizendo.</p></a>                                            
                    </div>
                </div>                
                <div id="boxlink">
                        <a title="Blogger" href="http://www.dsonhos.blogspot.com/" target="_blank" class="lin"><img id="ico" src="../../imagens/layout/ico-blog.png"/></a>
                    <div class="titulo3">
                        <a title="Blogger" href="http://www.dsonhos.blogspot.com/" target="_blank" class="lin">Blog</a>
                    </div>
                    <div id="texto">
                        <p><a title="Blogger" href="http://www.dsonhos.blogspot.com/" target="_blank" class="lin">Viagens, moda, decoração, matérias interessantes, curiosidades e não podemos esquecer da comida!</a></p>
                    </div>                                
                </div>
                <div id="boxlink">
                    <div class="titulo3">
                        Visite-nos também:
                    </div>
                    <div id="texto">
                        
                        <p><a title="FaceBook" href="http://www.facebook.com/Dsonhos" target="_blank"><img id="ico-14" height="14px" src="../../imagens/layout/ico-face.png"/>Facebook</a></p>
                        <p><a title="Orkut" href="http://www.orkut.com.br/Main#Profile?uid=11019648720893110744" target="_blank"><img id="ico-14" height="14px" src="../../imagens/layout/ico-youtube.png"/>Youtube</a></p>
                        <p><a title="Youtube" href="http://www.youtube.com/user/dsonhosselma" target="_blank"><img id="ico-14" height="14px" src="../../imagens/layout/ico-orkut.png"/>Orkut</a></p>
                        
                    </div>                                
                </div>
            </div>
            
            <?php include("../../includes/msg_rodape.php"); ?>            
            
        </div>
    	<div id="boxbottom"></div>
		<?php
            include("../../includes/rodape.php");
        ?>
    </div>
<!--
<script type="text/javascript">
        function popup(){

                $.fancybox(
                                {
                          'href':"images/inauguracao-shopnortesul.jpg",
                          'transitionIn'	: 'fade',
                          'transitionOut'	: 'elastic',		  
                          'titlePosition' 	: 'over',
                          'titleFormat'		: null,
                          'padding'		: 0
                                }
                );
        }
</script>	-->

</body>
</html>
