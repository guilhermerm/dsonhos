<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="StyleSheet" type="text/css" href=" ../../css/estiloHome.css">
    <link rel="StyleSheet" type="text/css" href="./css/estiloAltaconfeitaria.css">    
    <link rel="SHORTCUT ICON" href="../../imagens/outras/favicon.png" type="image/x-icon" />

    <script type="text/javascript" src="fancybox/jquery-1.4.3.min.js"></script>    

    <title>: : D'Sonhos : :</title>

    <script>
            function click() {
                if (event.button==2||event.button==3) {
                    alert("Cópia Proibida! Conteúdo exclusivo D'Sonhos.");
                    oncontextmenu='return false';
                }
            }
            document.onmousedown=click;
            document.oncontextmenu = new Function("return false;");

    </script>

</head>
<body onload="popup()">
    <?php include_once("../../includes/analysticsgoogle.php") ?>

    <div id="corpo">    
        <div id="logo">
            <a href="../inicio/inicio.php">
                <img src="../../imagens/layout/logo-dsonhos.png"/>                
            </a>
        </div>
       
        <div id="boxtop"></div>
        <div id="boxcenter">
            
            <div id="menu">
                <?php include("../../includes/menu.php"); ?>                
            </div>
            
            <div id="land-slider">

                <div id="slider-imagem">
                    <img src="../../imagens/layout/slide-img-01-.jpg"/>
                </div>
            </div>
            
            <div id="conteudo">
                    
                <div id="texto">
                    <p>A <b>D'SONHOS ALTA CONFEITARIA</b> é especializada no fornecimento de:</p>
                    <p>
                        <ul style="margin-left: 20px; font-size: 15px;">
                            <li>Maquetes e Bolo de corte</li>
                            <li>Bolos decorado, Personalizados ou Esculpidos,</li>
                            <li>Bem casados e Lua de Mel</li>
                        </ul>
                    </p>

                    <p>Compõe também nossa cartela de produtos: Doces finos, bombons tradicionais, mesa de saída, fonte de chocolate, cup cakes, etc.</p>

                    <p>Temos atendimento exclusivo com hora agendada para maior comodidade do cliente, pois entendemos que cada evento é único e especial, os atendimentos são realizados por Selma e Thiago no Shopping Norte Sul, Piso 1, em Jardim Camburi.</p>

                    <p>Os preços de nossos produtos são tabelados e não enviamos essas informações por e-mail, nosso cardápio de doces possui mais de 60 sabores, nossos bem casados foram eleitos por nossos clientes como o Melhor. Simplesmente incomparável!!!</p>

                    <p>Os bolos e maquetes são calculados conforme o projeto desenvolvido com o cliente, mas temos maquetes de três andares para casamento a partir de R$ 400,00 e bombons a partir de R$ 97,00 o cento.</p>

                    <p>Agende seu horário e se surpreenda com um atendimento na medida do seu <b>Sonho</b>... </p>
                </div>                
                
                <div id="box-links">
                    <div id="boxlink">
                    <a title="Depoimentos" href="../depoimentos/depoimentos.php" class="lin"><img id="ico" src="../../imagens/layout/ico-depoimentos.png"/></a>
                    <div class="titulo3">
                        <a title="Depoimentos" href="../depoimentos/depoimentos.php" class="lin"> Comentários dos clientes </a>
                    </div>
                    <div id="texto">
                        <p><a href="../depoimentos/depoimentos.php" class="lin"> A melhor maneira de nos conhecer é sabendo o que os nossos clientes estão dizendo.</a></p>
                    </div>
                </div>                
                <div id="boxlink">
                        <a title="Blogger" href="http://www.dsonhos.blogspot.com/" target="_blank"><img id="ico" src="../../imagens/layout/ico-blog.png"/></a>
                    <div class="titulo3">
                        <a title="Blogger" href="http://www.dsonhos.blogspot.com/" target="_blank" class="lin">Blog</a>
                    </div>
                    <div id="texto">
                        <p><a title="Blogger" href="http://www.dsonhos.blogspot.com/" target="_blank"  class="lin">Viagens, moda, decoração, matérias interessantes, curiosidades e não podemos esquecer da comida!</a></p>
                    </div>                                
                </div>
                <div id="boxlink">
                    <div class="titulo3">
                        Visite-nos também:
                    </div>
                    <div id="texto">
                        
                        <p><a title="FaceBook" href="http://www.facebook.com/Dsonhos" target="_blank"><img id="ico-14" height="14px" src="../../imagens/layout/ico-face.png"/>Facebook</a></p>
                        <p><a title="Orkut" href="http://www.orkut.com.br/Main#Profile?uid=11019648720893110744" target="_blank"><img id="ico-14" height="14px" src="../../imagens/layout/ico-youtube.png"/>Youtube</a></p>
                        <p><a title="Youtube" href="http://www.youtube.com/user/dsonhosselma" target="_blank"><img id="ico-14" height="14px" src="../../imagens/layout/ico-orkut.png"/>Orkut</a></p>
                        
                    </div>                                
                </div>
                </div>                
                
<!--                <div id="box-links">
                    <div id="boxlink">
                        <div class="titulo3">
                            Comentários dos clientes
                        </div>
                        <div id="texto">
                            A melhor maneira de nos conhecer é sabendo o que os nossos clientes estão dizendo.
                        </div>
                    </div>                
                    <div id="boxlink">
                        <div class="titulo3">
                            Blog
                        </div>
                        <div id="texto">
                            Viagens, moda, decoração, matérias interessantes, curiosidades e não podemos de esquecer da comida!
                        </div>                                
                    </div>
                    <div id="boxlink">
                        <div class="titulo3">
                            Visite-nos também:
                        </div>
                        <div id="texto">
                            Facebook<br>Youtube<br>Orkut
                        </div>                                
                    </div>
                </div>-->
            </div>
            
            
            <?php include("../../includes/msg_rodape.php"); ?>            
            
        </div>
    	<div id="boxbottom"></div>
		<?php
            include("../../includes/rodape.php");
        ?>
    </div>

</body>
</html>
