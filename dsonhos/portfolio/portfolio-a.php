<?php
//include_once '../../includes/manutencao.php';
include_once '../../painel/lib/conection.php';

$pastafotos = 'fotos/';

//Possiveis erros
$e_sql      = false; //Erro de SQL 
$e_noalb    = false; //Nenhum Album criado
$e_albnot   = true;  //Album não encontrado
$error      = false; //Qualquer erro

$port_inicio= true;

//GET
if(isset($_GET['album'])){
    $album = $_GET['album'];
    $port_inicio= false;
}else{
//    $album = false;
    
    $port_inicio = false;
    $album = '2'; //Casamento
}
//Buscar Albuns para MENU
$sql = 'SELECT  id, idpai, texto1, texto2, foto
          FROM  albuns
         WHERE  deletado = 0 
		   AND  oculto = 0
      ORDER BY  ordem';

$res_alb = mysql_query($sql);
if($res_alb){
    if(mysql_num_rows($res_alb)==0){
        $e_noalb = true;
    }
}else{
    $e_sql = true;
    $error = true;
}
//Se não tem erro de sql e possui algum album
if( !$e_sql && !$e_noalb){
    //Informações do Album e MENU-Albuns
    $menu_li = '';
    $alb_idpai = 0;
    $cont = 0; $res_row = 0;
    while($row = mysql_fetch_array($res_alb)){
        $id = $row['id'];
        $idpai = $row['idpai'];
        $texto1 = utf8_decode($row['texto1']);
        $texto2 = utf8_decode($row['texto2']);
        
        //Se Album-pai entra no menu.
        if($idpai == 0){
            $menu_li .= '<a href="portfolio.php?album='.$id.'"><div class="alink">'.$texto1.'</div></a>';        
        }
        
        //Se Album a exibir: guarda informações.
        if($id == $album){
            //Album existe
            $e_albnot = false;
            
            //Guarda infos do album a exibir
            $alb_texto1 = $texto1;
            $alb_texto2 = $texto2;
            
            //Dados para pegar info do album pai.
            $albpai_id  = $idpai;  //id pai
            $res_rowpai = $cont;   //pos do $res
        }
        $cont++;
    }//while

    //Buscar Fotos do Album
    if(!$e_albnot){
        
        //Seleciona as fotos
        $sql = "SELECT  name 
                  FROM  fotos
                 WHERE  deletado = 0 
                   AND  idalbum = $album 
              ORDER BY  ordem";

        $res_fts = mysql_query($sql);          
        
        //Guardar infos do Album-PAI
        if( $alb_idpai != 0){
            mysql_data_seek($res_alb,$res_rowpai);
            $row = mysql_fetch_array($res_alb);    
            $albpai_texto1 = utf8_decode($row['texto1']);
            $albpai_texto2 = utf8_decode($row['texto2']);
        }                        
    }
     
}//if
                          

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="StyleSheet" type="text/css" href=" ../../css/estiloHome.css">
    <link rel="StyleSheet" type="text/css" href="./css/estiloPortfolio.css">
    <link rel="stylesheet" type="text/css" href="css/screen.css" media="screen" />
    
    <link rel="SHORTCUT ICON" href="../../imagens/outras/favicon.png" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="css/lightbox.css" media="screen" />
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/lightbox-2.6.min.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.js"></script>

    <script>
        function click() {
            if (event.button==2||event.button==3) {
                alert("Cópia Proibida! Conteúdo exclusivo D'Sonhos.");
                oncontextmenu='return false';
            }
        }
        document.onmousedown=click;
        document.oncontextmenu = new Function("return false;");                
        
    </script>
 
    <title>: : D'Sonhos : :</title>    	

</head>
<body>
    <?php include_once("../../includes/analysticsgoogle.php") ?>

    <div id="corpo">
       
        <div id="boxtop"></div>
        <div id="boxcenter">
            
            <?php $layout = "menor"; include("../../includes/topo.php"); ?>           
                        
            <div id="conteudo">   
<?php
if(!$error){
    if(!$e_noalb){
?>                                                                
                <div id="esquerda">   
                    <div class="menu">
                        <!--<ul >-->
                          <div id="titMenu">Portfólio</div>                                    
                          <?php echo $menu_li; ?>
                          <!--<li id="tituloMenuLateral2"></li>-->
                        <!--</ul>-->
                    </div>                    
                </div>            
<?php
        if(!$port_inicio){
            if(!$e_albnot){
?>
                <div id="direita">
                    <div id="albuns">                        
                        <div id="alb-titulo"><?php echo $alb_texto1 ?><div class="small"><?php echo $alb_texto2 ?></div></div>
                        <div class="albsubalbs">                            
    <?php
        //Exibir Sub-Albuns
        mysql_data_seek($res_alb,0);
        while($row = mysql_fetch_array($res_alb)){
            $idpai = $row['idpai'];
            
            //Exibir os sub-albuns deste álbum
            if($idpai != $album) {continue;} 
            
            $id = $row['id'];
            $texto1 = utf8_decode($row['texto1']);
            $texto2 = utf8_decode($row['texto2']);
            $foto = $row['foto']=="0"? "img/t_nofoto.png":$pastafotos.$id."/".$row['foto'];
    ?>                         
                        <a href="portfolio.php?album=<?php echo $id; ?>" class="linksubalbum">
                            <div class="itemalbum">
                                    <div class="alb-img">
                                        <img src="<?php echo $foto; ?>"/>
                                    </div>
                                    <div class="alb-titulos">
                                        <div class="alb-titulo1">
                                            <?php echo $texto1;?>
                                        </div>
                                        <div class="alb-titulo2">
                                            <?php echo $texto2;?>
                                        </div>
                                    </div>
                            </div>                            
                        </a>               
    <?php
        }//while
    ?>             
                            
                        <div class="clear"></div>
                        </div>
                        <div class="albfotos">                            
    <?php
        while($l = mysql_fetch_array($res_fts)){
            $name = $l['name'];
    ?>                         
                            <div class="box">
                                <div class="boximg">
                                    <a href="<?php echo $pastafotos,$album,"/",$name; ?>" rel="lightbox[]" data-lightbox="portfolio" title="<?php echo $alb_texto1 ?>">
                                        <div class="ftoimg" style="background: url('<?php echo $pastafotos,$album,"/",'t_',$name ?>') center center no-repeat;"></div>
                                    </a>
                                </div>
                            </div>
    <?php
        }               
    ?>                                                 
                        </div>
                    </div>                                       
                </div>                                       
                                
<?php
            }else{
                //Album Não Encontrado
?>
                    <div class="error"><b>Álbum não encontrado.</b> Possivelmente este álbum não existe ou foi removido. <span style="float:right;"><a href="portfolio.php">Ir para o Portfólio</a></span></div>                                   
<?php
            }
        }else{
            // Portfolio INICIO
?>                                                                
                    <div id="albuns">
<?php
            mysql_data_seek($res_alb,0);
            while($row = mysql_fetch_array($res_alb)){        
                if($row['idpai']!=0){ continue;} //Mostrar apenas álbuns PAI
                $id = $row['id'];
                $texto1 = utf8_decode($row['texto1']);
                $texto2 = utf8_decode($row['texto2']);
                $foto = $row['foto']=="0"? "img/t_nofoto.png":$pastafotos.$id."/".$row['foto'];
?>                                                                
                        <a href="portfolio.php?album=<?php echo $id; ?>" class="linksubalbum">
                        <div class="itemalbum">
                                <div class="alb-img">
                                    <img src="<?php echo $foto; ?>" />
                                </div>
                                <div class="alb-titulos">
                                    <div class="alb-titulo1">
                                        <?php echo $texto1;?>
                                    </div>
                                    <div class="alb-titulo2">
                                        <?php echo $texto2;?>
                                    </div>
                                </div>
                        </div>
                        </a>
<?php
            }//while
?>                                                                
                    </div>                                                       
<?php
        }
        
    }else{
        //Nenhum Álbum criado ainda.
?>
                    <div class="error"><b>Em Breve.</b></div>                                   
<?php
    }
}else{
//Erro sql
?>
                    <div class="error"><b>Houve um error e o álbum não pode ser exibido no momento.</b> Tente novamante mais tarde.</div>                                                
<?php
}//if error
?>
                    
                    
                </div>                                                          
            <?php include("../../includes/msg_rodape.php"); ?>                        
            </div>            

    	<div id="boxbottom"></div>
        <?php include("../../includes/rodape.php"); ?>
    </div>

</body>    

    <script>
        $('.linksubalbum').hover(
            function(){
                $(this).find('.alb-img').css("box-shadow","0px 0px 3px #444");
          },function(){
                $(this).find('.alb-img').css("box-shadow","0px 0px 3px #9A9A9A");       
        });
        
//        $('.itemalbum').hover(function(){
//            $(this).animate({"background-color":"red"},1000);
////            $(this).hide("slow");
//
//        },function(){
//            $(this).animate({"background-color":"green"},1000);
////            $(this).animate({background-color:"black"},1500);            
//        });
            
        
    </script>
  