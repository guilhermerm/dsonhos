<?php
include_once '../../includes/manutencao.php';
include_once '../../painel/lib/conection.php';

$pastafotos = 'fotos/';

//Possiveis erros
$e_sql           = false; //Erro de SQL 
$e_zeroalb       = false; //Nenhum Album criado
$e_albnotfound   = false; //Album não encontrado
$e_zeroalbsubs   = false; //Nenhum Album criado
$e_zeroftos      = false; //Nenhuma foto inserida
$e_zerovid       = false; //Nenhum Video inserido
$e_vidnotfound   = false; //Video não encontrado
$error           = false; //Qualquer erro

//Variaveis de controle
$isinicio  = true;
$isalbum   = false;
$isvideo   = false;

$video = false;

// GET'S
if(isset($_GET['album'])){
    $isinicio = false;
    $album = $_GET['album'];
    
    if($album == 'videos'){
        $isvideo = true;
        if(isset($_GET['video'])){
            $video = $_GET['video'];
        }else{
            $video = '0';
        }        
    }else{
        $isalbum = true;        
    }
}else{
//    $album = false;
    $isinicio = false;
    $isalbum = true;        
    $album = '2'; //Casamento
}


//- - - - - - -  Montar Albuns para o MENU - - - - - - - 

    $andidpai = " AND  idpai = 0 ";
    $sql = "SELECT  id, texto1, texto2, link, foto
              FROM  albuns
             WHERE  deletado = 0 
               AND  oculto = 0
                    $andidpai
          ORDER BY  ordem";
    $res_albmenu = mysql_query($sql);
    if($res_albmenu){
        if(mysql_num_rows($res_albmenu)>0){
            $menu_alb = '';
            while($r1 = mysql_fetch_array($res_albmenu)){
                $id     = $r1['id'];
                $texto1 = utf8_decode($r1['texto1']);
                $link = $r1['link'];
//                $menu_alb .= '<a href="portfolio.php?album='.$id.'"><div class="alink">'.$texto1.'</div></a>';        
                $menu_alb .= "<a href='portfolio.php?album=$id&s=$link'><div class='alink'>$texto1</div></a>";        
            }
            $menu_alb .= '<a href="portfolio.php?album=videos"><div class="alink">Vídeos</div></a>';    
        }else{
            //Nenhum Álbum criado
            $e_zeroalb = true;
            $error = true;
        }
    }else{
        //Erro no SQL
        $e_sql = true;
        $error = true;
    }

// - - - - SE ALBUM BUSCAR FOTOS E SUB ALBUNS - - - 
    if(!$error && $isalbum){
        
        //Info do ALBUM
        $sql2 = "SELECT  id, idpai, texto1, texto2, link, foto
                  FROM  albuns
                 WHERE  deletado = 0 
                   AND  oculto = 0
                   AND  id = $album
              ORDER BY  ordem";
        $res_alb = mysql_query($sql2);
        if($res_alb){
            if(mysql_num_rows($res_alb)>0){
                $r2 = mysql_fetch_array($res_alb);
                    $alb_id     = $r2['id'];
                    $alb_idpai  = $r2['idpai'];
                    $alb_texto1 = utf8_decode($r2['texto1']);
                    $alb_link   = $r2['link'];
                    $alb_texto2 = utf8_decode($r2['texto2']);                    
                    
                    // AQUI BUSCARIA DADOS DO PAI
                    
                    //BUSCAR SUB-ALBUNS
                    $sql3 = "SELECT  id, texto1, texto2, link, foto
                               FROM  albuns
                              WHERE  deletado = 0 
                                AND  oculto = 0
                                AND  idpai = $album
                           ORDER BY  ordem";
                    $res_albsubs = mysql_query($sql3);                    
                    if($res_albsubs){
                        if(mysql_num_rows($res_albsubs)==0){                            
                            $e_zeroalbsubs = true;
                        }
                    }else{
                        $e_sql = true;
                        $error = true;
                    }
                    
                    //BUSCAR FOTOS
                    $sql4 = "SELECT  name
                               FROM  fotos
                              WHERE  deletado = 0 
                                AND  idalbum = $album
                           ORDER BY  ordem";
                    $res_albftos = mysql_query($sql4);                           
                    if($res_albftos){
                        if(mysql_num_rows($res_albftos)==0){                            
                            $e_zeroftos = true;
                        }
                    }else{
                        $e_sql = true;
                        $error = true;
                    }
                    
            }else{
                $e_albnotfound = true;
                $error = true;
            }
        }else{
            $e_sql = true;
        }
  
    }//if(isalbum)

// - - - - SE VIDEOS BUSCAR OS VIDEOS - - - - 
    if(!$error && $isvideo){
        $sql5 = 'SELECT  id, texto1, texto2, link, watchv, texto3
                  FROM  videosyt
                 WHERE  deletado = 0 
              ORDER BY  ordem';
        $res_vid = mysql_query($sql5);       
        $alb_texto1 = "Vídeos";
        $alb_texto2 = "";
        $alb_link = "videos";
    }//if(isvideo)    

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="StyleSheet" type="text/css" href=" ../../css/estiloHome.css">
    <link rel="StyleSheet" type="text/css" href="./css/estiloPortfolio.css">
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    
    <link rel="SHORTCUT ICON" href="../../imagens/outras/favicon.png" type="image/x-icon" />
    <?php if($isalbum){ ?>
    <link rel="stylesheet" type="text/css" href="css/lightbox.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/screen.css" media="screen" />
    <script type="text/javascript" src="js/lightbox-2.6.min.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.js"></script>
    <?php } ?>
    
    <?php if($isvideo){ ?>
    <script type="text/javascript" src="js/videobox/js/mootools.js"></script>
    <script type="text/javascript" src="js/videobox/js/swfobject.js"></script>
    <script type="text/javascript" src="js/videobox/js/videobox.js"></script>
    <link rel="stylesheet" type="text/css" href="js/videobox/css/videobox.css" media="screen" />
    <?php } ?>
    

    <script>
        function click() {
            if (event.button==2||event.button==3) {
                alert("Cópia Proibida! Conteúdo exclusivo D'Sonhos.");
                oncontextmenu='return false';
            }
        }
        document.onmousedown=click;
        document.oncontextmenu = new Function("return false;");                
        
    </script>
 
    <title>: : D'Sonhos : :</title>    	

</head>
<body>
    <?php include_once("../../includes/analysticsgoogle.php") ?>

    <div id="corpo">
       
        <div id="boxtop"></div>
        <div id="boxcenter">
            
            <?php $layout = "menor"; include("../../includes/topo.php"); ?>           
                        
            <div id="conteudo">   
<?php
    if(!$error){
        if($isinicio){
            if(!$e_zeroalb){
            // Portfolio INICIO            
    ?>
                    <div id="albuns">                                                             
    <?php
            mysql_data_seek($res_albmenu,0);
            while($row = mysql_fetch_array($res_albmenu)){                      
                $id = $row['id'];
                $texto1 = utf8_decode($row['texto1']);
                $link = $row['link'];
                $texto2 = utf8_decode($row['texto2']);
                $foto = $row['foto']=="0"? "img/t_nofoto.png":$pastafotos.$id."/".$row['foto'];            
    ?>
                        <a href="portfolio.php?album=<?php echo $id,"&s=",$link; ?>" class="linksubalbum">
                        <div class="itemalbum">
                                <div class="alb-img">
                                    <img src="<?php echo $foto; ?>" />
                                </div>
                                <div class="alb-titulos">
                                    <div class="alb-titulo1">
                                        <?php echo $texto1;?>
                                    </div>
                                    <div class="alb-titulo2">
                                        <?php echo $texto2;?>
                                    </div>
                                </div>
                        </div>
                        </a>
    <?php
            }//while
    ?>
                    </div>    
                
    <?php
            }else{
        //Nenhum Álbum criado ainda.
    ?>                                                
                    <div class="error"><b>Em Breve.</b></div>                                   
    <?php
            }
        //if(isinicio)
        }
?>

                
                
<?php
        if($isalbum){
            if(!$e_zeroalb){
            //MENU
    ?>
                <div id="esquerda">   
                    <div class="menu">
                          <div id="titMenu">Portfólio</div>                                    
                          <?php echo $menu_alb; ?>
                    </div>                    
                </div>            
                
                <div id="direita">                
                
                    <div id="albuns">
                        <?php if($alb_idpai!="0"){ ?>
                        <div class="albvoltar"><a href="portfolio.php?album=<?php echo $alb_idpai,"&s=",$alb_link ?>"><b>&larr;</b> Voltar</a></div>
                        <?php } ?>
                        <div id="alb-titulo"><?php echo $alb_texto1 ?><div class="small"><?php echo $alb_texto2 ?></div></div>
    <?php
                if(!$e_albnotfound){
                
                //Exibir Sub-Albuns
                    if(!$e_zeroalbsubs){
    ?>                        
                        <div class="albsubalbs">                                                   
        <?php 
                        while($r2 = mysql_fetch_array($res_albsubs)){
                            $sub_id = $r2['id'];
                            $sub_texto1 = utf8_decode($r2['texto1']);
                            $sub_link = $r2['link'];
                            $sub_texto2 = utf8_decode($r2['texto2']);
                            $sub_foto = $r2['foto']=="0"? "img/t_nofoto.png":$pastafotos.$sub_id."/".$r2['foto'];            
        ?>
                            <a href="portfolio.php?album=<?php echo $sub_id,"&s=",$sub_link; ?>" class="linksubalbum">
                                <div class="itemalbum">
                                        <div class="alb-img">
                                            <img src="<?php echo $sub_foto; ?>"/>
                                        </div>
                                        <div class="alb-titulos">
                                            <div class="alb-titulo1">
                                                <?php echo $sub_texto1;?>
                                            </div>
                                            <div class="alb-titulo2">
                                                <?php echo $sub_texto2;?>
                                            </div>
                                        </div>
                                </div>                            
                            </a>                                                      
        <?php 
                    }//while $res_albsubs
        ?>
                            <div class="clear"></div>
                        </div>
        <?php 
                    }//if(zerosubalbuns)
                
                    //FOTOS DO ALBUM
                    if(!$e_zeroftos){
        ?>
                        <div class="albfotos">                            
        <?php 
                        while($r3 = mysql_fetch_array($res_albftos)){
                             $name = $r3['name'];
        ?>
                            <div class="box">
                                <div class="boximg">
                                    <a href="<?php echo $pastafotos,$alb_id,"/",$name; ?>" rel="lightbox[]" data-lightbox="portfolio" title="<?php echo $alb_texto1 ?>">
                                        <div class="ftoimg" style="background: url('<?php echo $pastafotos,$alb_id,"/",'t_',$name ?>') center center no-repeat;"></div>
                                    </a>
                                </div>
                            </div>
        <?php
                        }//while $res_ftos                      
        ?>
                        </div>
        <?php
                    }//if(zeroftos)
                
                    if($e_zeroftos && $e_zeroalbsubs){
        ?>                
                        <div class="error"><b>Sem fotos.</b></div> 
        <?php
                    }
                }else{
                    //Album Não Encontrado
        ?>
                        <div class="error"><b>Álbum não encontrado.</b> Este álbum não existe ou foi removido. <span style="float:right;"><a href="portfolio.php">Ir para o Portfólio</a></span></div>                                
    <?php
                }
            }else{
            //Nenhum Álbum criado ainda.
    ?>                                                
                    <div class="error"><b>Em Breve.</b></div>                                   
    <?php
            }
    ?>               
                    </div> <!-- ALBUNS -->
                </div><!-- DIREITA -->                    
    <?php
        //if(isvideo)
        }
?>               
        
            
<?php            
        if($isvideo){
    ?>                   
                    <div id="esquerda">   
                        <div class="menu">
                              <div id="titMenu">Portfólio</div>                                    
                              <?php echo $menu_alb; ?>
                        </div>                    
                    </div>            

                    <div id="direita">        
                        <div class="albfotos"> 
                            <div id="alb-titulo"><?php echo $alb_texto1 ?><div class="small"><?php echo $alb_texto2 ?></div></div>                            
    <?php
            if(!$e_zerovid){
                        //VIDEOS
                while($r4 = mysql_fetch_array($res_vid)){
                    $vid_id = $r4['id'];
                    $vid_texto1 = utf8_decode($r4['texto1']);
                    $vid_link = $r4['link'];
                    $vid_texto2 = utf8_decode($r4['texto2']);
                    $vid_watchv = utf8_decode($r4['watchv']);   
    ?>                   
                            <!--<a href="portfolio.php?album=videos&video=<?php echo $vid_id; ?>" class="linksubalbum">-->                            
                            <a href="<?php echo $vid_texto2,"&s=",$vid_link; ?>" class="linksubalbum" rel="vidbox 640 480" title="<?php echo $vid_texto1; ?>">
                                <div class="itemalbum">
                                        <div class="alb-img">
                                            <img src="https://i1.ytimg.com/vi/<?php echo $vid_watchv ?>/default.jpg"/>
                                        </div>
                                        <div class="alb-titulos">
                                            <div class="alb-titulo1">
                                                <?php echo $vid_texto1;?>
                                            </div>
    <!--                                        <div class="alb-titulo2">
                                                <?php echo $vid_texto2;?>
                                            </div>-->
                                        </div>
                                </div>                            
                            </a>                                                      
    <?php
                }//while
            }else{
                //SEM VIDEOS
    ?>                   
                            <div class="error"><b>Sem Videos.</b></div> 
    <?php
            }
    ?>                   
                        </div>
                    <!--</div>  ALBUNS -->
                </div><!-- DIREITA -->                    
    <?php
        //if(isvideo)
        }
?>                        

<?php
    //if(error)
    }else{
    //Erro sql
?>                            
                    <div class="error"><b>Houve um error e o álbum não pode ser exibido no momento.</b> Tente novamante mais tarde.</div>
<?php
    }
?> 
                </div>                                                          
            <?php include("../../includes/msg_rodape.php"); ?>                        
            </div>            

    	<div id="boxbottom"></div>
        <?php include("../../includes/rodape.php"); ?>
    </div>

</body>    
    <script>
        var $jQ = jQuery.noConflict();
        $jQ('.linksubalbum').hover(
            function(){
                $jQ(this).find('.alb-img').css("box-shadow","0px 0px 6px #333");
          },function(){
                $jQ(this).find('.alb-img').css("box-shadow","0px 0px 4px #9A9A9A");       
        });   
    </script>
  