<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="StyleSheet" type="text/css" href=" ../../css/estiloHome.css"/>
    <link rel="StyleSheet" type="text/css" href="./css/estiloPublicidade.css"/>
    <link rel="SHORTCUT ICON" href="../../imagens/outras/favicon.png" type="image/x-icon" />
    <title>: : D'Sonhos : :</title>

    <link rel="stylesheet" type="text/css" href="../style-projects-jquery.css" />

    <!-- Arquivos utilizados pelo jQuery lightBox plugin -->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.lightbox-0.5.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
    <!-- / fim dos arquivos utilizados pelo jQuery lightBox plugin -->

    <script>
        function click() {
            if (event.button==2||event.button==3) {
                alert("Cópia Proibida! Conteúdo exclusivo D'Sonhos.");
                oncontextmenu='return false';
            }
        }
        document.onmousedown=click;
        document.oncontextmenu = new Function("return false;");

    </script>

     <!-- Ativando o jQuery lightBox plugin -->
    <script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox();
    });
    </script>
   	<style type="text/css">
	/* jQuery lightBox plugin - Gallery style */
	#gallery {
/*		background-color: #444;
		padding: 10px;
		width: 520px;
        text-align: left;*/
	}
	#gallery ul { list-style: none; }
	#gallery ul li { display: inline; }
	#gallery ul img {
/*		border: 5px solid #3e3e3e;*/
		border-width: 15px 15px 30px;
                margin: 15px;
	}
	#gallery ul a:hover img {
/*		border: 5px solid #fff;*/
		border-width: 15px 15px 30px;
		/*color: #fff;*/
	}
	#gallery ul a:hover { color: #fff;
        }
	</style>
</head>
    
<body onload="popup()">
    <?php include_once("../../includes/analysticsgoogle.php") ?>

    <div id="corpo">    
        <div id="bgraios"></div>
        <div id="logo">
            <a href="../inicio/inicio.php">
                <img src="../../imagens/layout/logo-dsonhos.png"/>                
            </a>
        </div>
       
        <div id="boxtop"></div>
        <div id="boxcenter">
            
            <div id="menu-maior">
                <?php include("../../includes/menu.php"); ?>
            </div>                       
            
            <div id="conteudo">                    
                
                <div id="gallery">
                    <div id="titleft">
                        <p class="titulo1">Mídia Impressa</p> 
                    </div>                       
                    <div id="imgs">
                        <a href="photos/impresso12.jpg"><img id="imp" src="photos/impresso12_t.jpg"></img></a>
                        <a href="photos/impresso10.jpg"><img id="imp" src="photos/impresso10_t.jpg"></img></a>
                        <a href="photos/impresso11.jpg"><img id="imp" src="photos/impresso11_t.jpg"></img></a>
                        <a href="photos/impresso09.jpg"><img id="imp" src="photos/impresso09_t.jpg"></img></a>
                        <br>										  
                        <a href="photos/impresso07.jpg"><img id="imp" src="photos/impresso07_t.jpg"></img></a>
                        <a href="photos/impresso08.jpg"><img id="imp" src="photos/impresso08_t.jpg"></img></a>
                        <a href="photos/impresso05.jpg"><img id="imp" src="photos/impresso05_t.jpg"></img></a>
                        <a href="photos/impresso06.jpg"><img id="imp" src="photos/impresso06_t.jpg"></img></a>
                        <br>
                        <a href="photos/impresso01.jpg"><img id="imp" src="photos/impresso01_t.jpg"></img></a>
                        <a href="photos/impresso04.jpg"><img id="imp" src="photos/impresso04_t.jpg"></img></a>
                        <a href="photos/impresso02.jpg"><img id="imp" src="photos/impresso02_t.jpg"></img></a>
                        <a href="photos/impresso03.jpg"><img id="imp" src="photos/impresso03_t.jpg"></img></a>
                        <br>                        
                        (Clique nas imagens para ampliar)
                        <br>                        
                        <br>                        
                    </div>
                </div>

                <div id="box">
                    <p class="titulo1">Cursos</p>

                    <img id="img-quebradelinha" src="./images/quebra-linha-dsonhos-felipao.png" />
                    <br>

                    Entre em contato com o Centro Técnico Felipão.
                    <br>
                            <a href="http://www.felipao-es.com.br">www.felipao-es.com.br</a>
                    <br>
                    <br>

                </div>
            
            <?php include("../../includes/msg_rodape.php"); ?>                    
            
            
            </div>
        </div>
    	<div id="boxbottom"></div>
		<?php
                    include("../../includes/rodape.php");
                ?>
    </div>

</body>    
</html>
