<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="StyleSheet" type="text/css" href=" ../../css/estiloHome.css"/>
    <link rel="StyleSheet" type="text/css" href="./css/estiloParceiros.css"/>
    <link rel="SHORTCUT ICON" href="../../imagens/outras/favicon.png" type="image/x-icon" />
    <title>: : D'Sonhos : :</title>

    <link rel="stylesheet" type="text/css" href="../style-projects-jquery.css" />

    <!-- Arquivos utilizados pelo jQuery lightBox plugin -->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.lightbox-0.5.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
    <!-- / fim dos arquivos utilizados pelo jQuery lightBox plugin -->

    <script>
        function click() {
            if (event.button==2||event.button==3) {
                alert("Cópia Proibida! Conteúdo exclusivo D'Sonhos.");
                oncontextmenu='return false';
            }
        }
        document.onmousedown=click;
        document.oncontextmenu = new Function("return false;");

    </script>

     <!-- Ativando o jQuery lightBox plugin -->
    <script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox();
    });
    </script>
   	<style type="text/css">
	/* jQuery lightBox plugin - Gallery style */
	#gallery {
/*		background-color: #444;*/
		padding: 0px;
		width: 300px;
        /*text-align: left;*/
	}
	#gallery ul { list-style: none; }
	#gallery ul li { display: inline; }
	#gallery ul img {
/*		border: 5px solid #3e3e3e;*/
		border-width: 15px 15px 30px;
                margin: 15px;
	}
	#gallery ul a:hover img {
/*		border: 5px solid #fff;*/
		border-width: 15px 15px 30px;
		/*color: #fff;*/
	}
	#gallery ul a:hover { color: #fff;
        }
	</style>


</head>
<body>
	<?php include_once("../../includes/analysticsgoogle.php") ?>
    <div id="bg2">

    </div>

    <div id="corpo">
    	<div id="bgraios"> </div>
    	<div id="logo"> </div>
        <div id="boxtop"> </div>

        <div id="boxcenter">
				<?php
					include("../../includes/menu.php");
				?>
                <div id="contexto">
                    <table>
                        <tr>
                            <td id="contexto0" colspan="2">
                                <p id="titulo2" > Parceiros</p>
                            </td>
                        </tr>
                        <tr>
                            <td id="contexto1">
                                <!--vazio-->								
                            </td>
                            <td id="contexto2">
                                 


                                    <div id="sombra">
                                        <div id="box">
                                            <table>
                                                <tr>
                                                    <td id="texto2">

                                                        <b>Acimar Gassoli</b>
                                                        <br>
                                                            <b>Tel.: </b>(27) 3386-3419
                                                        <br>
                                                            <a href="http://www.agnoivas.blogspot.com">www.agnoivas.blogspot.com</a>

                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <br>
									
                                    <div id="sombra">
                                        <div id="box">
                                            <table>
                                                <tr>
                                                    <td id="texto2">                                                                                                               
											
                                                        <b>Ana Lú Decoração</b>
                                                        <br>
                                                            <b>Ana Lú</b>  3281-3146 / 9954-0979
                                                        <br>
                                                            <h5>Decorações de Eventos Sociais e Corporativos.</h5> 
				     					<a href="http://www.analudecoracoes.com.br">www.analudecoracoes.com.br</a>

														
														
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
				    <br>

				    <div id="sombra">
                                        <div id="box">
                                            <table>
                                                <tr>
                                                    <td id="texto2">                                                                                                               
													
                                                        <b>Bull Grill Buffet Externo </b>
                                                        <br>
                                                            <b>Gislaine e Wesley </b> 3299-9279 / 8133 0699
                                                        <br>
                                                            <h5>Serviço de Buffet completo para eventos sociais e corporativos, coquetéis, churrasco, coffee break, almoço, jantar entre outros.</h5>                                                        
                                                            <a href="http://www.bullgrill.com.br">www.bullgrill.com.br </a>
															
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
				   <br>

                                    <div id="sombra">
                                        <div id="box">
                                            <table>
                                                <tr>
                                                    <td id="texto2">

                                                        <b>De Prá Sonorização</b>
                                                        <br>
                                                            <b>Tel.: </b>(27) 9274-2000
                                                         <br>
                                                            <a href="http://www.depra.com.br">www.depra.com.br</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <br>	

				    <div id="sombra">
                                        <div id="box">
                                            <table>
                                                <tr>
                                                    <td id="texto2">                                                                                                               

                                                        <b>Emporio Life - Multimarcas </b>
                                                        <br>
                                                            <b>Dani e Marcelo</b> (27) 3227-8069 
                                                        <br>
                                                            <h5>R: Joaquim Lírio, 189 – Loja 08  -Shopping Plaza Praia <br> Praia do Canto – Vitória/ES</h5>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
				<br>

                                    <div id="sombra">
                                        <div id="box">
                                            <table>
                                                <tr>
                                                    <td id="texto2">                                                                                                               

                                                        <b>Felipão</b>                                                            
                                                        <br>
                                                            <h5>Cursos de confeitaria, bombons, doces, tortas, salgados. Loja com produtos para confeitaria, panificação, etc.</h5>                                                        
															<a href="http://www.felipao-es.com.br">www.felipao-es.com.br</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>								
				<br>

                                    <div id="sombra">
                                        <div id="box">
                                            <table>
                                                <tr>
                                                    <td id="texto2">

                                                        <b>Indústria do Software</b>
                                                        <br>
                                                            <b>Site</b> <a href="http://www.industriadosoftware.com.br">www.industriadosoftware.com.br</a>
                                                        <br>
                                                            <h5>Desenvolvimento de Websites</h5>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <br>

				  <div id="sombra">
                                        <div id="box">
                                            <table>
                                                <tr>
                                                    <td id="texto2">                                                                                                               
                                                        <b>Opem Bar Classic</b>
                                                        <br>
                                                            <b>Elaílson</b> 3233-6981 / 9997-0858
                                                        <br>
                                                            <h5>Formado por uma equipe de jovens treinados na preparação de coquetéis e drinks variados</h5>															<a href="http://classicopenbar.webnode.com.br/">http://classicopenbar.webnode.com.br/</a>
															
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>									
				    <br>
                           </td>
                        </tr>
                        <tr >                        
                            <td colspan="2" id="contexto3">Bolos artísticos, temáticos e maquetes, para aniversários e casamentos. Doces, bombons finos <br> (27) 3084-9511 / (27) 9921-8795. Selma e Thiago - dsonhos@gmail.com</td>
                        </tr>

                    </table>
                </div>
        </div>

    	<div id="boxbottom">

        </div>
		<?php
            include("../../includes/rodape.php");
        ?>
    </div>

</body>
</html>
