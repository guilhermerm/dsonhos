<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="StyleSheet" type="text/css" href=" ../../css/estiloHome.css"/>
    <link rel="StyleSheet" type="text/css" href="./css/estiloParceiros.css"/>
    <link rel="SHORTCUT ICON" href="../../imagens/outras/favicon.png" type="image/x-icon" />
    <title>: : D'Sonhos : :</title>

    <link rel="stylesheet" type="text/css" href="../style-projects-jquery.css" />

    <!-- Arquivos utilizados pelo jQuery lightBox plugin -->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.lightbox-0.5.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
    <!-- / fim dos arquivos utilizados pelo jQuery lightBox plugin -->

    <script>
        function click() {
            if (event.button==2||event.button==3) {
                alert("Cópia Proibida! Conteúdo exclusivo D'Sonhos.");
                oncontextmenu='return false';
            }
        }
        document.onmousedown=click;
        document.oncontextmenu = new Function("return false;");

    </script>

     <!-- Ativando o jQuery lightBox plugin -->
    <script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox();
    });
    </script>
   	<style type="text/css">
	/* jQuery lightBox plugin - Gallery style */
	#gallery {
/*		background-color: #444;*/
		padding: 0px;
		width: 300px;
        /*text-align: left;*/
	}
	#gallery ul { list-style: none; }
	#gallery ul li { display: inline; }
	#gallery ul img {
/*		border: 5px solid #3e3e3e;*/
		border-width: 15px 15px 30px;
                margin: 15px;
	}
	#gallery ul a:hover img {
/*		border: 5px solid #fff;*/
		border-width: 15px 15px 30px;
		/*color: #fff;*/
	}
	#gallery ul a:hover { color: #fff;
        }
	</style>


</head>
<body>
	<?php include_once("../../includes/analysticsgoogle.php") ?>
    <div id="bg2">

    </div>

    <div id="corpo">
    	<div id="bgraios"> </div>
    	<div id="logo"> </div>
        <div id="boxtop"> </div>

        <div id="boxcenter">
				<?php
					include("../../includes/menu.php");
				?>
                <div id="contexto">
                                                               
                    <table>
                        <tr>
                            <td id="contexto0" colspan="2">
                                <p id="titulo2" > Depoimentos</p>
                                <p style="font-size:75%; font-weight:normal;"><a href="../contato/contato.php?d=depoimento">Envie-nos um depoimento.</a></p>
                            </td>
                        </tr>
                        <tr>
                            <td id="contexto1">
                                <!--vazio-->								
                            </td>
                            
                            <td id="contexto2">
                                <div id="gallery"> 
                                    <div id="sombra"><div id="box"><table>
                                    <tr>
                                            <td id="texto2">												
                                                
                                                <a href="imgdepoimentos/imgd11.jpg"><img id="depoimento-img" src="imgdepoimentos/imgd11_t.jpg"></img></a>
                                                  <img id="abre-aspas" src="imgdepoimentos/abre-aspas.png">
                                                  <div id="depoimento-texto">                                                                                                                               
                                                    <b>Nolimar e Marcelo</b>
                                                    <br>
															Selma e Thiago,
													<br>	Deus nos deu oportunidade de conhecer duas pessoas maravilhosas, que foram vocês, obrigada do fundo do nossos corações por realizar o nosso sonho, pelo carinho e atenção que vocês nos receberam desde o primero dia que a gente se conheceu, sem contar o amor, a dedicação que tiveram para fazer nosso BOLO.
													<br>	Cada vez que a gente ia comentar como seria o bolo os olhinhos de vocês e os nossos, é claro, brilhavam com a nossa ideia maluca de fazer um bolo de roda de boteco para o nosso casamento.
													<br>	Na festa, quando eu vi aquele bolo do jeitinho que tinhamos sonhado fiquei igual uma criança adimirando o presente tão esperado.
													<br>	Obrigada por tudo.
													    	O bolo estava deliciosoooooo.....
													<br>	Beijos com carinho, Nolimar e Marcelo.													
                                                    
                                                  </div>                                                    
                                            </td>
                                    </tr>
                                    </table></div></div>								
                                    <br>
                                    
                                    <div id="sombra"><div id="box"><table>
                                    <tr>
                                            <td id="texto2">												
                                                
                                                <a href="imgdepoimentos/imgd01.jpg"><img id="depoimento-img" src="imgdepoimentos/imgd01_t.jpg"></img></a>
                                                  <img id="abre-aspas" src="imgdepoimentos/abre-aspas.png">
                                                  <div id="depoimento-texto">                                                                                                                               
                                                    <b>Amanda e Victor.</b>
                                                    <br>
                                                            Olá, Selma! Nós realmente amamos a maquete. Todos da festa comentaram. Foi a maquete mais linda que já vimos. Os doces e o bolo também estavam divinos. Nós dois nos acabamos de tanto comer. Queremos agradecer muito a vc e seu marido por terem tanto carinho conosco e tanto cuidado ao enfeitarem as mesas.
                                                    <br>    Agradecemos muito por terem participado da realização deste sonho. 
                                                    <br>    A vida de casado está ótima, fomos em lua de mel para Cancún, que é lindo. E já estamos morando em SP. 
                                                    <br>    Não consideramos vocês apenas como fornecedores, mas tb como amigos.
                                                    <br>    Grande beijo para você e seu marido
                                                    <br>    Bjs 
                                                  </div>                                                    
                                            </td>
                                    </tr>
                                    </table></div></div>								
                                    <br>
                                        
                                    <div id="sombra"><div id="box"><table>
                                    <tr>
                                            <td id="texto2">												
                                                
                                                <a href="imgdepoimentos/imgd02.jpg"><img id="depoimento-img" src="imgdepoimentos/imgd02_t.jpg"></img></a>
                                                  <img id="abre-aspas" src="imgdepoimentos/abre-aspas.png">
                                                  <div id="depoimento-texto">                                                                                                                               
                                                    <b>Casamento Ana Paula e Marcos </b>
                                                    <br> <br>
                                                           Desculpe-me por não ter agradecido antes! Os doces ficaram maravilhosos e gostosos!  Ameiiiii!
                                                    <br>    Muito obrigado ! Belíssimo trabalho!

                                                    <br><br>    OBS: Eu chamei atenção da pessoa que passou o dedo no bolo achando que era de verdade! Vê se pode? Acharam que eu não ia corta aquele bolo e ficaria com ele só para mim! rsrs
                                                    <br><br>  Bjs 
                                                  </div>                                                            
                                            </td>
                                    </tr>
                                    </table></div></div>								
                                    <br>
                                        
                                    <div id="sombra"><div id="box"><table>
                                    <tr>
                                            <td id="texto2">												
                                                
                                                <a href="imgdepoimentos/imgd03.jpg"><img id="depoimento-img" src="imgdepoimentos/imgd03_t.jpg"></img></a>
                                                  <img id="abre-aspas" src="imgdepoimentos/abre-aspas.png">
                                                  <div id="depoimento-texto">                                                                                                                               
                                                    <b>Carol e Ian </b>
                                                    <br>
                                                    Queridos Selma e Thiago, 
                                                    <br>ter em minha festa de casamento, um momento tão marcante e especial na minha vida, os doces e bolos preparados por vocês, não só foi maravilhoso como ajudou para que tudo ficasse perfeito e inesquecível. Como disse se eu pudesse me casaria todos os anos, e teria a D'Sonhos como fornecedora sempre. Adoramos os doces e bolos, e só de ver as fotos fico querendo mais. Parabéns a vocês e sua equipe, desejo sucesso sempre. 
                                                    <br>Um super beijo.
                                                  </div>                                                        
                                            </td>
                                    </tr>
                                    </table></div></div>								
                                    <br>
                                        
                                    <div id="sombra"><div id="box"><table>
                                    <tr>
                                            <td id="texto2">												
                                                
                                                <a href="imgdepoimentos/imgd04.jpg"><img id="depoimento-img" src="imgdepoimentos/imgd04_t.jpg"></img></a>
                                                  <img id="abre-aspas" src="imgdepoimentos/abre-aspas.png">
                                                  <div id="depoimento-texto">                                                                             
                                                      
                                                    <b>Islany e Eduardo de Souza</b>
                                                     <br> Bom dia,

                                                     <br>   Gostaríamos de agradecer a todos pela linda festa que vocês preparam para nós... Todos ficaram muito satisfeitos, foram muito bem servidos, amaram os doces, o bolo, a fonte de chocolate então, rsrsrs, ... tudo estava perfeito!!!

                                                     <br>   Muito obrigada mesmo. Agradecemos de coração.

                                                      <br>  Que Deus continue abençoando e prosperando cada um de vocês.

                                                       <br> Um grande beijo... agora dos casados.
                                                  </div>                                                           
                                            </td>
                                    </tr>
                                    </table></div></div>								
                                    <br>
                                        
                                    <div id="sombra"><div id="box"><table>
                                    <tr>
                                            <td id="texto2">												
                                                
                                                <a href="imgdepoimentos/imgd05.jpg"><img id="depoimento-img" src="imgdepoimentos/imgd05_t.jpg"></img></a>
                                                  <img id="abre-aspas" src="imgdepoimentos/abre-aspas.png">
                                                  <div id="depoimento-texto">                                                                             
                                                      
                                                    <b>Michele Mastela</b>
                                                     <br> Olá Selma e Thiago, tudo bem.... 
                                                     <br> Olha gostaria de agradecer o carinho e o empenho de vcs na realização do meu sonho... o meu casamento foi maravilhoso e tudo estava divino... o Senhor Jesus foi muito maravilhoso comigo...tinha certeza de que iria ficar lindo, mas fui surpreendida com coisas além do que eu imaginava...realmente tive o casamento dos sonhos de qualquer uma...e tenho certeza que através do meu casamento vcs terão muito outros trabalhos, pq são maravilhosos..
                                                     <br> Um grande bju no coração.. fiquem com Deus e uma semana mais que abençoada...
                                                  </div>                                                         
                                            </td>
                                    </tr>
                                    </table></div></div>								
                                    <br>
                                        
                                    <div id="sombra"><div id="box"><table>
                                    <tr>
                                            <td id="texto2">												                                                
                                                <a href="imgdepoimentos/imgd06.jpg"><img id="depoimento-img" src="imgdepoimentos/imgd06_t.jpg"></img></a>
                                                  <img id="abre-aspas" src="imgdepoimentos/abre-aspas.png">
                                                  <div id="depoimento-texto">                                                                             
                                                      
                                                    <b>Kanik Ramos Pimentel</b>
                                                    <br> Eii Selma tudo bem?
                                                    <br>     Menina não consigo esquece o gosto daquele seu bolo e daqueles docinhos de lua de mel!  Perfeitos, Muito obrigada por tudo.
                                                    <br>     Por tanto gostaria de saber se vc faz um bolo no  recheio que foi o do meu casamento e aqueles lua de mel, só que seria para o aniversário da minha mãe, dia 10 de março mais a festinha seria dia 11 ou 12 de março!
                                                    <br>     bjinhos
                                                  </div>                                                        
                                            </td>
                                    </tr>
                                    </table></div></div>								
                                    <br>
                                        
                                    <div id="sombra"><div id="box"><table>
                                    <tr>
                                            <td id="texto2">												
                                                <a href="imgdepoimentos/imgd07.jpg"><img id="depoimento-img" src="imgdepoimentos/imgd07_t.jpg"></img></a>
                                                  <img id="abre-aspas" src="imgdepoimentos/abre-aspas.png">
                                                  <div id="depoimento-texto">     
                                                      
                                                    <b>Janaína e Guilherme</b>
                                                    <br>
                                                         Ei, Selma!!! Tudo bem??
                                                    <br> Obrigada por tudo,viu?? 
                                                         A maquete ficou linda, a minha cara!! O bolo e os doces estavam maravilhosos, tudo muito gostoso!!
                                                    <br> Parabéns pelo trabalho!!
                                                    <br> Bjs 
                                                </div>
                                                
                                            </td>
                                    </tr>
                                    </table></div></div>								
                                    <br>
                                        
                                    <div id="sombra"><div id="box"><table>
                                    <tr>
                                            <td id="texto2">												
                                                 <a href="imgdepoimentos/imgd08.jpg"><img id="depoimento-img" src="imgdepoimentos/imgd08_t.jpg"></img></a>                                                  
                                                  <img id="abre-aspas" src="imgdepoimentos/abre-aspas.png">
                                                  <div id="depoimento-texto">                                                                                                                
                                                      
                                                    <b>Alessandra Lima</b>
                                                    <br>Olá, Selma!!!
                                                    <br>Acho que vou em seu evento, só pra relembrar o como seus doces são maravilhosos.
                                                    <br>Obrigado por toda sua atenção e parabéns pelo trabalho perfeito que você realiza.
                                                    <br>Que o nosso Senhor Jesus continue te abençoando muito sempre.	
                                                  </div>

                                            </td>
                                    </tr>
                                    </table></div></div>								
                                    <br>
                                        
                                    <div id="sombra"><div id="box"><table>
                                    <tr>
                                            <td id="texto2">												
                                                
                                                <a href="imgdepoimentos/imgd09.jpg"><img id="depoimento-img" src="imgdepoimentos/imgd09_t.jpg"></img></a>
                                                  <img id="abre-aspas" src="imgdepoimentos/abre-aspas.png">
                                                  <div id="depoimento-texto">                                                                             
                                                      
                                                    <b>Simone e José Luiz</b>
                                                    <br>   Ei Selma!
                                                    <br>    Queremos deixar registrada a nossa recomendação dos seus serviços. Seus doces e bolo são maravilhosos. Todos da festa elogiaram. Os doces não deram para quem quis (de tão gostosos). Muita gente que comeu ficou com água na boca, querendo mais RSRSRSRS.
                                                    <br>    Muito obrigada, viu!?? Seu atendimento é perfeito. Você e o Thiago são dois doces. Desejo muito sucesso pra vocês.
                                                    <br>    E nós, é claro, assim que pudermos vamos encomendar uma torta sua só para relembrar do dia que foi tão especial em nossas vidas.
                                                    <br>    Grande abraço!
                                                  </div>
                                            </td>
                                    </tr>
                                    </table></div></div>								
                                    <br>
                                        
                                        
                                    <div id="sombra"><div id="box"><table>
                                    <tr>
                                            <td id="texto2">												
                                                
                                                <a href="imgdepoimentos/imgd10.jpg"><img id="depoimento-img" src="imgdepoimentos/imgd10_t.jpg"></img></a>
                                                  <img id="abre-aspas" src="imgdepoimentos/abre-aspas.png">
                                                  <div id="depoimento-texto">                                                                             
                                                      
                                                    <b>Cristine Barroso </b>
                                                    <br>Olá Selma gostaria de te agradecer pelo carinho e dedicação com que vc nos tratou ontem com o bolo da Emanuelle,seu trabalho é simplismente perfeito.Parabéns ...que vc tenha muito sucesso...
                                                    <br>Seu bolo estava uma delicia!! 
                                                    <br>bjs...
                                                  </div>                                                    
                                            </td>
                                    </tr>
                                    </table></div></div>								
                                    <br>
                                        
                                </div>
                            </td>
                        </tr>
                        <tr >                        
                            <td colspan="2" id="contexto3">								
								<?php
									include("../../includes/msg_rodape.php");
								?>
							</td>
                        </tr>

                    </table>
                  
                </div>
        </div>

    	<div id="boxbottom">

        </div>
		<?php
            include("../../includes/rodape.php");
        ?>
    </div>

</body>
</html>
