<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="StyleSheet" type="text/css" href=" ../../css/estiloHome.css"/>
    <link rel="StyleSheet" type="text/css" href="./css/estiloParceiros.css"/>
    <link rel="SHORTCUT ICON" href="../../imagens/outras/favicon.png" type="image/x-icon" />
    <title>: : D'Sonhos : :</title>

    <link rel="stylesheet" type="text/css" href="../style-projects-jquery.css" />

    <!-- Arquivos utilizados pelo jQuery lightBox plugin -->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.lightbox-0.5.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
    <!-- / fim dos arquivos utilizados pelo jQuery lightBox plugin -->

    <script>
        function click() {
            if (event.button==2||event.button==3) {
                alert("Cópia Proibida! Conteúdo exclusivo D'Sonhos.");
                oncontextmenu='return false';
            }
        }
        document.onmousedown=click;
        document.oncontextmenu = new Function("return false;");

    </script>

     <!-- Ativando o jQuery lightBox plugin -->
    <script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox();
    });
    </script>
   	<style type="text/css">
	/* jQuery lightBox plugin - Gallery style */
	#gallery {
/*		background-color: #444;*/
		padding: 0px;
		width: 300px;
        /*text-align: left;*/
	}
	#gallery ul { list-style: none; }
	#gallery ul li { display: inline; }
	#gallery ul img {
/*		border: 5px solid #3e3e3e;*/
		border-width: 15px 15px 30px;
                margin: 15px;
	}
	#gallery ul a:hover img {
/*		border: 5px solid #fff;*/
		border-width: 15px 15px 30px;
		/*color: #fff;*/
	}
	#gallery ul a:hover { color: #fff;
        }
	</style>


</head>
<body>
	<?php include_once("../../includes/analysticsgoogle.php") ?>
    <div id="bg2">

    </div>

    <div id="corpo">
    	<div id="bgraios"> </div>
    	<div id="logo"> </div>
        <div id="boxtop"> </div>

        <div id="boxcenter">
				<?php
					include("../../includes/menu.php");
				?>
                <div id="contexto">
                    <table>
                        <tr>
                            <td id="contexto0" colspan="2">
                                <p id="titulo2" > Depoimentos</p>
                                <p style="font-size:75%; font-weight:normal;"><a href="../contato/contato.php?d=depoimento">Envie-nos um depoimento.</a></p>
                            </td>
                        </tr>
                        <tr>
                            <td id="contexto1">
                                <!--vazio-->								
                            </td>
                            <td id="contexto2">
                                 
								<div id="sombra"><div id="box"><table>
								<tr>
									<td id="texto2">												

										<b>Janaína e Guilherme</b>
										<br>
										Ei, Selma!!! Tudo bem??
										Obrigada por tudo,viu?? <br>A maquete ficou linda, a minha cara!!
										O bolo e os doces estavam maravilhosos, tudo muito gostoso!! <br>
										Parabéns pelo trabalho!!<br>
										Bjs 
									</td>
								</tr>
								</table></div></div>								
								<br>

								<div id="sombra"><div id="box"><table>
								<tr>
									<td id="texto2">												

										<b>Islany e Eduardo de Souza</b>
										<br>
											Bom dia,
										<br>Gostaríamos de agradecer a todos pela linda festa que vocês preparam para nós... Todos ficaram muito satisfeitos, foram muito bem servidos, amaram os doces, o bolo, a fonte de chocolate então, rsrsrs, ...tudo estava perfeito!!!
										<br>Muito obrigada mesmo. Agradecemos de coração.
										<br>Que Deus continue abençoando e prosperando cada um de vocês.
										<br>Um grande beijo... agora dos casados.
									</td>
								</tr>
								</table></div></div>								
								<br>
								
								<div id="sombra"><div id="box"><table>
								<tr>
									<td id="texto2">												

										<b>Michele Mastela</b>
										<br>
											Olá Selma e Thiago, tudo bem....
											<br>Olha gostaria de agradecer o carinho e o empenho de vcs na realização do meu sonho...o meu casamento foi maravilhoso e tudo estava divino...o Senhor Jesus foi muito maravilhoso comigo...tinha certeza de que iria ficar lindo, mas fui surpreendida com coisas além do que eu imaginava...realmente tive o casamento dos sonhos de qualquer uma...e tenho certeza que através do meu casamento vcs terão muito outros trabalhos,  pq são maravilhosos..um grande bju no coração.. fiquem com Deus e uma semana mais que abençoada...
									</td>
								</tr>
								</table></div></div>								
								<br>		
								
								<div id="sombra"><div id="box"><table>
								<tr>
									<td id="texto2">												

										<b>Kanik Ramos Pimentel</b>
										<br>
											Eii Selma tudo bem?
										<br>Menina não consigo esquece o gosto daquele seu bolo e daqueles docinhos de lua de mel!  Perfeitos, Muito obrigada por tudo.
										<br>Por tanto gostaria de saber se vc faz um bolo no  recheio que foi o do meu casamento e aqueles lua de mel, só que seria para o aniversário da minha mãe, dia 10 de março mais a festinha seria dia 11 ou 12 de março!
										<br>bjinhos									
									</td>
								</tr>
								</table></div></div>								
								<br>


								<div id="sombra"><div id="box"><table>
								<tr>
									<td id="texto2">												

										<b>Alessandra Lima</b>
										<br>Olá, Selma!!!
										<br>Acho que vou em seu evento, só pra relembrar o como seus doces são maravilhosos.
										<br>Obrigado por toda sua atenção e parabéns pelo trabalho perfeito que você realiza.
										<br>Que o nosso Senhor Jesus continue te abençoando muito sempre.							
									</td>
								</tr>
								</table></div></div>								
								<br>

								<div id="sombra"><div id="box"><table>
								<tr>
									<td id="texto2">												

										<b>Cristine Barroso </b>										
										<br>Olá Selma gostaria de te agradecer pelo carinho e dedicação com que vc nos tratou ontem com o bolo da Emanuelle,seu trabalho é simplismente perfeito.Parabéns ...que vc tenha muito sucesso ...seu bolo estava uma delicia!! bjs...
									</td>
								</tr>
								</table></div></div>								
								<br>

                           </td>
                        </tr>
                        <tr >                        
                            <td colspan="2" id="contexto3">								
								<?php
									include("../../includes/msg_rodape.php");
								?>
							</td>
                        </tr>

                    </table>
                </div>
        </div>

    	<div id="boxbottom">

        </div>
		<?php
            include("../../includes/rodape.php");
        ?>
    </div>

</body>
</html>
