<?php
    include 'descricao.php';
    
    //Verificar qual lista de eventos mostrar pelo link
    $evento ='';
    if(empty($_GET))                
        $evento = 'casamento';
    else{    
        $evento = explode("?", $_SERVER['REQUEST_URI']);
        $evento = $evento[1];
    }
    //Buscar os eventos da categoria
    $eventos = getEventosCategoria($evento);         
    
    if($evento == 'todos'){
        $categoria = array ( 'casamento' => 'Casamento' 
                            ,'15anos'     => 'Festa de 15 Anos' 
                            ,'aniversario'=> 'Aniversário'
        );
    }
    
    //Links para as categorais
    $linkalbuns = array ( 'todos'      => 'Todos'
                         ,'casamento' => 'Casamentos e Eventos' 
                         ,'15anos'     => 'Festas de 15 Anos' 
                         ,'aniversario'=> 'Aniversários' 
    );
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="StyleSheet" type="text/css" href=" ../../css/estiloHome.css"/>
    <link rel="StyleSheet" type="text/css" href="./css/estiloEventos.css"/>
    <link rel="SHORTCUT ICON" href="../../imagens/outras/favicon.png" type="image/x-icon" />
    <title>: : D'Sonhos : :</title>   

    <script>
        function click() {
            if (event.button==2||event.button==3) {
                alert("Cópia Proibida! Conteúdo exclusivo D'Sonhos.");
                oncontextmenu='return false';
            }
        }
        document.onmousedown=click;
        document.oncontextmenu = new Function("return false;");

    </script>

</head>
<body>
    <?php //include_once("../../includes/analysticsgoogle.php") ?>
    <div id="bg2"></div>

    <div id="corpo">
    	<div id="bgraios"> </div>
    	<div id="logo"> </div>
        <div id="boxtop"> </div>

        <div id="boxcenter">
                <?php
                        include("../../includes/menu.php");
                ?>
                <div id="contexto">
                    <table>
                        <tr>
                            <td id="contexto0" colspan="2">
                                
                            </td>
                        </tr>
                        <tr>
                            <td id="contexto1">
                                <!--vazio-->
                            </td>
                            <td id="contexto2">
                                <div id="gallery">
                                    <div id="sombra">
                                        <div id="box">
                                            <table>
                                            <tr>
                                            <td id="texto2">                                                
                                                <p id="titulo2">Eventos</p> 
                                                <div id="eventos-lista">

                                                    <?php
                                                        foreach ($linkalbuns as $link => $nome){
                                                            if ($link==$evento) {
                                                                echo '<a href="?'.$link.'" class="selecionado"><p class="link-album">'.$linkalbuns[$link].'</p></a>';
                                                            }else{
                                                                echo '<a href="?'.$link.'"><p class="link-album">'.$linkalbuns[$link].'</p></a>';                                                                
                                                            }
                                                        }
                                                    ?>
                                                </div>
                                                <div id="fotos-album">
                                                    <?php
                                                    
                                                        foreach ($eventos as $e => $infos){
                                                            if( $evento=='todos' ? $cat = '<br><span>'.$categoria[$eventos[$e][0]].'</span>' : $cat= '<br><span>'.$eventos[$e][2].'</span>');
                                                            echo '<a href="evento.php?'.$e.'"><div class="foto-album" ><img id="image-album" src="'.'fotos/'.$e.'/'.$eventos[$e][3].'"></img></div><div class="desc-album">'.$eventos[$e][1].$cat.'</div></a>';                                                            
                                                            //print_r ($eventos);
                                                          
                                                        }
                                                                                                           
                                                    ?>                                                                           
                                                </div>                                                       
                                            </td>
                                            </tr>
                                            </table>
                                        </div>
                                    </div>                                   
                                </div>
                                
                            </td>
                        </tr>
                        <tr >                        
                            <td colspan="2" id="contexto3">								
                                <?php
                                    include("../../includes/msg_rodape.php");
                                ?>
                            </td>
                        </tr>

                    </table>
                </div>
        </div>

    	<div id="boxbottom">

        </div>
		<?php
            include("../../includes/rodape.php");
        ?>
    </div>

</body>
</html>
