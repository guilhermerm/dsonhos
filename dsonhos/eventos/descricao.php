<?php
/*Definido as categorias! Casamentos e Eventos | 15 Anos  | Aniversário
 * 
 * A ordem obdecida será a de declaração
 * 
 */ 

 $descricao_eventos = array(
 	 'diana-e-wesley-labella' 				=> array('casamento','Diana e Wesley','La Bella','01t.jpg')		
	,'evento-praia-da-costa' 				=> array('casamento','Evento Praia da Costa','Shopping Praia da Costa','01t.jpg')	
	,'iza-e-bruno-missao-praia-da-costa'	=> array('casamento','Iza e Bruno','Missão Praia da Costa','01t.jpg')
	,'fabiola-e-thiago-chacara-paineiras' 	=> array('casamento','Fabíola e Thiago','Chacara Paineiras','01t.jpg')				
	,'jaqueline-porto-bianco' 				=> array('casamento','Jaqueline','Porto Bianco','01t.jpg')	
	,'priscila-e-higor-penha-e-guilherme-itamaraty' => array('casamento','Priscila e Higor - Penha e Guilherme','Itamaraty','01t.jpg')	
	,'fernanda-e-diego-lago-de-garda' 		=> array('casamento','Fernanda e Diego','Lago de Garda','01t.jpg')	
	,'tatiana-four-tower' 				=> array('casamento','Tatiana','Four Tower','08t.jpg')	
	,'franciene-e-estevo-aerth' 		=> array('casamento','Franciene e Estevo','Aerth','001t.jpg')	
	,'michele-e-alexandre-oasis' 		=> array('casamento','Michele e Alexandre','Oasis','001t.jpg')
	,'amanda-e-vitor-oasis' 			=> array('casamento','Amanda e Vitor','Oasis','004t.jpg')	
	,'camila-e-wesley-lareiraup' 		=> array('casamento','Camila e Wesley','Lareira Up','004t.jpg')	
	,'dryely-e-danilo-colatina' 		=> array('casamento','Dryely e Danilo','Colatina','001t.jpg')
	,'tamara-casamaio' 					=> array('casamento','Tamara','Casa Maio','006t.jpg')
	,'claudia-e-mauricio-warung' 		=> array('casamento','Claudia e Mauricio','Warung','001t.jpg')	
	,'anapaula-e-marcos-chacaraflora' 	=> array('casamento','Ana Paula e Marcos','Chácara Flora','002t.jpg')	
	,'nathalia-e-luiz-aerth' 			=> array('casamento','Nathalia e Luiz','Aerth','001t.jpg')
	,'morgana-e-pedro-warung'			=> array('casamento','Morgana e Pedro','Warung','003t.jpg')
	,'sabrina-e-vinicius-versales' 		=> array('casamento','Sabrina e Vinícius','Versales','007t.jpg')
	,'samira-e-cleidison-solardejasmim'	=> array('casamento','Samira e Cleidison','Solar de Jasmim','004t.jpg')
	,'janaina-e-guilherme-espaoverde' 	=> array('casamento','Janaina e Guilherme','Espaço Verde','001t.jpg')
	,'adriana-e-vinicius-aerth' 		=> array('casamento','Adriana e Vinícius','Aerth','003t.jpg')
	,'sr-e-sra-thomas-excelence' 		=> array('casamento','Sr e Sra Thomas','Excelence','001t.jpg')    
	,'alessandra-e-marcelo-igvida' 		=> array('casamento','Alessandra e Marcelo','Igreja Vida','005t.jpg')				
);

//Retonar um array com os eventos de uma categoria.
function getEventosCategoria($ev){
    global $descricao_eventos;
                        
    if($ev != 'todos'){
        $d_result = array();        
        foreach ($descricao_eventos as $d_evento => $infos){        
            if( $descricao_eventos[$d_evento][0] == $ev){
                $d_result[$d_evento] = $descricao_eventos[$d_evento];
            }            
        };            
        return $d_result;
    }else{
        return $descricao_eventos;
    }    
}

function getDescricao($ev){
    global $descricao_eventos;
    return $descricao_eventos[$ev][1];
}
?>