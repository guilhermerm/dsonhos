<?php
    include 'descricao.php';

    //Verificar qual evento mostrar pelo link
    $evento ='';
    if(empty($_GET))        
        header ("Location: eventos.php");
    else{    
        $evento = explode("?", $_SERVER['REQUEST_URI']);
        $evento = $evento[1];
    }
       
    $descricao = getDescricao($evento);
    //listar arquivos
    $fileZ = glob('fotos/'.$evento.'/*');
    
    if($fileZ == FALSE){
         header ("Location: eventos.php");
    }
    
    $c=0; $d=0;
    $fotos = null;
    $thumbs = null;
    $max = sizeof($fileZ);
    for($i=0;$i<$max;$i++)
    {
        if(!strpos($fileZ[$i],"t."))
        {
            //Foto
            $fotos[$c] = $fileZ[$i];
            $c++;
        }else{
            //Miniatura
            $thumbs[$d] = $fileZ[$i];
            $d++;
        }
    }        

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="StyleSheet" type="text/css" href=" ../../css/estiloHome.css"/>
    <link rel="StyleSheet" type="text/css" href="./css/estiloEventos.css"/>
    <link rel="SHORTCUT ICON" href="../../imagens/outras/favicon.png" type="image/x-icon" />
    <title>: : D'Sonhos : :</title>

    <link rel="stylesheet" type="text/css" href="../style-projects-jquery.css" />

    <!-- Arquivos utilizados pelo jQuery lightBox plugin -->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.lightbox-0.5.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
    <!-- / fim dos arquivos utilizados pelo jQuery lightBox plugin -->

    <script>
        function click() {
            if (event.button==2||event.button==3) {
                alert("Cópia Proibida! Conteúdo exclusivo D'Sonhos.");
                oncontextmenu='return false';
            }
        }
        document.onmousedown=click;
        document.oncontextmenu = new Function("return false;");

    </script>

     <!-- Ativando o jQuery lightBox plugin -->
    <script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox();
    });
    </script>
   	<style type="text/css">
	/* jQuery lightBox plugin - Gallery style */
	#gallery {
/*		background-color: #444;
		padding: 10px;
		width: 520px;
        text-align: left;*/
	}
	#gallery ul { list-style: none; }
	#gallery ul li { display: inline; }
	#gallery ul img {
/*		border: 5px solid #3e3e3e;*/
		border-width: 15px 15px 30px;
                margin: 15px;
	}
	#gallery ul a:hover img {
/*		border: 5px solid #fff;*/
		border-width: 15px 15px 30px;
		/*color: #fff;*/
	}
	#gallery ul a:hover { color: #fff;
        }
	</style>
</head>
<body onload="popup()">
    <?php include_once("../../includes/analysticsgoogle.php") ?>

    <div id="corpo">    
        <div id="bgraios"></div>
        <div id="logo">
            <a href="../inicio/inicio.php">
                <img src="../../imagens/layout/logo-dsonhos.png"/>                
            </a>
        </div>
       
        <div id="boxtop"></div>
        <div id="boxcenter">
            
            <div id="menu-maior">
                <?php include("../../includes/menu.php"); ?>
            </div>                       
            
            <div id="conteudo">                    
                
                <div id="tit-album">
                    <div class="titulo2"><?php echo $descricao; ?></div>
                </div>
                
                <div id="opcoes"><a href="javascript:history.go(-1)">Voltar</a></div>
                
                <div id="gallery">
                    <div id="fotos">
                        <?php
                            for($i=0;$i<$c;$i++){
                                echo '<a href="'.$fotos[$i].'"><div class="foto" ><img id="image" src="'.$thumbs[$i].'"></img></div></a>';                                                            
                            }
                        ?>              
                    </div>
                </div>  
            </div>
            
            
            <?php include("../../includes/msg_rodape.php"); ?>            
            
        </div>
    	<div id="boxbottom"></div>
		<?php
            include("../../includes/rodape.php");
        ?>
    </div>

</body>    

</html>
